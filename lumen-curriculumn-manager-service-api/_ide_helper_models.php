<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Contact
 *
 * @property int $id
 * @property int $school_id
 * @property string $contact_name
 * @property string $contact_number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact query()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereContactNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereUpdatedAt($value)
 */
	class Contact extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\School
 *
 * @property int $id
 * @property string $name
 * @property string|null $short_name
 * @property string $type
 * @property int|null $region_id
 * @property int|null $location_id
 * @property int|null $contact_id
 * @property string $ownership
 * @property string $sex
 * @property string $students
 * @property string|null $classification
 * @property string|null $school_code
 * @property int|null $population
 * @property string|null $bio_info
 * @property string|null $ghana_post_code
 * @property string|null $email
 * @property string|null $website
 * @property string|null $colours
 * @property string|null $logo_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Integration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration query()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereBioInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereClassification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereColours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereContactId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereGhanaPostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereLogoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereOwnership($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration wherePopulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereSchoolCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereStudents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereWebsite($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 * @property-read int|null $contacts_count
 */
	class School extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @mixin \Eloquent
 */
	class User extends \Eloquent implements \Illuminate\Contracts\Auth\Authenticatable, \Illuminate\Contracts\Auth\Access\Authorizable {}
}

