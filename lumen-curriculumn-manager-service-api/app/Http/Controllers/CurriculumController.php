<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\Curriculum;
use App\Models\CurriculumSubject;
use App\Models\FileModel;
use App\Models\Semester;
use App\Models\SemesterCurriculum;
use App\Models\Subject;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CurriculumController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){

        $curriculumns = Curriculum::select([
            'curriculumns.id',
            'curriculumns.name',
            'curriculumns.code',
            'curriculumns.year as duration',
            \DB::raw('count(curriculums_subjects.curriculum_id) as subjects'),
            'curriculumns.class_id',
            'curriculumns.course_id',
            'curriculumns.level_id',
            'curriculumns.created_at',
            'curriculumns.updated_at'
        ])->leftJoin('curriculums_subjects', 'curriculumns.id', '=', 'curriculums_subjects.curriculum_id')
            ->groupBy('curriculumns.id');

        return Datatables::of($curriculumns)->make(true);
    }

    public function store(Request $request)
    {

//        return $request->all();

//        selectedTertiaryCourse: ""
//        selectedTertiaryDepartment: ""

        $this->validate($request, [
            "curriculumnName" => "required",
            "curriculumnCode" => "required",
            "curriculumnYear" => "required",
        ]);

        $semesters = $request->get("semesters");

        $class_id = 0;
        $course_id = 0;

        if ($request->has("selectedBasicSchoolClass") && !empty($request->selectedBasicSchoolClass)){
            $class_id = $request->selectedBasicSchoolClass;
        }

        if ($request->has("selectedSeniorHighClass") && !empty($request->selectedSeniorHighClass)){
            $class_id = $request->selectedSeniorHighClass;
        }

        if ($request->has("selectedTertiaryClass") && !empty($request->selectedTertiaryClass)){
            $class_id = $request->selectedTertiaryClass;
        }

        if ($request->has("curriculumnClassId") && !empty($request->curriculumnClassId)){
            $class_id = $request->curriculumnClassId;
        }

        if ($request->has("curriculumnCourseId") && !empty($request->curriculumnCourseId)){
            $course_id = $request->curriculumnCourseId;
        }

        if ($request->has("selectedSeniorHighCourse") && !empty($request->selectedSeniorHighCourse)){
            $course_id = $request->selectedSeniorHighCourse;
        }

        if ($request->has("selectedTertiaryCourse") && !empty($request->selectedTertiaryCourse)){
            $course_id = $request->selectedTertiaryCourse;
        }

        $curriculumn = Curriculum::create([
                "name" => $request->curriculumnName,
                "code" => $request->curriculumnCode,
                "year" => $request->curriculumnYear,
                "class_id" => $class_id != 0 ? $class_id : null,
                "course_id" => $course_id != 0 ? $course_id :null,
                "level_id"  => $request->curriculumnLevelId
            ]);



        if ($request->has("semesterOneStartDate") && !empty($request->semesterOneStartDate)) {

                $semester = Semester::firstOrCreate([
                    "name" => "Semester One"
                ]);

                SemesterCurriculum::create([
                    "curriculumn_id" => $curriculumn->id,
                    "semester_id" => $semester->id,
                    "start_period" => $request->semesterOneStartDate,
                    "end_period" => $request->semesterOneEndDate,
                ]);
            }

        if ($request->has("semesterTwoStartDate") && !empty($request->semesterTwoStartDate)) {
            $semester = Semester::firstOrCreate([
                "name" => "Semester Two"
            ]);

            SemesterCurriculum::create([
                "curriculumn_id" => $curriculumn->id,
                "semester_id" => $semester->id,
                "start_period" => $request->semesterTwoStartDate,
                "end_period" => $request->semesterTwoEndDate,
            ]);
        }

        if ($request->has("semesterThreeStartDate") && !empty($request->semesterThreeStartDate)) {

            $semester = Semester::firstOrCreate([
                "name" => "Semester Three"
            ]);

            SemesterCurriculum::create([
                "curriculumn_id" => $curriculumn->id,
                "semester_id" => $semester->id,
                "start_period" => $request->semesterTwoStartDate,
                "end_period" => $request->semesterTwoEndDate,
            ]);
        }


        return response()->json(["data" => $curriculumn, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $curriculum = Curriculum::find($id)->update($request->all());
        return response()->json(["message" => "empty", "data" => $curriculum]);
    }

    public function show(Request $request, $id){
        $curriculum = Curriculum::find($id);
        return response()->json(["message" => "empty", "data" => $curriculum]);
    }

    public function delete(Request $request, $id){
        $curriculum = Curriculum::find($id)->delete();
        return $this->response("$curriculum",200);
    }

    public function subjects($curriculum_id){

        $subjects = Subject::select([
            'subjects.id',
            'subjects.name',
            'subjects.code',
            \DB::raw('count(subject_topics.subject_id) as topics'),
            'subjects.created_at',
            'subjects.updated_at'
        ])->rightJoin("curriculums_subjects","subjects.id","=","curriculums_subjects.subject_id")
            ->leftJoin("subject_topics","subjects.id","=","subject_topics.subject_id")
            ->where("curriculums_subjects.curriculum_id",$curriculum_id)
            ->groupBy('subjects.id')
            ->orderBy("created_at","desc")
            ->get();
        return response()->json( $subjects);
    }

    public function addSubjectToCurriculum(Request $request){

        $this->validate($request, [
            "name"      => "required",
            "code"      => "required",
            "curriculum_id" => "required"
        ]);

        $subject = Subject::firstOrCreate([
            "name" => $request->name,
            "code" => strtolower($request->code)
        ]);

        CurriculumSubject::create([
            "subject_id" => $subject->id,
            "curriculum_id" => $request->curriculum_id
        ]);

        return response()->json(["message" => "success"]);
    }

}
