<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\Course;
use App\Models\FileModel;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class CoursesController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){
        $courses = Course::paginate(15);
        return response()->json(["data" => $courses, "message" => "success"],200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $course = new Course();
        $course->name = $request->name;
        $course->save();
        return response()->json(["data" => $course, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $course = Course::find($id)->update($request->all());
        return response()->json(["message" => "empty", "data" => $course]);
    }

    public function show(Request $request, $id){

        $course = Course::find($id);
        return response()->json(["message" => "empty", "data" => $course]);
    }

    public function delete(Request $request, $id){
        $course = Course::find($id)->delete();

        return $this->response("$course",200);
    }

}
