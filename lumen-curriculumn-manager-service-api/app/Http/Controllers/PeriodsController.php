<?php

namespace App\Http\Controllers;

use App\Models\Period;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PeriodsController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){

        $query_builder = Period::query();

        if ($request->has("curriculum_id")){
            $query_builder->where("curriculum_id","=",$request->query("curriculum_id"))->get();
        }else{
            $query_builder->get();
        }

        return Datatables::of($query_builder)->make(true);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "label" => "required",
            "start_time" => "required",
            "end_time" => "required",
            "curriculum_id" => "required"
        ]);

        $period = new Period();
        $period->label = $request->label;
        $period->start = $request->start_time;
        $period->end = $request->end_time;
        $period->curriculum_id = $request->curriculum_id;
        $period->save();
        return response()->json(["data" => $period, "message" => "success"],200);
    }


    public function update(Request $request,$id){
        $period = Period::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $period]);
    }

    public function show(Request $request, $id){
        $period = Period::find($id);
        return response()->json(["message" => "empty", "data" => $period]);
    }

    public function delete(Request $request, $id){
        $period = Period::find($id)->delete();
        return $this->response($period,200);
    }

}
