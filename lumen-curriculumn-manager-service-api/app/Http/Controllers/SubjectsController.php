<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\FileModel;
use App\Models\Subject;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SubjectsController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){
        $subjects = Subject::select([
            'subjects.id',
            'subjects.name',
            'subjects.code',
            \DB::raw('count(subject_topics.subject_id) as topics'),
            'subjects.created_at',
            'subjects.updated_at'
        ])->leftJoin('subject_topics', 'subjects.id', '=', 'subject_topics.subject_id')

            ->groupBy('subjects.id');

        return Datatables::of($subjects)->make(true);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "code" => "required"
        ]);

        $subject = new Subject();
        $subject->name = $request->name;
        $subject->code = $request->code;
        $subject->save();
        return response()->json(["data" => $subject, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $subject = Subject::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $subject]);
    }

    public function show(Request $request, $id){

        $subject = Subject::find($id);
        return response()->json([$subject]);
    }

    public function delete(Request $request, $id){
        $subject = Subject::find($id)->delete();

        return $this->response("$subject",200);
    }

}
