<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\FileModel;
use App\Models\InstitutionHierarchy;
use App\Models\InstitutionType;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class InstitutionTypesController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){

        $institution_types = InstitutionType::with(["hierarchies" => function ($hierarchy){
            $hierarchy->with("hierarchy");
        }])->get();

        return response()->json(["data" => $institution_types, "message" => "success"],200);
    }

    public function show($id){

//        $institution_type = InstitutionType::with(["hierarchies" => function ($hierarchy){
//            $hierarchy->with("hierarchy");
//        }])->where("id", $id)->first();

        $institution_type = InstitutionType::where("id", $id)->selectRaw("id,name")->first();

        return response()->json( $institution_type,200);
    }

    public function hierarchiesIndex(Request $request): JsonResponse
    {
        $institution_hierarchies = InstitutionHierarchy::all();
        return response()->json(["data" => $institution_hierarchies, "message" => "success"],200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:institution_types"
        ]);

        $institution_type = new InstitutionType();
        $institution_type->name = $request->name;
        $institution_type->order = $request->order;
        $institution_type->save();
        return response()->json(["data" => $institution_type, "message" => "success"],200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeInstitutionHierarchy(Request $request): JsonResponse
    {
        $this->validate($request, [
            "name" => "required|unique:institution_hierarchies"
        ]);

        $institution_hierarchy = new InstitutionHierarchy();
        $institution_hierarchy->name = $request->name;
        $institution_hierarchy->order = $request->order;
        $institution_hierarchy->save();
        return response()->json(["data" => $institution_hierarchy, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $types = InstitutionType::find($id)->update($request->all());
        return response()->json(["data" => $types]);
    }

    public function storeHierarchy(Request $request): JsonResponse
    {
        $this->validate($request, [
            "institution_type_id" => "required",
            "institution_hierarchy_id" => "required"
        ]);

        $hierarchy = \DB::table("institution_type_hierarchies")->insert([
            "institution_type_id" => $request->institution_type_id,
            "institution_hierarchy_id" => $request->institution_hierarchy_id
        ]);

        return response()->json(["data" => $hierarchy, "message" => "success"],200);
    }

//    public function update(Request $request,$id){
//        $types = InstitutionType::find($id)->update($request->all());
//        return response()->json(["data" => $types]);
//    }

//    public function show(Request $request, $id){
//
//        $class = Clazz::find($id);
//        return response()->json(["message" => "empty", "data" => $class]);
//    }

    public function delete(Request $request, $id){
        $type = InstitutionType::find($id)->delete();
        return $this->response("$type",200);
    }

}
