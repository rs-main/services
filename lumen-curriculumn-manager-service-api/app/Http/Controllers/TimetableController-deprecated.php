<?php

namespace App\Http\Controllers;

use App\Models\Curriculum;
use App\Models\Day;
use App\Models\Period;
use App\Models\Timetable;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class TimetableController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){

        $query_builder = Timetable::query();

        if ($request->has("curriculum_id")){
            $timetables = $query_builder->where("curriculum_id","=",$request->query("curriculum_id"))
                ->with(["subject:id,name","curriculum:id,name","period:id,label,start,end","day:id,name"])
                ->distinct("")
                ->get();
        }else{
            $timetables = $query_builder->get();
        }

//        $tables = $timetables->map(function ($timetable){
//            $days = Day::all();
//            foreach ($days as $day){
//                if ($timetable->day_id == $day->id){
//                   return  [
//                        "period" => [
//                            "label" => $timetable->period->label,
//                            "start_time" => $timetable->period->start,
//                            "end_time" => $timetable->period->end,
//                        ],
//                       $day->name => $timetable->subject->name
//                    ];
//                }
//            }
//        });

        $tables = $timetables->map(function ($timetable){
            $periods = Period::where("curriculum_id",$timetable->curriculum_id)->get();
            $periodsArray = [];
            $days =         [];

            foreach ($periods as $key => $period) {

//                foreach (Day::all() as $day_key => $day){
//                    if ($period->id == $timetable->period_id && $day->id == $timetable->day->id){
//                        $days[$day_key] = [
//                            $day->name => [
//                                "subject"  => $timetable->subject->name
//                            ]
//                        ];
//                    }
//                }

                $periodsArray[$key] =  [
                         "id"   => $timetable->id,
                        "label" => $timetable->period->label,
                        "start_time" => $timetable->period->start,
                        "end_time" => $timetable->period->end,
                        "subject" => $timetable->subject->name,
                        "days" => $days
                    ];
                }

            return $periodsArray;

        });

        return $this->response($tables,200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "day_id" => "required",
            "period_id" => "required",
            "curriculum_id" => "required",
            "subject_id" => "required"
        ]);

        $timetable = new Timetable();
        $timetable->day_id = $request->day_id;
        $timetable->period_id = $request->period_id;
        $timetable->curriculum_id = $request->curriculum_id;
        $timetable->subject_id = $request->subject_id;
        $timetable->save();
        return response()->json(["data" => $timetable, "message" => "success"],200);
    }


    public function update(Request $request,$id){
        $timetable = Timetable::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $timetable]);
    }

    public function show(Request $request, $id){
        $timetable = Timetable::find($id);
        return response()->json(["message" => "empty", "data" => $timetable]);
    }

    public function delete(Request $request, $id){
        $timetable = Timetable::find($id)->delete();
        return $this->response($timetable,200);
    }

}
