<?php

namespace App\Http\Controllers;

use App\Models\Curriculum;
use App\Models\Day;
use App\Models\Period;
use App\Models\Timetable;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class TimetableController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){

        $query_builder = Period::query();
        $query_builder->where("curriculum_id",$request->curriculum_id);
        $periods= $query_builder->get();
        $days = Day::all();
        $periods = $periods->map(function ($period)use($days){
             $daysfiltered = $days->filter(function ($day)use($period){
                $timetable = Timetable::where("day_id",$day->id)->where("period_id",$period->id)->first();
                return [
                    $day->name => $timetable->id
                ];
            });
            return [
                "start" => $period->start,
                "end"   => $period->end,
                "days" => $daysfiltered
            ];
        });

        return $this->response($periods,200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "day_id" => "required",
            "period_id" => "required",
            "curriculum_id" => "required",
            "subject_id" => "required"
        ]);

        $timetable = new Timetable();
        $timetable->day_id = $request->day_id;
        $timetable->period_id = $request->period_id;
        $timetable->curriculum_id = $request->curriculum_id;
        $timetable->subject_id = $request->subject_id;
        $timetable->save();
        return response()->json(["data" => $timetable, "message" => "success"],200);
    }


    public function update(Request $request,$id){
        $timetable = Timetable::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $timetable]);
    }

    public function show(Request $request, $id){
        $timetable = Timetable::find($id);
        return response()->json(["message" => "empty", "data" => $timetable]);
    }

    public function delete(Request $request, $id){
        $timetable = Timetable::find($id)->delete();
        return $this->response($timetable,200);
    }

}
