<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\FileModel;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class ClassesController  extends Controller
{
    use ConsumeExternalService;

    private $storage;
    private $s3_url;

    public function __construct()
    {

    }

    public function index(Request $request){
        $classes = Clazz::paginate(15);
        return response()->json(["data" => $classes, "message" => "success"],200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $clazz = new Clazz();
        $clazz->name = $request->name;
        $clazz->save();
        return response()->json(["data" => $clazz, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $class = Clazz::find($id)->update($request->all());
        return response()->json(["message" => "empty", "data" => $class]);
    }

    public function show(Request $request, $id){

        $class = Clazz::find($id);
        return response()->json(["message" => "empty", "data" => $class]);
    }

    public function delete(Request $request, $id){
        $class = Clazz::find($id)->delete();

        return $this->response("$class",200);
    }




}
