<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\FileModel;
use App\Models\Subject;
use App\Models\SubjectTopic;
use App\Models\SubTopic;
use App\Models\Topic;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class TopicsController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){
        $topics = Topic::with(["subtopics" => function($subtopic){
           $subtopic->with("topic")->selectRaw("id,topic_id,parent_id");
        }])->selectRaw("topics.id,topics.name");

        return Datatables::of($topics)->make(true);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "subjectId" => "required"
//            "code" => "required"
        ]);

        $topic = new Topic();
        $topic->name = $request->name;
//        $topic->code = $request->code;
        if($topic->save()){
            SubjectTopic::create([
                "topic_id" => $topic->id,
                "subject_id" => $request->subjectId
            ]);
        }
        return response()->json(["data" => $topic, "message" => "success"],200);
    }

    public function storeSubTopic(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "parent_id" => "required"
        ]);

        $topic = new Topic();
        $topic->name = $request->name;
//        $topic->code = $request->code;
        if($topic->save()){
            $subtopic = new SubTopic();
            $subtopic->topic_id = $topic->id;
            $subtopic->parent_id = $request->parent_id;
            $subtopic->save();
            return response()->json(["data" => $subtopic, "message" => "success"],200);
        }

        return response()->json(["data" => null, "message" => "failed"],500);
    }

    public function update(Request $request,$id){
        $topic = Topic::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $topic]);
    }

    public function show(Request $request, $id){

        $topic = Topic::find($id);
        return response()->json(["message" => "empty", "data" => $topic]);
    }

    public function delete(Request $request, $id){
        $topic = Topic::find($id)->delete();
        return $this->response($topic,200);
    }

}
