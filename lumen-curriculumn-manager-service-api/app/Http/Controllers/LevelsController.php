<?php

namespace App\Http\Controllers;

use App\Models\Clazz;
use App\Models\Course;
use App\Models\FileModel;
use App\Models\Level;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class LevelsController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {

    }

    public function index(Request $request){
        $levels = Level::paginate(15);
        return response()->json(["data" => $levels, "message" => "success"],200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $level = new Level();
        $level->name = $request->name;
        $level->save();
        return response()->json(["data" => $level, "message" => "success"],200);
    }

    public function update(Request $request,$id){
        $level = Level::find($id)->update($request->all());
        return response()->json(["message" => "success", "data" => $level]);
    }

    public function show(Request $request, $id){

        $level = Level::find($id);
        return response()->json(["message" => "empty", "data" => $level]);
    }

    public function delete(Request $request, $id){
        $level = Level::find($id)->delete();
        return $this->response($level,200);
    }

}
