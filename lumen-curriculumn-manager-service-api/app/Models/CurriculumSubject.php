<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class CurriculumSubject extends Model
{
    use HasFactory;

    protected $table = "curriculums_subjects";

    protected $guarded = [];

    public function subjects(){
        return $this->hasMany(Subject::class,"subject_id","id");
    }
}
