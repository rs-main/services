<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class InstitutionHierarchy extends Model
{
    use HasFactory;

    protected $table = "institution_hierarchies";

    protected $guarded = [];


}
