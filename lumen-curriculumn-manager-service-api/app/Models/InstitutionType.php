<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class InstitutionType extends Model
{
    use HasFactory;

    protected $table = "institution_types";

    protected $guarded = [];

    public function hierarchies(){
        return $this->hasMany(InstitutionTypeHierarchy::class,"institution_type_id");
    }

}
