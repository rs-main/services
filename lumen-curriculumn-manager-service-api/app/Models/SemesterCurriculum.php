<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class SemesterCurriculum extends Model
{
    use HasFactory;

    protected $table = "semester_curriculumn";

    protected $guarded = [];
}
