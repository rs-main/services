<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class InstitutionTypeHierarchy extends Model
{
    use HasFactory;

    protected $table = "institution_type_hierarchies";

    protected $guarded = [];

    public function hierarchy(){
        return $this->belongsTo(InstitutionHierarchy::class,"institution_hierarchy_id");
    }


}
