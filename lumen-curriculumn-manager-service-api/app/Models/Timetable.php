<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Timetable extends Model
{
    use HasFactory;

    protected $table = "timetables";

    protected $guarded = [];

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function curriculum(){
        return $this->belongsTo(Curriculum::class);
    }

    public function period(){
        return $this->belongsTo(Period::class);
    }

    public function day(){
        return $this->belongsTo(Day::class);
    }

}
