<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class SubjectTopic extends Model
{
    use HasFactory;

    protected $table = "subject_topics";

    protected $guarded = [];
}
