<?php


namespace App\Interfaces;

interface INotification
{
    public function buildNotification($user, $type,$service,$mode,$message);

    public function sendNotification();

}
