<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use Illuminate\Console\Command;

class HeartBeatCommand extends Command
{

    protected $signature = 'send:heart-beat';


    protected $description = 'Send Heartbeat to Eureka';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->sendHeartBeat();
    }

    private function sendHeartBeat(){

        $name = "curriculums-service";
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' => $name,
            "appName" => $name,
            'port' => ["8006", true],

        ]);

        $client = new EurekaClient($config);
        return $client->heartbeat();

    }
}
