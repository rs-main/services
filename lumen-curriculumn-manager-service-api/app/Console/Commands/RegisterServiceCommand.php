<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use Illuminate\Console\Command;


class RegisterServiceCommand extends Command
{

    protected $signature = 'register:service';


    protected $description = 'Register Service';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->registerService();
    }

    private function registerService(){

        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName'       =>  "curriculums-service",
            "appName"        =>  "curriculums-service",
            "ip"             =>  $this->getIp(),
            'port'           => ["8006", true],
            'homePageUrl'    => "http://localhost:"."8006",
            'statusPageUrl'  => "http://localhost:"."8006/info",
            'healthCheckUrl' => "http://localhost:"."8006/health"
        ]);

        $serviceClient = new EurekaClient($config);
        $serviceClient->register();
    }

    public function getIp()
    {
        return env("APP_ENV")== "local" ? "127.0.0.1" : \request()->ip();
    }
}
