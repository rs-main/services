<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionTypeHierarchiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_type_hierarchies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("institution_type_id");
            $table->foreign("institution_type_id")->references("id")->on("institution_types")->onDelete("cascade");
            $table->unsignedBigInteger("institution_hierarchy_id");
            $table->foreign("institution_hierarchy_id")->references("id")->on("institution_hierarchies")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institution_type_hierarchies');
    }
}
