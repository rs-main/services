<?php

namespace Database\Seeders;

use App\Models\SubTopic;
use App\Models\Topic;
use Illuminate\Database\Seeder;

class SubTopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topic_ids = Topic::inRandomOrder()->pluck("id")->toArray();

        for($i = 0; $i < sizeof($topic_ids); $i++ )
        SubTopic::create([
            "topic_id" => $topic_ids[$i],
            "parent_id" => $topic_ids[$i],
        ]);
    }
}
