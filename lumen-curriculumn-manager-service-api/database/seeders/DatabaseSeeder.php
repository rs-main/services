<?php

namespace Database\Seeders;

use App\Models\Clazz;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\Level;
use App\Models\Subject;
use App\Models\SubTopic;
use App\Models\Topic;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

//        Curriculum::truncate();
//        Clazz::truncate();
//        Course::truncate();
//        Level::truncate();
//        Subject::truncate();
//        \DB::table("curriculums_subjects")->truncate();

        for($i=1; $i < 6; $i++){
            $class = Clazz::create([
                "name" => "Class $i"
            ]);

            $course = Course::create([
                "name" => "Course $i"
            ]);

            $level = Level::create([
                "name" => "Level $i"
            ]);

            $subject = Subject::create([
                "name" => "Subject $i"
            ]);

            $curriculum = Curriculum::create([
                "name" => "Curriculum $i",
                "year" => rand(1,3),
                "class_id" => $class->id,
                "course_id" => $course->id,
                "level_id" => $level->id,
            ]);

            \DB::table("curriculums_subjects")->insert([
                "curriculum_id" => $curriculum->id,
                "subject_id"    => $subject->id
            ]);

            $topic = Topic::create([
                "name" => "Topic $i"
            ]);

            \DB::table("subject_topics")->insert([
                "subject_id" => 1,
                "topic_id"    => $topic->id
            ]);

        }




    }
}
