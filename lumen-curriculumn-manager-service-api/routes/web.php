<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    //Curriculum routes
    $router->get('curriculums', 'CurriculumController@index');
    $router->post('curriculums', 'CurriculumController@store');
    $router->get('curriculums/{id}', 'CurriculumController@show');
    $router->post('curriculums/{id}', 'CurriculumController@delete');
    $router->get('curriculums/subjects/{curriculum_id}', 'CurriculumController@subjects');

    $router->post('subject-curriculums', 'CurriculumController@addSubjectToCurriculum');

    // Subject routes
    $router->get('subjects', 'SubjectsController@index');
    $router->post('subjects', 'SubjectsController@store');
    $router->get('subjects/{id}', 'SubjectsController@show');
    $router->post('subjects/{id}', 'SubjectsController@delete');


//    $router->get('curriculum-subjects', 'SubjectsController@delete');

    // Topic routes
    $router->get('topics', 'TopicsController@index');
    $router->post('topics', 'TopicsController@store');
    $router->post('subtopics', 'TopicsController@storeSubTopic');
    $router->get('topics/{id}', 'TopicsController@show');
    $router->post('topics/{id}', 'TopicsController@delete');

    // Levels routes
    $router->get('levels', 'LevelsController@index');
    $router->post('levels', 'LevelsController@store');
    $router->get('levels/{id}', 'LevelsController@show');
    $router->post('levels/{id}', 'LevelsController@delete');


    // Classes routes
    $router->get('classes', 'ClassesController@index');
    $router->post('classes', 'ClassesController@store');
    $router->get('classes/{id}', 'ClassesController@show');
    $router->post('classes/{id}', 'ClassesController@delete');

    // Courses routes
    $router->get('courses', 'CoursesController@index');
    $router->post('courses', 'CoursesController@store');
    $router->get('courses/{id}', 'CoursesController@show');
    $router->post('courses/{id}', 'CoursesController@delete');

    $router->get('periods', 'PeriodsController@index');
    $router->post('periods', 'PeriodsController@store');
    $router->get('periods/{id}', 'PeriodsController@show');
    $router->post('periods/{id}', 'PeriodsController@delete');

    // timetables
    $router->get('timetables', 'TimetableController@index');
    $router->post('timetables', 'TimetableController@store');
    $router->get('timetables/{id}', 'TimetableController@show');
    $router->post('timetables/{id}', 'TimetableController@delete');

    // institution types
    $router->get('institution-types', 'InstitutionTypesController@index');
    $router->post('institution-types', 'InstitutionTypesController@store');
    $router->get('institution-types/{id}', 'InstitutionTypesController@show');
    $router->post('institution-types/{id}', 'InstitutionTypesController@delete');

    // institution type hierarchies
    $router->get('institution-hierarchies', 'InstitutionTypesController@hierarchiesIndex');
    $router->post('institution-hierarchies', 'InstitutionTypesController@storeInstitutionHierarchy');

    //assign institution hierarchies
    $router->post('institution-hierarchies/assign-hierarchy', 'InstitutionTypesController@storeHierarchy');

});

