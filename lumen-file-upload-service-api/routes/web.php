<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Models\FileModel;

$router->get('/', function () use ($router) {
    return ["name" => "UPLOAD Service", "port" => "8003"];
});

$router->get('/health', function () use ($router) {
    return response()->json("Service is active",200);
});

$router->get('/info', function () use ($router) {
    return response()->json("Service is active",200);
});


$router->group(['prefix' => 'api'], function () use ($router) {
        // Uploads
    $router->get('uploads', 'UploadsController@index');
    $router->post('uploads', 'UploadsController@create');
    $router->post('uploads/file/new', 'UploadsController@uploadOnlyFile');
    $router->get('uploads/{id}', 'UploadsController@show');
    $router->post('uploads/{id}', 'UploadsController@delete');

    $router->get('uploads/file/bookmarks/{user_id}/{file_id}', 'UploadsController@getBookMark');
    $router->post('uploads/file/bookmarks', 'UploadsController@bookMarkPage');
    $router->get('uploads/file/bookmarks','UploadsController@getUserBookMarks');
    $router->get('files/stats','UploadsController@getStats');

    // Search
    $router->get('search',"SearchController@search");
    $router->get('search/index',"SearchController@indexSearch");
    $router->get('search/queries','UploadsController@getSearchQueries');

    // Bag
    $router->get("/downloads/bag/{user_id}","DownloadsController@getBag");
    $router->post("/downloads/bag/{id}","DownloadsController@sendToBag");

    //Favourites
    $router->get("/downloads/favourites/{user_id}","DownloadsController@getFavourites");
    $router->post("/downloads/favourites","DownloadsController@addToFavourites");

    //Downloads
    $router->get("/downloads/{id}","DownloadsController@download");
    $router->post("/downloads/{id}","DownloadsController@delete");


    $router->get('index','UploadsController@indexModel');
    $router->get('flush','UploadsController@flushModel');

    // Storage routes
    $router->get('storage','StorageController@show');
    $router->post('storage','StorageController@addClientStorage');

    $router->get('authors','AuthorController@index');
    $router->post('authors','AuthorController@store');
    $router->get('authors/{id}','AuthorController@show');




    $router->get('/books',function (){
        $json_url = public_path("books.json");
        $json = (json_decode(file_get_contents($json_url),true));

//        foreach ($json as $auth){
//            $author = new \App\Models\Author();
//            $author->name = $auth["author"];
//            $author->save();
//        }

        foreach ($json as $file){

            foreach ($file["authors"] as $auth){
            $author = new \App\Models\Author();
            $author->name = $auth;
            $author->save();
             }
        }
//            $filemodel->save();
////            echo "File Upload $i \n";
//        }

//        FileModel::truncate();
//        foreach ($json as $file){
//            $filemodel = new FileModel();
//            $filemodel->service_id = 1;
//            $filemodel->name = $file["title"];
//            $filemodel->storage = "s3";
//            $filemodel->file_url = "https://rs-file-service.s3.af-south-1.amazonaws.com/gkb/books/cover-arts/A-Microservice-Architecture-with-Spring-Boot-and-Spring-Cloud.pdf";
//            $filemodel->extension = "pdf";
//            $filemodel->type = "books";
//            $filemodel->file_size = 1.15;
//            $filemodel->isbn = isset($file["isbn"]) ? $file["isbn"] : "";
//            $filemodel->user_id = 1;
//            $filemodel->cover_art_url = isset($file["thumbnailUrl"]) ? $file["thumbnailUrl"] : "";
//            $filemodel->cover_art_size = 1.15;
//            $filemodel->cover_art_extension = "jpg";
//            $filemodel->page_count = isset($file["pageCount"]) ? $file["pageCount"] : "";
//            $filemodel->downloads  = 0;
//            $filemodel->description  = isset($file["shortDescription"]) ? $file["shortDescription"] : "";
//            $filemodel->language  = "english";
//            $filemodel->downloadable  = true;
//            $filemodel->save();
////            echo "File Upload $i \n";
//        }

    });

    $router->get('/author',function (){
        $json_url = public_path("authors.json");
        $json = (json_decode(file_get_contents($json_url),true));
//        $json = sizeof(json_decode(file_get_contents($json_url),true));

        foreach ($json as $auth){
          $author = new \App\Models\Author();
          $author->name = $auth["author"];
          $author->save();
        }

    });

});



