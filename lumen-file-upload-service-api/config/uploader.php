<?php

return [

    'uploader_folder' => storage_path() . '/uploader',

    'temp_folder' => '/temp',

    'fine_uploader' => [

        'allowed_extensions' => [],
        'size_limit'    => 20*1024*1024, // 20 Mb
        'input_name'    => 'qqfile',
        'chunks_folder' => '/chunks'

    ],

    // Can be overridden by client
    'thumbnails' => [
        'height' => 100,
        'width' => 100,
        'crop' => 'fill'
    ],

    'storage' => 'cloudinary',

    'storage_url_resolver' => [
        'class' => Optimus\FineuploaderServer\Http\CloudinaryUrlResolver::class
    ],

    'success_response_class' => Optimus\FineuploaderServer\Response\OptimusResponse::class,

    'storages' => [

        'local' => [
            'class' => Optimus\FineuploaderServer\Storage\LocalStorage::class,
            'config' => [
                'root_folder' => storage_path() . '/uploader'
            ]
        ],

        'cloudinary' => [
            'class' => Optimus\FineuploaderServer\Storage\CloudinaryStorage::class,
            'config' => [
                'cloud_name' => env('CLOUDINARY_CLOUD_NAME', 'cloud_name'),
                'api_key' => env('CLOUDINARY_API_KEY'),
                'api_secret' => env('CLOUDINARY_API_SECRET')
            ]
        ]

    ],

    'naming_strategy' => Optimus\FineuploaderServer\Naming\UniqidStrategy::class,

    'middleware' => [

    ]

];
