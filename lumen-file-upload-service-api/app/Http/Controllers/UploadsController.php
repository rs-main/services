<?php

namespace App\Http\Controllers;

use App\Classes\Statistics;
use App\Helpers\ThumbImage;
use App\Jobs\FlushJob;
use App\Jobs\IndexJob;
use App\Jobs\UploadJob;
use App\Models\Bookmark;
use App\Models\FileModel;
use App\Models\Query;
use App\Models\SearchQuery;
use App\Traits\ConsumeExternalService;
use Aws\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class UploadsController  extends Controller
{
    use ConsumeExternalService;

    private $storage;
    private $s3_url;

    public function __construct()
    {
        $this->s3_url = "https://rs-file-service.s3.af-south-1.amazonaws.com/";
        $this->storage = Storage::disk("s3");
    }

    public function index(Request $request){

        $query = $request->query("field");
        $value = $request->query("value");
        $limit = $request->has("limit") ? $request->query("limit") : 5;
        $page  = $request->has("page") ? $request->query("page") : 1;

        $builder = FileModel::query();

        if ($request->has("client")){
            $builder->where("service", $request->query("client"));
        }

        switch ($query){
            case "extension":
                return DataTables::of($builder->whereExtension($value)->orderByDesc("created_at")->get())->make(true);

            case "type":
                return DataTables::of($builder->whereType($value)->orderByDesc("created_at")->get())->make(true);

            case "all":
            case "recent":
                return $this->recentQuery($page, $limit, $builder);

            case "featured":
                return $this->featuredQuery($page, $limit, $builder, $value);

            case "downloads":
                return $this->ByDownloadsQuery($page, $limit, $builder);

            case "file-type":
                return $this->ByFileTypeQuery($page, $limit, $builder, $value);

            case "subject":
                return $this->BySubjectQuery($page, $limit, $builder, $value);


            case "class":
                return $this->ByClassQuery($page, $limit, $builder, $value);


            case "paid":
                return $this->ByPaidQuery($page, $limit, $builder, $value);

            default:
                return DataTables::of($builder->orderByDesc("created_at")->get())->make(true);
        }


    }

    public function create(Request $request)
    {

        $this->validateRequest($request);
        $upload_types = $this->getUploadTypes();

        if (!array_key_exists($request->type,$upload_types)){
            return response()->json(["message" => "Resource type not valid! Choose from books, audiobooks,articles,videos"],400);
        }

        try {

//          DB::beginTransaction();
            $file_model = $this->saveToFileModel($request);

            $levels = explode(",",$request->get("levels"));

            if ($file_model) {
                foreach ($levels as $level) {
                    DB::table("files_levels")->insert([
                        "level_id" => $level,
                        "file_id" => $file_model->id
                    ]);
                }

                $job = new UploadJob($file_model->id,$file_model->user_id,$file_model->subject_id);
                $this->dispatch($job);
            }

//            DB::commit();

        }catch (\Exception $exception){
            return response()->json(["message" => $exception->getMessage()]);
        }

        return response()->json(["message" => "success"],200);
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id){

        $this->validateRequest($request);

        FileModel::find($id)->update([
            "name" => $request->name,
            "service_id" => 1,
            "service" => $request->service,
            "storage" => "s3",
            "file_url" => $request->book_url,
            "file_size" => $request->book_size,
            "extension" => $request->book_extension,
            "cover_art_url" => $request->cover_art_url,
            "cover_art_size" => $request->cover_art_size,
            "cover_art_extension" => $request->cover_art_extension,
            "isbn" => $request->isbn,
            "year" => $request->year,
            "class_id" => $request->class_id,
            "subject_id" => $request->subject_id,
            "user_id" => $request->user_id,
            "page_count" => $request->page_count,
            "meta_info" => json_encode($request->meta_info),
            "type" => $request->type,
            "tags" => $request->tags,
            "downloads" => 0,
            "downloadable" => $request->downloadable,
            "language"     => $request->language,
            "description" => $request->get("description"),
            "author_id" => $request->author_id
        ]);
    }

    public function uploadOnlyFile(Request $request){

        $this->validate($request,
            [
                "file"=>"required|file",
                "resource_type" => "required",
                "image_type"=>"required"
            ]);

        if (!array_key_exists($request->resource_type,$this->getUploadTypes())){
            return response()->json(["message" => "Resource type not valid! Choose from books,audiobooks,articles,videos"],400);
        }

        $mimeType = $request->get('resource_type');
        $imageType = $request->get('image_type');
        $file = $request->file('file');
        $file_name = str_replace([" ",",","(",")","+"],"-",($file->getClientOriginalName()));
        $path = $this->getPath($mimeType,$imageType);
        $file_url = $this->s3_url.$path.$file_name;
        $imageSize = $file->getSize();
        $file_size = number_format($imageSize / 1048576,2);

        if ($file_size){

        }

//        $uploader = new MultipartUploader($this->storage->getDriver()->getAdapter()->getClient(),
//            ($request->file('file')->getRealPath()), [
//                'bucket' => env("AWS_BUCKET"),
//                'key'    => $path.$file_name,
//                'visibility' => 'public'
//        ]);
//
//        try {
//            $result = $uploader->upload();
//        } catch (MultipartUploadException $e) {
//            echo $e->getMessage();
//        }

        $this->uploadFile($path, $file_name, $request);

        $this->indexModel();

        return $this->response(
            [
                "response" => [],
                "url" => $file_url,
                "extension" => $file->getClientOriginalExtension(),
                "size" => $file_size,
            ],200);
    }

    public function show(Request $request, $id){
        $this->validate($request,["search_term" => "required"]);
        $file = FileModel::find($id);

        if ($file){
            $this->addToSearchQuery($file,$request->get("search_term"));
            return response()->json(["message" => "success","data" => $file,"other"=>$request->get("search_term")]);
        }

        return response()->json(["message" => "empty", "data" => null]);
    }

    public function delete(Request $request, $id){
        $file_model = FileModel::find($id);
        if ($file_model)
            $file_model->delete();
            $file_url = $request->file_url;
            $this->storage->delete($file_url);

        return $this->response($file_url,200);
    }

    public function getUserBookmarks(Request $request){
        $bookmark = Bookmark::with("file:id,name,file_url")
            ->where("user_id",$request->query("user_id"))->get();
        return $this->response($bookmark,200);
    }

    public function getBookmark($user_id, $file_id){
        $bookmark = Bookmark::with("file:id,name,file_url")->where("user_id",$user_id)->where("file_id",$file_id)->first();
        return $this->response($bookmark,200);
    }

    public function bookMarkPage(Request $request){
        $this->validate($request,[
            "file_id" => "required",
            "user_id" => "required",
            "page_number" => "required"
        ]);

        $bookmark = Bookmark::where("file_id",$request->file_id)->where("user_id", $request->user_id)->first();

        if ($bookmark){
            $bookmark->update(["page_number" => $request->page_number]);
        }else{
            $bookmark = DB::table("bookmarks")->insert([
                "user_id" => $request->user_id,
                "file_id" => $request->file_id,
                "page_number" => $request->page_number
            ]);
        }

        return $this->response($bookmark,200);

    }

    /**
     * @param $mimeType
     * @return string
     */
    public function getPath($mimeType, $image_type): string
    {
        $file_dest = $image_type == "cover-arts" ? "cover-arts":"files";

        $books_path = "gkb/books/$file_dest/";
        $audio_books_path="gkb/audiobooks/$file_dest/";
        $videos_path="gkb/videos/$file_dest/";
        $articles_path="gkb/articles/$file_dest/";

        $path="";
        switch ($mimeType) {
            case "books":
                $path = $books_path;
                break;

            case "audiobooks":
                $path = $audio_books_path;
                break;

            case "articles":
                $path = $articles_path;
                break;

            case "videos":
                $path = $videos_path;
                break;
        }
        return $path;
    }

    /**
     * @param string $path
     * @param string $file_name
     * @param Request $request
     */
    public function uploadFile(string $path, string $file_name, Request $request): string
    {
        return $this->storage->put($path.$file_name,
            file_get_contents($request->file('file')->getRealPath()), [
                'visibility' => 'public'
            ]);
    }

    public function getUpload($id){
        $file_model = FileModel::find($id);

        return response()->json(["data" => $file_model]);
    }

    /**
     * @param Request $request
     */
    public function saveToFileModel(Request $request)
    {
        $fileModel = FileModel::create([
            "name" => $request->name,
            "service" => $request->service,
            "service_id" => 1,
            "storage" => "s3",
            "file_url" => $request->book_url,
            "file_size" => $request->book_size,
            "extension" => $request->book_extension,
            "cover_art_url" => $request->cover_art_url,
            "cover_art_size" => $request->cover_art_size,
            "cover_art_extension" => $request->cover_art_extension,
            "isbn" => $request->isbn,
            "year" => $request->year,
            "class_id" => $request->class_id,
            "subject_id" => $request->subject_id,
            "user_id" => $request->user_id,
            "page_count" => $request->page_count,
            "meta_info" => json_encode($request->meta_info),
            "type" => $request->type,
            "tags" => $request->tags,
            "downloads" => 0,
            "downloadable" => $request->downloadable,
            "language"     => $request->language,
            "description" => $request->get("description"),
            "author_id" => $request->author_id
        ]);

        return $fileModel;
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Http\JsonResponse
     */
    public function recentQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->selectRaw("files.*")->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function featuredQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder, $value): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->whereFeatured($value)->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Http\JsonResponse
     */
    public function ByDownloadsQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->orderByDesc("downloads");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function ByFileTypeQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder, $value): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->whereType($value)->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function BySubjectQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder, $value): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->whereSubjectId($value)->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function ByClassQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder, $value): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->whereClassId($value)->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }

    /**
     * @param $page
     * @param $limit
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function ByPaidQuery($page, $limit, \Illuminate\Database\Eloquent\Builder $builder, $value): \Illuminate\Http\JsonResponse
    {
        $skipped = $page == 1 ? 0 : $page * $limit;
        $builder = $builder->wherePaid($value)->orderByDesc("created_at");
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        return response()->json(["total" => $count, "data" => $data]);
    }


    /**
     * @return string[]
     */
    public function getUploadTypes(): array
    {
        $upload_types = [
            "books"      => "books",
            "articles"   => "articles",
            "audiobooks" => "audiobooks",
            "videos"     => "videos"
        ];
        return $upload_types;
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateRequest(Request $request): void
    {
        $this->validate($request, [
            'name'           => 'required|unique:files',
            'type'           => 'required',
            'book_url'       => 'required',
            'book_size'      => 'required',
            'cover_art_url'  => 'required',
            'cover_art_size' => 'required',
            'user_id'        => 'required',
            'subject_id'     => 'required',
            'service'     => 'required',
            "levels"         => 'required',
            "downloadable"   => 'required'
        ]);
    }

    public function indexModel(){
        $job = new IndexJob();
        $this->dispatch($job);
    }

    public function flushModel(){
        $job = new FlushJob();
        $this->dispatch($job);
    }

    public function getStats(Request $request){
        $this->validate($request, ["client" => "required"]);
        $uploadStats = new Statistics($request->query("client"));
        $total_counts = $uploadStats->totalCounts();
        $total_books = $uploadStats->getResourceCounts(Statistics::BOOKS);
        $total_audio_books = $uploadStats->getResourceCounts(Statistics::AUDIOBOOKS);
        $total_videos = $uploadStats->getResourceCounts(Statistics::VIDEOS);
        $total_downloads = $uploadStats->getResourceDownloads();
        $queries = $uploadStats->queries();
        $monthly_impressions = $uploadStats->byMonthlyQueries(Statistics::IMPRESSION);
        $monthly_clicks = $uploadStats->byMonthlyQueries(Statistics::CLICKED);
         $monthly_ctr = $uploadStats->monthlyCTR();

        return response()->json([
            "total_counts" => $total_counts,
            "total_books" => $total_books,
            "total_audio_books" => $total_audio_books,
            "total_videos" => $total_videos,
            "total_downloads" => $total_downloads,
            "queries" => $queries,
            "monthly_impressions" => $monthly_impressions,
            "monthly_clicks" => $monthly_clicks,
            "monthly_ctr_percentage" => $monthly_ctr
        ]);
    }

    public function getSearchQueries(Request $request){
        $this->validate($request, ["client" => "required"]);
        $uploadStats = new Statistics($request->query("client"));
        $queries = $uploadStats->queries();
        $monthly_impressions = $uploadStats->byMonthlyQueries(Statistics::IMPRESSION);
        $monthly_clicks = $uploadStats->byMonthlyQueries(Statistics::CLICKED);

        return response()->json([
            "queries" => $queries,
            "monthly_impressions" => $monthly_impressions,
            "monthly_clicks" => $monthly_clicks
        ]);

    }

    /**
     * @param $file
     */
    public function addToSearchQuery($file,$search_term): void
    {
        $query = Query::where("name",$search_term)->first();

        if ($query){
            $query_id = $query->id;
        }else{
            $query_object = Query::create([
                "name" => $search_term
            ]);
            $query_id = $query_object->id;
        }

        SearchQuery::create([
            "file_id" => $file->id,
            "type" => "clicked",
            "query_id" => $query_id
        ]);
    }
}
