<?php

namespace App\Http\Controllers;

use App\Models\ClientStorage;
use App\Models\FileModel;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class StorageController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {
    }

    public function index(Request $request)
    {

    }

    public function show(Request $request){

        $this->validate($request, [
            "client" => "required"
        ]);

        $fileModelBuilder = FileModel::whereClient($request->client);
        $sum_of_client_files = $fileModelBuilder->sum("file_size") + $fileModelBuilder->sum("cover_art_size");
        $client_storage =    ClientStorage::where("client", $request->client)->first();
        $storage_left = 0;
        $original_storage=0;
        if ($client_storage){
            $original_storage = $client_storage->storage_capacity_in_mb;
            $storage_left = $original_storage-$sum_of_client_files;
        }

        $storage_left_percentage = $sum_of_client_files/$original_storage *100;

        return response()->json(["used" => ceil($sum_of_client_files),"left" => ceil($storage_left), "used_percentage" => ceil($storage_left_percentage) ]);
    }

    public function addClientStorage(Request $request){
        $this->validate($request, [
            "client" => "required",
            "storage" => "required"
        ]);


        $client_storage = ClientStorage::whereClient($request->get("client"))->first();
        if ($client_storage){
            $client_storage->update([
                "storage_capacity_in_mb" => $request->get("storage")
            ]);
        }else{
            ClientStorage::create([
                "client" => $request->get("client"),
                "storage_capacity_in_mb" => $request->get("storage")
            ]);
        }

        return response()->json(["message" => "success"]);
    }

}
