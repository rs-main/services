<?php

namespace App\Http\Controllers;

use App\Jobs\ImpressionsJob;
use App\Models\FileModel;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;

class SearchController  extends Controller
{
    use ConsumeExternalService;

    private $storage;
    private $s3_url;

    public function __construct()
    {
    }

    public function search(Request $request){

        $q = $request->query("q");
        $results = FileModel::search($q)->take(20)->get();
        $job = new ImpressionsJob(substr($q,0,-1));
        $this->dispatch($job);
        return $this->response($results,200);
    }

    public function index(Request $request)
    {

    }

}
