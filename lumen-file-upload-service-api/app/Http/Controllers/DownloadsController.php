<?php

namespace App\Http\Controllers;

use App\Jobs\UploadJob;
use App\Models\Download;
use App\Models\FileModel;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class DownloadsController  extends Controller
{
    use ConsumeExternalService;

    private $storage;
    private $s3_url;

    public function __construct()
    {
        $this->s3_url = "https://rs-file-service.s3.af-south-1.amazonaws.com/";
        $this->storage = Storage::disk("s3");
    }

    public function getBag($user_id){
        $downloads = Download::with("file")->whereUserId($user_id)->whereType("bag")->get();
        return $this->response($downloads,200);
    }

    public function sendToBag($id,Request $request)
    {
        $this->validate($request,[
            "file_id" => "required",
            "user_id"  => "required"
        ]);

        DB::transaction(function ()use ($request){
            $download = Download::whereFileId($request->file_id)->whereUserId($request->user_id)->first();
            $this->saveToDownloads($download, $request,"bag");
        });
        return response()->json(["message" => "success"],200);
    }

    public function getFavourites($user_id){
        $downloads = Download::with("file")->whereUserId($user_id)->whereType("favourite")->get();
        return $this->response($downloads,200);
    }

    public function addToFavourites(Request $request){
        $this->validate($request,[
            "file_id" => "required",
            "user_id"  => "required"
        ]);

        DB::transaction(function ()use ($request){
            $download = Download::whereFileId($request->file_id)->whereUserId($request->user_id)->first();
            $this->saveToDownloads($download, $request,"favourite");
        });
        return response()->json(["message" => "success"],200);
    }


    public function download($id){
        $file = FileModel::find($id);
        if ($file && $file->downloadable){
            $this->updateDownloadCounts($file);
            return response()->json(["data" => $file, "message" => "success"],200);
        }

        return response()->json(["data" => null,"message" => "file is not downloadable!"],403);
    }



    public function delete($id){
        $download = Download::find($id);

        if ($download){
            $download->delete();
            return response()->json(["message" => "success"]);
        }

        return response()->json(["message" => "download doesn't exist!"]);

    }

    /**
     * @param $file
     */
    public function updateDownloadCounts($file): void
    {
        if ($file) {
            $download_counts = $file->downloads + 1;
            $file->update([
                "downloads" => $download_counts
            ]);
        }
    }

    /**
     * @param $download
     * @param Request $request
     */
    public function saveToDownloads($download, Request $request, $type): void
    {
        if (!$download) {
            Download::create([
                "file_id" => $request->file_id,
                "user_id" => $request->user_id,
                "type" => $type
            ]);
            $file = FileModel::find($request->file_id);

            $this->updateDownloadCounts($file);
        }
    }


}
