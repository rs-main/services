<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function response($json, $status){
        return response()->json(["data" => $json],$status);
    }
}
