<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;

class AuthorController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $builder = Author::query();

        if ($request->has("q")){
            $search_query = strtolower(trim($request->query("q")));
            $builder->where("name", "LIKE","%".$search_query."%");
        }

        $skipped = $page == 1 ? 0 : $page * $limit;
        $count = $builder->count();
        $data = $builder->skip($skipped)->limit($limit)->get();
        $last_page = ceil($count/$limit);

        return response()->json(["total" => $count, "data" => $data, "limit" => $limit, "last_page" => $last_page]);
    }


    public function store(Request $request){
        $this->validate($request,["name" => "required"]);
        $author = Author::create($request->only("name"));
        return response()->json(["data" => $author]);
    }

    public function show($id){
        $author = Author::find($id);
        return response()->json(["data" => $author],200);
    }


}
