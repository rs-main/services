<?php

namespace App\Classes;

use App\Models\FileModel;
use App\Models\Query;
use App\Models\SearchQuery;
use Illuminate\Support\Facades\DB;

class Statistics
{

    const CLICKED = "clicked";
    const IMPRESSION = "impression";
    const QUERY_LIMIT = 10;
    const BOOKS = "books";
    const AUDIOBOOKS = "audiobooks";
    const VIDEOS = "videos";

    private $service;

    public function __construct($service)
    {
        $this->service = $service;
    }

    public function totalCounts(){
        return FileModel::where("client",$this->service)
            ->count();
    }

    public function getResourceCounts($type){
        return FileModel::where("client",$this->service)
            ->where("type",$type)->select("id","type")->count();
    }

    public function getResourceDownloads($type=""){
        $downloads = FileModel::where("client",$this->service)
            ->orWhere("type",$type)->select("id","type")->sum("downloads");
        return intval($downloads);
    }

    public function queries(){
        return Query::withCount(["clicks" => function($clicked){
            $clicked->where("type",self::CLICKED);
        }])->withCount(["impressions" => function($sq){
            $sq->where("type", self::IMPRESSION)->orderBy("impressions_count","desc");
        }] )->selectRaw("name as query")
            ->orderBy("impressions_count","desc")
            ->take(self::QUERY_LIMIT)->get();
    }

    public function byMonthlyQueries($type){
        return SearchQuery::where("type",$type)
            ->select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy('month')
            ->get();
    }

    public function monthlyCTR(){
        $impressions_count = SearchQuery::where("type",self::IMPRESSION)->count();
        $clicks_count = SearchQuery::where("type",self::CLICKED)->count();
        return floatval(number_format(($clicks_count/$impressions_count)*100,2));
    }

}
