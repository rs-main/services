<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class PaymentService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $make_payment_url;
    private  $verify_payment_url;

    public function __construct()
    {
        $this->base_url = config("services.payments.base_url");
        $this->secret = config("services.payments.secret");
        $this->make_payment_url = config("services.payments.endpoints.make_payment_url");
        $this->verify_payment_url = config("services.payments.endpoints.verify_payment_url");
    }

    public function makePayment($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->make_payment_url,$options,$headers);
    }

    public function verifyPayment($reference){
        $verify_payment_url = $this->verify_payment_url.$reference;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$verify_payment_url,[],[]);
    }

    public function submitOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->make_payment_url,$options,$headers);
    }
}
