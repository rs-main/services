<?php

namespace App\Libraries\Eureka\Exceptions;


class HeartbeatFailureException extends EurekaClientException {
}
