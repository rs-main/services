<?php

namespace App\Libraries\Eureka\Exceptions;


class InstanceFailureException extends EurekaClientException {
}
