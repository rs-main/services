<?php

namespace App\Jobs;

use App\Models\FileModel;
use App\Services\SubjectService;
use App\Services\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UploadJob implements ShouldQueue
{
    use  InteractsWithQueue, Queueable;

    private $file_id;
    private $user_id;
    private $subject_id;
    private $user_service;
    private $subject_service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_id,$user_id,$subject_id)
    {
        $this->file_id         = $file_id;
        $this->user_id         = $user_id;
        $this->subject_id      = $subject_id;
        $this->user_service    = new UserService();
        $this->subject_service = new SubjectService();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file_builder =   FileModel::find($this->file_id);

        if ($this->user_id != null){
            $file_builder->update([
                "user_object" => $this->getUserObject($this->user_id)
            ]);
        }

        if ( $this->subject_id != null){
            $file_builder->update([
                "subject_object" => $this->getSubjectObject($this->subject_id)
            ]);
        }
    }

    private function getUserObject($id){
        $response =  ($this->user_service->getUser($id))["response"];
        return ($response["user"]) ?? null;
    }

    private function getSubjectObject($id){
        $response = $this->subject_service->getSubject($id)["response"];
        return ($response["data"]) ?? null;
    }

}
