<?php

namespace App\Jobs;

use App\Models\FileModel;
use App\Models\Query;
use App\Models\SearchImpression;
use App\Models\SearchQuery;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImpressionsJob extends Job implements ShouldQueue
{
    private $results;
    private $term;
    public function __construct($term)
    {
        $this->results = FileModel::search($term)->get();
        $this->term = $term;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $query = Query::firstOrCreate([
            'name' => $this->term
        ]);

        if (count($this->results) > 0) {
            foreach ($this->results as $result) {
                    SearchQuery::create([
                        "file_id" => $result->id,
                        "type" => "impression",
                        "query_id" => $query->id
                    ]);
            }
        }

    }


}
