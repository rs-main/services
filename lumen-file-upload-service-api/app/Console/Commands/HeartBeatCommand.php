<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use App\Payment\Api\V1\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class HeartBeatCommand extends Command
{

    protected $signature = 'send:heart-beat';


    protected $description = 'Send Heartbeat to Eureka';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->sendHeartBeat();
    }

    private function sendHeartBeat(){

        $name = "upload-service";
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' => $name,
            "appName" => $name,
            'port' => ["8003", true],

        ]);

        $client = new EurekaClient($config);
        return $client->heartbeat();

    }
}
