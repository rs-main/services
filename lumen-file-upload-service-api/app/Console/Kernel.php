<?php

namespace App\Console;

use App\Console\Commands\HeartBeatCommand;
use App\Console\Commands\RegisterServiceCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        Commands\ServeCommand::class,
        Commands\HeartBeatCommand::class,
        Commands\RegisterServiceCommand::class,
//        Commands\SubscribeToPaymentChannel::class,
//        Commands\PublishPaymentChannel::class,
    ];


    protected function schedule(Schedule $schedule)
    {
        $schedule->command(RegisterServiceCommand::class);
        $schedule->command(HeartBeatCommand::class)->everyMinute();
    }
}
