<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Scout\Searchable;

class FileModel extends Model
{
    use HasFactory,Searchable;
    protected $table = "files";

    protected $guarded = [];

    protected $casts = ["tags" => "array" , "subject_object" => "array", "user_object" => "array"];

    public function toSearchableArray()
    {
        $array = $this->only('id', 'name');
        return ($array);
    }
}
