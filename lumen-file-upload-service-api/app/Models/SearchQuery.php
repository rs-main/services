<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchQuery extends Model
{
    use HasFactory;
    protected $table = "search_queries";

    protected $guarded = [];

    public function file(){
        return $this->belongsTo(FileModel::class);
    }

}
