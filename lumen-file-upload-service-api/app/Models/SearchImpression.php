<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchImpression extends Model
{
    use HasFactory;
    protected $table = "search_impressions";

    protected $guarded = [];

}
