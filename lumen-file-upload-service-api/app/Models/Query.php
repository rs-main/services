<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    use HasFactory;
    protected $table = "queries";

    protected $guarded = [];

    public function impressions(){
        return $this->hasMany(SearchQuery::class);
    }

    public function clicks(){
        return $this->hasMany(SearchQuery::class);
    }
}
