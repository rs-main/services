<?php

namespace Database\Seeders;

use App\Models\FileModel;
use App\Models\Service;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call('UsersTableSeeder');

//        if (!Service::find(1)){
//            Service::create([
//                "name" => "GKB",
//                "service_code" => "001",
//                "service_url" => "localhost",
//                "tags" => "gkb,uploads"
//            ]);
//        }

        FileModel::truncate();
        for ($i = 0; $i < 10; $i++){
            $filemodel = new FileModel();
            $filemodel->service_id = 1;
            $filemodel->name = "pdf document- $i";
            $filemodel->storage = "s3";
            $filemodel->file_url = "https://rs-file-service.s3.af-south-1.amazonaws.com/gkb/books/files/1631686316f39ea540-0aac-11ec-875a-f5239025e0a7.pdf";
            $filemodel->extension = "pdf";
            $filemodel->type = "books";
            $filemodel->save();
            echo "File Upload $i \n";
        }

//        Artisan::call("scout:flush",["model" => "App\\Models\\FileModel" ]);
//        Artisan::call("scout:import",["model" => "App\\Models\\FileModel" ]);
    }
}
