<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_storage', function (Blueprint $table) {
            $table->id();
            $table->string("client");
            $table->bigInteger("storage_capacity_in_mb")->default(0);
            $table->bigInteger("used_storage_in_mb")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_storage');
    }
}
