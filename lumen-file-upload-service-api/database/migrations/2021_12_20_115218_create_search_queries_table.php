<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_queries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("file_id")->index();
            $table->foreign("file_id")->references("id")->on("files")->onDelete("cascade");
            $table->string("type",20);
            $table->unsignedBigInteger("user_id")->index()->nullable();
            $table->unsignedBigInteger("query_id");
            $table->foreign("query_id")->references("id")->on("queries")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_queries');
    }
}
