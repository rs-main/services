<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id")->index();
            $table->unsignedBigInteger("resource_id")->index();
            $table->string("resource_type",20);
            $table->unsignedBigInteger("parent_id")->index()->nullable();
            $table->text("comment");
            $table->integer("likes")->default(0);
            $table->integer("dislikes")->default(0);
            $table->boolean("spam")->default(false);
            $table->boolean("sticky")->default(false);
            $table->json("user")->nullable();
            $table->json("parent")->nullable();
            $table->string("service",15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
