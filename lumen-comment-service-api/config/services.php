<?php

use Illuminate\Support\Str;

return [

    'nexmo' => [
        'sms_from' => '233242953672',
    ],

    'discovery'=> [
        "base_url"       => "http://gateway:80",
        "secret"         => env("GATEWAY_SERVICE_SECRET"),
        "endpoints"      =>  [
            "register"   => "/api/register",
            "login"      => "/api/login",
            "profile"         => "/api/profile"
        ]
    ],

    'users' => [
        'base_url' => "http://nginx:80",
        "endpoints" => [
            'users_url' => "/api/users/",
        ],
        "secret"   => env("USER_SERVICE_SECRET")
    ],

    'notifications' => [
        'base_url' => "http://notifications:8005",
        "endpoints" => [
            'notifications_url' => "/api/notifications/user",
        ],
        "secret"   => env("USER_SERVICE_SECRET")
    ],
];
