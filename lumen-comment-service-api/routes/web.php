<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    // Get Comments.
    $router->get("/comments", "CommentsController@index");

    // Show Comment
    $router->get("/comments/{id}", "CommentsController@show");

    // Store Comment
    $router->post("/comments", "CommentsController@store");

    // Delete Comment
    $router->post("/comments/{id}", "CommentsController@delete");

    //Like Comment
    $router->post("/comments/likes/{user_id}/{comment_id}", "CommentsController@likeComment");

    //Dislike Comment
    $router->post("/comments/dislikes/{user_id}/{comment_id}", "CommentsController@dislikeComment");

    // Report Comment
    $router->post("/comments/reports/report", "CommentsController@reportComment");

    // Hides Comment
    $router->post("/comments/reports/hide", "CommentsController@hideComment");

    // Chat or Comment Participants
    $router->get("/comments/participants/list","CommentsController@getParticipants");

    // Notify Participants
    $router->post("/comments/participants/notify","CommentsController@notifyParticipants");
});




