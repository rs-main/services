<!DOCTYPE html>
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head><title></title>
    <meta charset="utf-8"/>
    <meta content="width=device-width,initial-scale=1" name="viewport"/>
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
    </xml><![endif]--><!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
    <!--<![endif]-->
    <style>* {
        box-sizing: border-box
    }

    th.column {
        padding: 0
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: inherit !important
    }

    #MessageViewBody a {
        color: inherit;
        text-decoration: none
    }

    p {
        line-height: inherit
    }

    @media (max-width: 620px) {
        .icons-inner {
            text-align: center
        }

        .icons-inner td {
            margin: 0 auto
        }

        .row-content {
            width: 100% !important
        }

        .image_block img.big {
            width: auto !important
        }

        .stack .column {
            width: 100%;
            display: block
        }
    }</style>
</head>
<body style="background-color:#e2eace;margin:0;padding:0;-webkit-text-size-adjust:none;text-size-adjust:none">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation"
       style="mso-table-lspace:0;mso-table-rspace:0;background-color:#e2eace" width="100%">
    <tbody>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content"
                               role="presentation" style="mso-table-lspace:0;mso-table-rspace:0" width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="image_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="padding-top:30px;width:100%;padding-right:0;padding-left:0">
                                                <div align="center" style="line-height:10px"><img alt="Image"
                                                                                                  class="big"
                                                                                                  src="{{url("images/rounder-up.png")}}"
                                                                                                  style="display:block;height:auto;border:0;width:600px;max-width:100%"
                                                                                                  title="Image"
                                                                                                  width="600"/></div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content"
                               role="presentation" style="mso-table-lspace:0;mso-table-rspace:0;background-color:#fff"
                               width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="image_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="width:100%;padding-right:0;padding-left:0;padding-top:5px">
                                                <div align="center" style="line-height:10px"><img alt="Image"
                                                                                                  src="{{url("images/4ccdd621659342935a1460a1303c0ba1.jpeg")}}"
                                                                                                  style="display:block;height:auto;border:0;width:250px;max-width:100%"
                                                                                                  title="Image"
                                                                                                  width="250"/></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td style="padding-bottom:15px;padding-left:10px;padding-right:10px;padding-top:10px">
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#555;line-height:1.5;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:14px;text-align:center">

                                                            {{$content}}
{{--                                                            I'm a new--}}
{{--                                                            Text block ready for your content.--}}

                                                        </p></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content"
                               role="presentation" style="mso-table-lspace:0;mso-table-rspace:0;background-color:#fff"
                               width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="100%">
                                    <table border="0" cellpadding="10" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td>
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#0d0d0d;line-height:1.2;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:14px;text-align:center"><span
                                                            style="font-size:28px"><strong><span style="font-size:28px">Hello Username,</span></strong></span><br/><span
                                                            style="font-size:28px">Registration completed</span></p>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" class="image_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="width:100%;padding-right:0;padding-left:0">
                                                <div align="center" style="line-height:10px"><img alt="Image"
                                                                                                  src="{{url("images/divider.png")}}"
                                                                                                  style="display:block;height:auto;border:0;width:316px;max-width:100%"
                                                                                                  title="Image"
                                                                                                  width="316"/></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="10" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td>
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#555;line-height:1.5;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:14px;text-align:center;mso-line-height-alt:21px">
                                                            Thanks so much for joining Our Site!<br/>Your username is:
                                                            <span
                                                                style="color:#a8bf6f;font-size:14px"><strong>TestUser<br/></strong></span>
                                                        </p></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td style="padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:20px">
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#0d0d0d;line-height:1.5;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:14px;text-align:center">TO FINISH
                                                            SIGNING UP AND ACTIVATE YOUR ACCOUNT<br/>
                                                            YOU JUST NEED TO SET YOUR PASSWORD</p></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" class="button_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="padding-bottom:55px;padding-left:10px;padding-right:10px;padding-top:25px">
                                                <div align="center">
                                                    <!--[if gte mso 12]>
                                                    <style>div.btnw {
                                                        display: block !important
                                                    }</style>
                                                    <div class="btnw" style="display:none">
                                                        <a:roundrect xmlns:a="urn:schemas-microsoft-com:vml"
                                                                     xmlns:w="urn:schemas-microsoft-com:office:word"
                                                                     style="v-text-anchor:middle;width:223px;height:62px;"
                                                                     arcsize="7%" stroke="false" fillcolor="#a8bf6f">
                                                            <w:anchorlock/>
                                                            <v:textbox inset="0px,0px,0px,0px">
                                                                <center
                                                                    style="font-family:sans-serif;color:#ffffff;font-size: 16px;">
                                                                    ACTIVATE MY ACCOUNT
                                                                </center>
                                                            </v:textbox>
                                                        </a:roundrect>
                                                    </div><![endif]-->
                                                    <!--[if !mso]><!--><span
                                                    style="background-color:#a8bf6f;border-radius:4px;color:#fff;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:200%;width:auto;display:inline-block;text-align:center;text-decoration:none"><div
                                                    style="padding-top:15px;padding-right:15px;padding-bottom:15px;padding-left:15px"><div
                                                    style="font-size:12px;line-height:24px"><p
                                                    style="margin:0;font-size:16px;line-height:32px">ACTIVATE MY ACCOUNT</p></div></div>
</span><!--<![endif]--></div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content"
                               role="presentation"
                               style="mso-table-lspace:0;mso-table-rspace:0;background-color:#525252" width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="33.333333333333336%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="social_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="padding-top:15px;text-align:center;padding-right:0;padding-left:0">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                       class="social-table" role="presentation"
                                                       style="mso-table-lspace:0;mso-table-rspace:0" width="111px">
                                                    <tr>
                                                        <td style="padding:0 5px 0 0"><a
                                                            href="https://www.facebook.com/" target="_blank"><img
                                                            alt="Facebook" height="32" src="{{url("images/facebook2x.png")}}"
                                                            style="display:block;height:auto;border:0" title="Facebook"
                                                            width="32"/></a></td>
                                                        <td style="padding:0 5px 0 0"><a href="https://twitter.com/"
                                                                                         target="_blank"><img
                                                            alt="Twitter" height="32" src="{{url("images/twitter2x.png")}}"
                                                            style="display:block;height:auto;border:0" title="Twitter"
                                                            width="32"/></a></td>
                                                        <td style="padding:0 5px 0 0"><a href="https://plus.google.com/"
                                                                                         target="_blank"><img
                                                            alt="Google+" height="32" src="{{url("images/googleplus2x.png")}}"
                                                            style="display:block;height:auto;border:0" title="Google+"
                                                            width="32"/></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="33.333333333333336%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td style="padding-top:25px;padding-bottom:5px">
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#a8bf6f;line-height:1.2;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:12px;text-align:center"><span
                                                            style="color:#fff;font-size:12px"><span
                                                            style="font-size:12px;color:#a8bf6f">Tel.:</span> +39 . 000 . 000 . 000</span><br/>
                                                        </p></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="33.333333333333336%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="text_block"
                                           role="presentation"
                                           style="mso-table-lspace:0;mso-table-rspace:0;word-break:break-word"
                                           width="100%">
                                        <tr>
                                            <td style="padding-top:25px;padding-bottom:5px">
                                                <div style="font-family:sans-serif">
                                                    <div
                                                        style="font-size:12px;color:#a8bf6f;line-height:1.2;font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
                                                        <p style="margin:0;font-size:12px;text-align:center">Email <span
                                                            style="color:#fff;font-size:12px">mail@example.com</span>
                                                        </p></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-5" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content"
                               role="presentation" style="mso-table-lspace:0;mso-table-rspace:0" width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="image_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="width:100%;padding-right:0;padding-left:0;padding-bottom:65px">
                                                <div align="center" style="line-height:10px"><img alt="Image"
                                                                                                  class="big"
                                                                                                  src="{{url("images/rounder-dwn.png")}}"
                                                                                                  style="display:block;height:auto;border:0;width:600px;max-width:100%"
                                                                                                  title="Image"
                                                                                                  width="600"/></div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-6" role="presentation"
                   style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack"
                               role="presentation" style="mso-table-lspace:0;mso-table-rspace:0" width="600">
                            <tbody>
                            <tr>
                                <th class="column"
                                    style="mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top"
                                    width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" class="icons_block"
                                           role="presentation" style="mso-table-lspace:0;mso-table-rspace:0"
                                           width="100%">
                                        <tr>
                                            <td style="color:#9d9d9d;font-family:inherit;font-size:15px;padding-bottom:10px;padding-top:10px;text-align:center">
                                                <table cellpadding="0" cellspacing="0" role="presentation"
                                                       style="mso-table-lspace:0;mso-table-rspace:0" width="100%">
                                                    <tr>
                                                        <td style="text-align:center">
                                                            <!--[if vml]>
                                                            <table align="left" cellpadding="0" cellspacing="0"
                                                                   role="presentation"
                                                                   style="display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <![endif]--><!--[if !vml]><!-->
                                                            <table cellpadding="0" cellspacing="0" class="icons-inner"
                                                                   role="presentation"
                                                                   style="mso-table-lspace:0;mso-table-rspace:0;display:inline-block;margin-right:-4px;padding-left:0;padding-right:0">
                                                                <!--<![endif]-->
                                                                <tr>
                                                                    <td style="text-align:center;padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:6px">
                                                                        <a href="https://www.designedwithbee.com/"><img
                                                                            align="center" alt="Designed with BEE"
                                                                            class="icon" height="32"
                                                                            src="{{url("images/bee.png")}}"
                                                                            style="display:block;height:auto;border:0"
                                                                            width="34"/></a></td>
                                                                    <td style="font-family:Montserrat,Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:15px;color:#9d9d9d;vertical-align:middle;letter-spacing:undefined;text-align:center">
{{--                                                                        <a href="https://www.designedwithbee.com/"--}}
{{--                                                                           style="color:#9d9d9d;text-decoration:none">Designed--}}
{{--                                                                            with BEE</a>--}}
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table><!-- End --></body>
</html>
