<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        "user" => "json",
        "parent" => "json"
    ];

    public function parent()
    {
        return $this->belongsTo(Comment::class,'parent_id')->where('parent_id',0)->with('parent');
    }

    public function replies()
    {
//        return $this->hasMany(Comment::class,'parent_id')->with('replies');
        return $this->hasMany(Comment::class,'parent_id');
    }

    public static function getReplyCounts($id){
        return static::where("parent_id",$id)->count();
    }
}
