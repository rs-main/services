<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class NotificationService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $users_url;
    private  $notifications_url;


    public function __construct()
    {
        $this->base_url                  = config("services.notifications.base_url");
        $this->secret                    = config("services.users.secret");
        $this->notifications_url         = config("services.notifications.endpoints.notifications_url");
    }

    public function Notifications($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->notifications_url,$options, []);
    }

    public function saveNotification($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->notifications_url,$options,$headers);
    }

}
