<?php

namespace App\Http\Controllers;

use App\Jobs\NotificationServiceJob;
use App\Jobs\SendNotification;
use App\Jobs\UserServiceJob;
use App\Models\Comment;
use App\Models\UserSpam;
use App\Traits\NotificationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Queue;
use Illuminate\Validation\Validator;

class CommentsController  extends Controller
{

    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $service = $request->query("service");
        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $skipped = $page == 1 ? 0 : ($page * $limit)-$limit;

        $comments_builder = Comment::whereService($service)
            ->whereResourceId($request->query("resource_id"))
            ->whereSpam(false)
            ->where("parent_id",null);

        if ($request->has("parent_id")) {

            $replies_builder = Comment::whereService($service)
                ->whereResourceId($request->query("resource_id"))
                ->whereSpam(false)
                ->where("parent_id", $request->query("parent_id"));

            if ($request->has("user_id")){
                $replies_builder->whereUserId($request->query("user_id"));
            }

            $replies = $replies_builder->get();

            $collection = new Collection();
            $replies->map(function($reply)use($collection){
                $reply_user = $reply["user"]   != null ? "@".strtolower($reply["user"]["first_name"])
                    ."_".strtolower($reply["user"]["last_name"]): "";

                $collection->push([
                    "name"      => $reply_user,
                    "mentions"  => $reply["parent"] != null ? "@".strtolower($reply["parent"]["first_name"])
                        ."_".strtolower($reply["parent"]["last_name"]): "",
                    "user_id"   => $reply["user_id"],
                    "reply"     => $reply->comment,
                    "type"      => "reply",
                    "date"      => $reply->created_at
                ]);
            });

            $comments = $collection->all();
            $count = $collection->count();
        }else {

            if ($request->has("user_id")){
                $comments_builder->whereUserId($request->query("user_id"));
            }

            if ($request->has("resource_id")){
                $comments_builder->whereResourceId($request->query("resource_id"));
            }

            $count = $comments_builder->count();
            $comments = $comments_builder->skip($skipped)->limit($limit)->orderByDesc("created_at")->get();

            $comment_collection = new Collection();
            $comments->map(function ($comment) use($comment_collection){
                $comment_collection->push([
                    "id" => $comment->id ,
                    "user_id" => $comment->user_id ,
                    "resource_type" => $comment->resource_type ,
                    "parent_id" => $comment->parent_id ,
                    "comment" => $comment->comment ,
                    "likes" => $comment->likes ,
                    "dislikes" => $comment->dislikes ,
                    "spam" => $comment->spam ,
                    "sticky" => $comment->sticky ,
                    "user" => $comment->user ,
                    "parent" => $comment->parent ,
                    "service" => $comment->service ,
                    "created_at" => $comment->created_at ,
                    "reply_counts" => Comment::getReplyCounts($comment->id)
                    ]
                );
            });

            $comments = $comment_collection;
        }

        $last_page = ceil($count/$limit);

        return response()->json(["total" => $count,"data" => $comments, "page" => $page, "limit" => $limit, "last_page" => $last_page],200);

    }

    public function allReports(Request $request){
        $this->validate($request,[
            "service" => "required"
        ]);
    }

    public function reportComment(Request $request){

        $this->validate($request,[
            "user_id" => "required",
            "comment_id" => "required"
        ]);

        $user_spam = UserSpam::where("user_id",$request->user_id)
            ->where("comment_id",$request->comment_id)->first();

        if (!$user_spam)
            UserSpam::create(
                ["user_id" => $request->user_id,
                    "comment_id" => $request->comment_id
                ]);

        return response()->json(["data" => $user_spam,"message" => "success"],200);
    }

    public function hideComment(Request $request){

        $this->validate($request,[
            "comment_id" => "required"
        ]);

        $comment = Comment::find($request->comment_id);

        if ($comment){
            $comment->update([
                "spam" => 1
            ]);
        }

        return response()->json(["data" => $comment,"message" => "success"],200);
    }

    public function notifyParticipants(Request $request){
        $participants =  explode(",",$request->get("user_id"));

        return $participants;

    }

    public function getParticipants(Request $request){
         $comments = Comment::whereResourceId($request->query("resource_id"))
            ->selectRaw("user,parent,user_id")
             ->get();

        $collection = new Collection();
        $comments->map(function($comment)use($collection){
            $username = $comment["user"] != null ? "@" . strtolower($comment["user"]["first_name"]) . "_" . strtolower($comment["user"]["last_name"]) : "";
            if ($comment['user'] != null) {
                $collection->push([
                    "name" => $username,
                    "user_id" => $comment["user_id"],
                ]);
            }
        });

        return $collection->unique("user_id")->values()->all();
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            "user_id" => "required",
            "resource_id" => "required",
            "comment" => "required",
            "resource_type" => "required",
            "service"      => "required"
        ]);

        $comment = Comment::create($request->only(
            [
                "user_id","resource_id","comment",
                "resource_type","parent_id","service"]
        ));

        if ($request->has("parent_id")){
            $comment_object = Comment::find($request->get("parent_id"));
            $parent_user_id = $comment_object ? $comment_object->user_id : null;
            $job = new UserServiceJob($comment->id,$request->user_id,$parent_user_id);

            $options = [
                "service" => "gkb",
                "user_id" => $parent_user_id,
                "source" => "comments",
                "message" => $request->get("comment"),
                "from"   => $request->user_id
            ];

            $notif_job = new NotificationServiceJob($options);
            Queue::push($notif_job);
        }else{
            $job = new UserServiceJob($comment->id,$request->user_id,null);
        }
            Queue::push($job);

        return response()->json(["data" => $comment],200);

    }

    public function show($id){
        $comment = Comment::find($id);
        return response()->json(["data" => $comment]);
    }

    public function userComments(Request $request, $user_id){

        $this->validate($request, [
            "service" => "required",
            "subject_id" => "required",
        ]);

        $service = $request->query("service");
        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $comments_builder = Comment::with("replies");
        $skipped = $page == 1 ? 0 : $page * $limit;

        $comments_builder = $comments_builder
            ->where("subject_id", $request->query("subject_id"))
            ->where("service", $service)
            ->whereUserId($user_id)->skip($skipped)->limit($limit);
        $count = $comments_builder->count();
        $comments = $comments_builder->get();

        return response()->json(["total" => $count,"data" => $comments, "page" => $page, "limit" => $limit],200);
    }

    public function subjectComments(Request $request, $subject_id){

        $this->validate($request, [
            "service" => "required",
        ]);

        $service = $request->query("service");
        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $comments_builder = Comment::with("replies");
        $skipped = $page == 1 ? 0 : $page * $limit;

        $comments_builder = $comments_builder
            ->where("subject_id", $subject_id)
            ->where("service", $service)
            ->skip($skipped)->limit($limit);
            $count = $comments_builder->count();
            $comments = $comments_builder->get();

        return response()->json(["total" => $count,"data" => $comments, "page" => $page, "limit" => $limit],200);
    }

    public function likeComment($user_id,$comment_id){
        $user_like_builder = \DB::table("user_like");
        $exist = $user_like_builder->where("user_id",$user_id)->where("comment_id",$comment_id)->first();

        if (!$exist){
            $user_like_builder->insert([
                "user_id" => $user_id,
                "comment_id" => $comment_id
            ]);

            $comment_builder = Comment::whereUserId($user_id)->whereId($comment_id)->first();

            if ($comment_builder) {
                $comment_builder->update([
                    "likes" => $comment_builder->likes + 1
                ]);
                return response()->json(["data" => $comment_builder]);
            }
        }
    }

    public function unlikeComment($user_id,$comment_id){
        $user_like_builder = \DB::table("user_like")->where("user_id",$user_id);
        $exist = $user_like_builder->where("comment_id",$comment_id)->first();

        if (!$exist){
            $user_like_builder->insert([
                "user_id" => $user_id,
                "comment_id" => $comment_id
            ]);

            $comment_builder = Comment::whereUserId($user_id)->whereId($comment_id)->first();

            if ($comment_builder) {
                $comment_builder->update([
                    "likes" => $comment_builder->dislikes + 1
                ]);

                return response()->json(["data" => $comment_builder]);
            }
        }
    }

    public function delete($id){
        $comment = Comment::find($id);

        if ($comment){
            $comment->delete();
        }

        return response()->json(["data" => $comment]);
    }

}
