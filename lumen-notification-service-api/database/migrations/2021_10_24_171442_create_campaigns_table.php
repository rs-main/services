<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string("sender_id")->nullable();
            $table->integer("counts")->default(0);
            $table->decimal("status")->default(0.0);
            $table->string("service")->nullable();
            $table->json("targets")->nullable();
            $table->boolean("email")->default(false);
            $table->boolean("sms")->default(false);
            $table->boolean("push")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
