<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClientMessagingParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_messaging_parameters', function (Blueprint $table) {
            $table->id();
            $table->string("client", 100)->nullable();
            $table->unsignedBigInteger("parameter_id")->nullable();
            $table->foreign("parameter_id")->references("id")->on("messaging_parameters")
                ->onDelete("cascade");
            $table->boolean("sms")->default(false);
            $table->boolean("email")->default(false);
            $table->boolean("notification")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
