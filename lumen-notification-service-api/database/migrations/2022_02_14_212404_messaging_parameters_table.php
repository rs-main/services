<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MessagingParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messaging_parameters', function (Blueprint $table) {
            $table->id();
            $table->string("service", 100)->nullable();
            $table->string("parameter");
//            $table->boolean("sms")->default(false);
//            $table->boolean("email")->default(false);
//            $table->boolean("notification")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messaging_parameters');
    }
}
