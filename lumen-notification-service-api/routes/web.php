<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/api/notify', ['as' => 'notify', 'uses' => 'NotificationController@notify']);

//$router->get('/api/notifications/sms-balance', 'NotificationController@getSmsAccountBalance');

$router->get('/api/notifications/sms-balance', 'NotificationController@getSmsAccountBalance');

$router->post('/api/notifications/bulk-sms','NotificationController@sendBulkSms');

$router->post('/api/notifications/top-up', 'NotificationController@topUpSmsCredits');

$router->get('/api/notifications', 'NotificationController@index');

$router->get('/api/notifications/statistics', 'NotificationController@getYearlyStatistics');

$router->post('/api/notifications/otp', 'NotificationController@sendOtp');

$router->post('/api/notifications/verify-otp', 'NotificationController@verifyOtp');

$router->post('/api/notifications', 'NotificationController@sendBulkSms');

// Create User Notification
$router->post('/api/notifications/user','NotificationController@createNotification');

// Get All User Notifications
$router->get('/api/notifications/user','NotificationController@getUserNotifications');

$router->get('/api/notifications/stats','NotificationController@getStats');

$router->get('/api/notifications/parameters',"NotificationController@getMessagingParameters");


$router->get('/api/sms',function (){
    $calculator = new \App\Helpers\SmsLengthCalculator();
//    return $calculator->getPartCount("Lorem ipsum dolor sit amet, consectetur adipiscing elit,
    return $calculator->getPartCount("Lorem ipsum dolor sit amet, consectetur adipiscing elit,
     sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
     nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
     cupidatat non proident,
    sunt in culpa qui officia deserunt mollit anim id est laborum.");


});

