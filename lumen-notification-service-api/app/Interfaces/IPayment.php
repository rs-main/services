<?php


namespace App\Interfaces;


interface IPayment
{
    public function makePayment($options = []);

    public function verifyPayment($options = []);

    public function getRequiredFields ();
}
