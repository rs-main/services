<?php

namespace App\Interfaces;

use App\Strategy\Notification;

interface NotificationStrategy
{
    public function send(Notification $notification);

}
