<?php

namespace App\Helpers;

class Helper
{
    public static function DetermineNetWork($prefix){
        $has_zero_prefix = substr($prefix,0,1) == 0;
        $prefix_code = $has_zero_prefix ? $prefix : "0".$prefix;
        $network = false;

        switch ($prefix_code){
            case "024":
            case "054":
            case "055":
            case "059":
                return $network = "MTN";
            case "020":
            case "050":
                return $network = "Vodafone";
            case "027":
            case "057":
            case "026":
            case "056":
                return $network = "AirtelTigo";
            default:
                return $network;
        }
    }

//    public static function GenerateRandomString($length = 10) {
//        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//        $charactersLength = strlen($characters);
//        $randomString = '';
//        for ($i = 0; $i < $length; $i++) {
//            $randomString .= $characters[rand(0, $charactersLength - 1)];
//        }
//        return $randomString;
//    }

    public static function GenerateRandomString($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
