<?php

namespace App\Http\Controllers;

use App\Jobs\SendBulkEmailNotification;
use App\Jobs\SendBulkPushNotification;
use App\Jobs\SendNotification;
use App\Jobs\SendSmsNotification;
use App\Models\Campaign;
use App\Models\ClientMessagingParameter;
use App\Models\Otp;
use App\Models\SmsAccount;
use App\Models\UserNotification;
use App\Strategy\BulkEmailStrategy;
use App\Strategy\BulkPushNotificationStrategy;
use App\Strategy\BulkSmsStrategy;
use App\Strategy\EmailStrategy;
use App\Strategy\Notification;
use App\Strategy\OtpStrategy;
use App\Strategy\PushNotificationStrategy;
use App\Strategy\SmsStrategy;
use App\Traits\NotificationTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class NotificationController  extends Controller
{
    use NotificationTrait;

    public function __construct()
    {
        //
    }

    public function index(Request $request){

        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $skipped = $page == 1 ? 0 : ($page * $limit)-$limit;

        $this->validate($request,["service" => "required"]);

        $sms_account = SmsAccount::whereService(strtolower($request->service))->first();
        if (!$sms_account) {
            return response()->json(["message" => "Service account does not exist"], 200);
        }

        $campaign_builder = Campaign::where("sms_account_id", $sms_account->id);
        $campaigns = $campaign_builder->orderByDesc("created_at")->get();

        $count = $campaign_builder->count();
        $last_page = ceil($count/$limit);

//        return response()->json(
//            ["data" => $campaigns, "page" => (int)$page,
//            "total" => (int)$count, "last_page" => (int)$last_page, "limit"=> (int)$limit
//            ]);

        return DataTables::of($campaigns)->make(true);
    }

    public function getStats(Request $request){
        $service = $request->query("service");
        $campaign_counts = Campaign::whereService($service)->sum("counts");
        $sms_campaigns   = Campaign::whereService($service)->whereSms(true)->sum("counts");
        $email_campaigns   = Campaign::whereService($service)->whereEmail(true)->sum("counts");
        $push_campaigns   = Campaign::whereService($service)->wherePush(true)->sum("counts");

        return response()->json([
            "total_messages_count" => $campaign_counts,
            "sms_messages_count" => $sms_campaigns,
            "email_messages_count" => $email_campaigns,
            "push_messages_count" => $push_campaigns,
        ]);
    }

    public function getYearlyStatistics(Request $request){
        $this->validate($request, ["service" => "required"]);

        $sms_account = SmsAccount::whereService(strtolower($request->service))->first();
        if (!$sms_account) {
            return response()->json(["message" => "Service account does not exist"], 200);
        }

        $campaigns = Campaign::where("sms_account_id",$sms_account->id)
            ->select(\DB::raw("(COUNT(*)) as count"),\DB::raw("MONTHNAME(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy('month')
            ->get();

        return response()->json(["data" => $campaigns ]);
    }

    public function getSmsAccountBalance(Request $request){
        $this->validate($request,["service" => "required"]);
        $service = $request->query("service");
        return SmsAccount::whereService($service)->first();
    }

    public function notify(Request $request){

        $this->validate($request,[
            "user_phone_number" => 'required|regex:/(3)[0-9]{9}/',
//            "user_email" => "required",
            "user_id" => "required",
            "type"    => "required",
            "service" => "required",
            "mode"    => "required",
            "sender_id" => "required"
        ]);

        if ($request->mode == "email" && !in_array($request->type,["registration","otp"])){
            return response()->json(["message" => "type not valid for email notification! Choose from registration,otp"],400);
        }

        if ($request->has("mode") && !in_array($request->mode,["otp","sms","email"])){
            return response()->json(["message" => "mode  not valid! Choose from email,sms,otp"],400);
        }

        if (!$request->has("message") && $request->has("mode") && in_array($request->mode,["sms","email"])){
            return response()->json(["message" => "Message is required for this mode"],400);
        }

        $sms_account =  $this->getSmsAccount($request);

        $campaign = new Campaign();
        $campaign->name = "Default Campaign";
        $campaign->sender_id = $request->sender_id;
        $campaign->counts = 1;
        $campaign->sms = true;
        $campaign->message = $request->message;
        $campaign->sms_account_id = $sms_account->id;

        if ($campaign->save()){
            $notification = new Notification();
            $notification->setEmail($request->user_email);
            $notification->setPhoneNumber($request->user_phone_number);
            $notification->setMode($request->mode);
            $notification->setType($request->get("type"));
            $notification->setMessage($request->message);
            $notification->setSenderId($request->sender_id);
            $notification->setServiceName($request->service);
            $notification->setCampaignId($campaign->id);
            $strategy = $this->getStrategy($notification);

            if (!$strategy->canSendMessage($notification)){
                return response()->json(["message" => "Not enough credits to send message"],400);
            }

            if ($strategy->canSendMessage($notification)){
                $job = new SendSmsNotification($strategy, $notification);
                $this->dispatch($job);
            }
        }

        return json_encode(["message" => "success" ]);
    }

    public function topUpSmsCredits(Request $request){

        $this->validate($request,[
            "service" => "required",
            "amount"  => "required"
        ]);

        $account = SmsAccount::whereService(strtolower($request->service))->first();

        if ($account){
            $previous_credit_balance = $account->credits;
            $charge = env('SMS_CHARGE_PER_MESSAGE');
            $top_up_credits = ceil((int)$request->amount/$charge);
            $account->update([
                "credits" => $previous_credit_balance+$top_up_credits
            ]);
        }

        return response()->json(["data" => $account],200);
    }

    public function sendOtp(Request $request){
        $this->validate($request,[
            "user_phone_number"       => "required",
            "service"                 => "required"
        ]);
        $notification = new Notification();
        $notification->setPhoneNumber($request->user_phone_number);
        $notification->setSenderId($request->sender_id);
        $notification->setMode("sms");
        $strategy = new OtpStrategy();
        $job = new SendNotification($strategy, $notification);
        $this->dispatch($job);
        return response()->json($notification);
    }

    public function verifyOtp(Request $request){
        $this->validate($request, [
            "code" => "required",
            "phone_number" => "required"
        ]);

        $code = $request->code;
        $phone_number = $request->phone_number;

        $otp = Otp::whereCode($code)->wherePhoneNumber($phone_number)->whereExpired(false)->latest()->first();

        if ($otp){
//            $otp->update([
//                "expired" => true
//            ]);
            return response()->json(["status" => "success"],200);
        }else{
            return response()->json(["status" => "failed", "message" => "Otp is not valid or expired!"],200);
        }
    }

    public function sendBulkNotification(Request $request){
        $this->validate($request,[
            "service"       => "required",
            "sender_id"     => "required",
            "phone_numbers" => "required",
            "user_ids"      => "required",
            "type"          => "sms",
            "message"       => "required",
            "campaign_name" => "required"
        ]);

        $types = explode(",", $request->get("type"));

        foreach ($types as $type) {
            switch ($type) {
                case "sms":
                    $this->sendBulkSms($request);
                    break;

                case "email":
                    $this->sendBulkEmails($request);
                    break;

                case "push":
                    $this->sendBulkPush($request);
                    break;
            }
        }
    }

    public function sendBulkSms(Request $request){

        $phone_numbers_array = explode(",", $request->phone_numbers);
        $user_ids_array = explode(",", $request->user_ids);
        $emails_array = explode(",", $request->user_ids);

        $sms_account = SmsAccount::whereService($request->service)->first();

        if (!$sms_account) {
            return new \Exception("Sms Account does not exist!");
        }

        $campaign = new Campaign();
        $campaign->name = $request->get("campaign_name");
        $campaign->sender_id = $request->sender_id;
        $campaign->counts = count($phone_numbers_array);
        $campaign->sms = true;
        $campaign->service = $request->service;
        $campaign->sms_account_id = $sms_account->id;
        $campaign->message = $request->message;

        if ($campaign->save()) {

                $notification = new Notification();
                $notification->setEmail($request->user_email);
                $notification->setMessage($request->message);
                $notification->setSenderId($request->sender_id);
                $notification->setServiceName($request->service);
                $notification->setPhoneNumbers($phone_numbers_array);
                $notification->setCampaignId($campaign->id);

            foreach (explode(",",$request->type) as $type ){
                $notification->setMode($type);

                switch ($notification->getType()){
                    case "sms":
                        $strategy = new BulkSmsStrategy();
                        $strategy->canSendMessage($notification);
                        if (!$strategy->canSendMessage($notification)) {
                            return response()->json(["message" => "Not enough credits to send message"], 400);
                        }
                        foreach ($notification->getPhoneNumbers() as $phoneNumber) {
                            $notification->setPhoneNumber($phoneNumber);
                            $job = new SendSmsNotification($strategy, $notification);
                            $this->dispatch($job);
                        }
                        break;

                    case "email" :
                        $notification->setEmails($emails_array);
                        $strategy = new BulkEmailStrategy();
                        $job = new SendBulkEmailNotification($strategy, $notification);
                        $this->dispatch($job);
                        break;

                    case "push" :
                        $notification->setUserIds($user_ids_array);
                        $strategy = new BulkPushNotificationStrategy();
                        $job = new SendBulkPushNotification($strategy, $notification);
                        $this->dispatch($job);
                        break;

                }

            }
        }
            return response()->json(["message" => "success"]);
    }

    public function sendBulkEmails(Request $request){

    }

    public function sendBulkPush(Request $request){

    }

    public function getUserNotifications(Request $request){
        $this->validate($request,[
            "service" => "required",
        ]);

        $user_notification_builder = UserNotification::query();

        if ($request->has("user_id")){
            $user_notification_builder->whereUserId($request->query("user_id"));
        }

        $notifications = $user_notification_builder->orderByDesc("created_at")->limit(10)->get();

        return response()->json(["data" => $notifications, "message" => "success"],200);
    }

    public function createNotification(Request $request){
        $this->validate($request,[
            "service" => "required",
            "user_id" => "required",
            "source"  => "required",
            "message" => "required",
            "from"    => "required"
        ]);

       $notify = UserNotification::create($request->only("user_id","source","from","message","service"));

        return response()->json(["data" => $notify, "message" => "success" ]);
    }

    /**
     * @param Notification $notification
     * @return EmailStrategy|PushNotificationStrategy|SmsStrategy
     * @throws \Exception
     */
    public function getStrategy(Notification $notification)
    {
        switch ($notification->getMode()) {
            case 'sms':
                $strategy = new SmsStrategy();
                break;

            case 'otp':
                $strategy = new OtpStrategy();
                break;

//            case 'bulk-sms':
//                $strategy = new BulkSmsStrategy();
//                break;

            case 'email':
                $strategy = new EmailStrategy();
                break;

//            case 'bulk-email':
//                $strategy = new BulkEmailStrategy();
//                break;

            case 'push':
                $strategy = new PushNotificationStrategy();
                break;

            default:
                throw new \Exception('Strategy not found for this mode.');
        }
        return $strategy;
    }


    public function getSmsAccount(Request $request)
    {
        $sms_account = SmsAccount::whereService($request->service)->first();
        if (!$sms_account) {
            return response()->json(["message" => "Service account does not exist"], 200);
        } else {
            return $sms_account;
        }
    }

    public function getMessagingParameters(Request $request){
        return ClientMessagingParameter::with("parameter")
            ->where("client",$request->query("client"))
            ->get();
    }
}
