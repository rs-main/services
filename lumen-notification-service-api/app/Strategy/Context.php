<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Interfaces\NotificationStrategy;
use App\Strategy\Notification;

class Context
{
    /**
     * @var NotificationStrategy
     */
    private $notificationStrategy;

    /**
     * Context constructor.
     * @param NotificationStrategy $notificationStrategy
     */
    public function __construct(NotificationStrategy $notificationStrategy)
    {
        $this->notificationStrategy = $notificationStrategy;
    }

    public function send(Notification $notification)
    {
         return $this->notificationStrategy->send($notification);
    }
}
