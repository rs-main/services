<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Interfaces\NotificationStrategy;
use App\Libraries\SmsLibraries\Sms;
use App\Strategy\Notification;
use Illuminate\Support\Facades\Mail;

/**
 * Class ElectronicTaxStrategy
 * @package App\Strategy
 */
class BulkEmailStrategy implements NotificationStrategy
{

    /**
     * @param Notification $notification
     *
     */
    public function send(Notification $notification)
    {

        foreach ($notification->getEmails() as $email) {
            Mail::send($notification->getType(), [
                "content" => $notification->getMessage()
            ], function ($message) use ($notification,$email) {
                $message->to($email)
                    ->subject($notification->getSubject());
            });
        }
    }
}
