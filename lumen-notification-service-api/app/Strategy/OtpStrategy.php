<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Helpers\Helper;
use App\Helpers\SmsLengthCalculator;
use App\Interfaces\NotificationStrategy;
use App\Libraries\SmsLibraries\Sms;
use App\Models\Otp;
use App\Models\SmsAccount;
use App\Strategy\Notification;

/**
 * Class ElectronicTaxStrategy
 * @package App\Strategy
 */
class OtpStrategy implements NotificationStrategy
{
    /**
     * OtpStrategy constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param Notification $notification
     *
     */
    public function send(Notification $notification)
    {
        $phone_number = $notification->getPhoneNumber();
        $code = Helper::GenerateRandomString(6);
        $message = env("OTP_SMS_MESSAGE");
        $notification->setMessage($message.$code);
        $sms = new Sms($phone_number,$notification->getMessage());
        $sms->sendOtp();
        $this->saveOtp($notification, $code);
    }

    private function saveOtp(Notification $notification, $code){
        Otp::create([
            "phone_number" => $notification->getPhoneNumber(),
            "code"         => $code
        ]);
    }

    public function canSendMessage(Notification $notification): bool
    {
        $account = SmsAccount::whereService(strtolower($notification->getServiceName()))->first();
        $sms_calculator = new SmsLengthCalculator();
        $total_credits = $sms_calculator->getPartCount($notification->getMessage());
        if ($account && $account->credits >= $total_credits) {
            return true;
        }
        return false;
    }
}
