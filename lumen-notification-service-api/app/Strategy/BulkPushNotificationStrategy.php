<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Interfaces\NotificationStrategy;

/**
 * Class ElectronicTaxStrategy
 * @package App\Strategy
 */
class BulkPushNotificationStrategy implements NotificationStrategy
{

    /**
     * @param Notification $notification
     * @return mixed
     */
    public function send(Notification $notification)
    {

        // TODO: Implement send() method.
    }
}
