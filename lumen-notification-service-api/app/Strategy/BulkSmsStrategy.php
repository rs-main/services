<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Helpers\SmsLengthCalculator;
use App\Interfaces\NotificationStrategy;
use App\Libraries\SmsLibraries\Sms;
use App\Models\SmsAccount;

/**
 * Class ElectronicTaxStrategy
 * @package App\Strategy
 */
class BulkSmsStrategy implements NotificationStrategy
{

    /**
     * @param Notification $notification
     *
     */
    public function send(Notification $notification)
    {
        $phone_number = $notification->getPhoneNumber();
        $message      = $notification->getMessage();
        $sms          = new Sms($phone_number,$message);
        $sms->sendSms();
    }

    public function canSendMessage(Notification $notification)
    {
        $account = SmsAccount::whereService(strtolower($notification->getServiceName()))->first();
        $sms_calculator = new SmsLengthCalculator();
        $total_credits = $sms_calculator->getPartCount($notification->getMessage()) * count($notification->getPhoneNumbers());

        if ($account && $account->credits >= $total_credits) {
            return true;
        }
        return false;
    }
}
