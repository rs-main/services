<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Interfaces\NotificationStrategy;
use Illuminate\Support\Facades\Mail;

/**
 * Class EmailStrategy
 * @package App\Strategy
 */
class EmailStrategy implements NotificationStrategy
{

    /**
     * @param Notification $notification
     */
    public function send(Notification $notification)
    {
        Mail::send($notification->getType(), [
            "content" => $notification->getMessage()
        ], function ($message) use($notification) {
                $message->to($notification->getEmail())
                    ->subject($notification->getSubject());
            });
    }
}
