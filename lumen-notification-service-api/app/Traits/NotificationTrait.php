<?php

namespace App\Traits;

use App\Exceptions\NotificationException;
use App\Libraries\SmsLibraries\Sms;
use App\Models\Service;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

trait NotificationTrait
{
    private $user;
    private $type;
    private $mode;
    private $message;
    private $service;
    private $subject;

    public function buildNotification($user, $type, $service, $mode, $message = "")
    {
        $this->user = $user;
        $this->type = $type;
        $this->service = $service;
        $this->message = $message;
        $this->mode = $mode;
    }

    /**
     * @throws \Exception
     */
    public function sendNotification()
    {
        $this->getModeAndSend();
    }


    private function sendOtp()
    {
        $sms = new Sms($this->user["phone_number"],$this->message);
        $sms->sendOtp();
    }

    /**
     * @throws \Exception
     */
    private function getModeAndSend()
    {
        switch ($this->mode) {
            case "email":
                $this->sendEmail();
                break;

            case "sms":
                $this->sendSms();
                break;

            case "otp":
                $this->sendOtp();
                break;

            default:
                throw new NotificationException("Not a valid notification mode");
        }
    }

    private function getEmailFormatType()
    {
        $format = false;
        switch ($this->type) {
            case "registration":
                $format = "emails.registration";
                break;

            case "otp":
                $format = "emails.otp";
                break;
        }

        return $format;
    }

    private function getServiceName()
    {
        $name = false;
        switch ($this->service) {
            case Service::XTRACLASS_SERVICE_CODE:
                 $name = Service::XTRACLASS_SERVICE_NAME;
                 break;

            case Service::GKB_SERVICE_CODE:
                 $name = Service::GKB_SERVICE_NAME;
                 break;
        }

        return $name;
    }


    private function demo(){
//        $product = new Product;
//        $product->setName('Product Test')
//            ->setCategory('electronics')
//            ->setPrice(100);
//
//        switch ($product->getCategory()) {
//            case 'electronics':
//                $strategy = new ElectronicTaxStrategy;
//                break;
//            case 'food':
//                $strategy = new FoodTaxStrategy;
//                break;
//            case 'books':
//                $strategy = new TaxFreeStrategy;
//                break;
//            default:
//                throw new \Exception('Strategy not found for this category.');
//        }

//        $context = new Context($strategy);
//        $context->calculateProduct($product);
    }
}
