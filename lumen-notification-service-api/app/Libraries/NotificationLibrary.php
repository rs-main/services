<?php

namespace App\Libraries;

use App\Interfaces\INotification;
use App\Traits\NotificationTrait;

class NotificationLibrary
{
    use NotificationTrait;

    public function __construct($user, $type, $service,$mode, $message)
    {
        $this->buildNotification($user, $type, $service,$mode,$message);
    }


    public function sendNotification()
    {
         $this->sendNotification();
    }
}
