<?php

namespace App\Libraries\SmsLibraries\Hubtel;
use App\Interfaces\Sms\ISms;
use Http\Client\Exception;

class HubtelSms implements ISms
{
    private $messagingApi;
    private $text;

    public function __construct()
    {
        $auth = new BasicAuth(env("HUBTEL_SMS_CLIENT_ID"), env("HUBTEL_SMS_CLIENT_SECRET"));
        $apiHost = new ApiHost($auth);
        $this->messagingApi = new MessagingApi($apiHost);
    }

    /**
     * @param $text
     * @return mixed
     */
    public function buildMessage($text)
    {
        $this->text = $text;
    }

    /**
     * @param $to
     * @return mixed
     */
    public function sendSms($to)
    {
        try {
            $messageResponse = $this->messagingApi
                ->sendQuickMessage(env('SMS_FROM_NUMBER'), $to, $this->text);
            if ($messageResponse instanceof MessageResponse) {
                echo $messageResponse->getStatus();
            } elseif ($messageResponse instanceof \HttpResponse) {
                echo "\nServer Response Status : " . $messageResponse->getStatus();
            }
        } catch (\Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }

    public function sendOtp($to){
        $this->sendSms($to);
    }
}
