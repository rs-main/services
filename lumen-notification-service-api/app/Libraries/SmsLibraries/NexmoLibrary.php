<?php

namespace App\Libraries\SmsLibraries;

use App\Interfaces\Sms\ISms;
use Vonage\Client;
use Vonage\SMS\Message\SMS as VonageSms;

class NexmoLibrary implements ISms
{
    private $nexmo;
    private $text;

    public function __construct()
    {
        $this->nexmo  = new Client(new Client\Credentials\Basic(env("NEXMO_KEY"), env("NEXMO_SECRET")));
    }

    public function buildMessage($text)
    {
        $this->text = $text;
    }


    public function sendSms($to){
        $text = new VonageSms($to,  env("SMS_FROM_NUMBER"), $this->text);
        $text->setClientRef('test-message');
        return $this->nexmo->sms()->send($text);
    }

}
