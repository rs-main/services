<?php

namespace App\Libraries\SmsLibraries;


use App\Libraries\SmsLibraries\Hubtel\HubtelSms;

class Sms
{
    private $nexmo;
    private $hubtel;
    private $coreSms;
    private $to;

    public function __construct($to,$text)
    {
        $this->text = $text;
        $this->to = $to;

        // Nexmo Implementation
//        $this->nexmo = new NexmoLibrary();
        $this->hubtel = new HubtelSms();
        $this->hubtel->buildMessage($text);

        // Core Sms
//        $this->coreSms = new CoreSmsLibrary();
//        $this->coreSms->buildMessage($text);
    }

    public function sendSms()
    {
        return $this->hubtel->sendSms($this->to);
    }

    public function sendOtp(){
        $this->hubtel->sendSms($this->to);
    }

}
