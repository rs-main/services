<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsAccount extends Model
{
    use HasFactory;

    protected $table = "sms_accounts";

    protected $guarded = [];
}
