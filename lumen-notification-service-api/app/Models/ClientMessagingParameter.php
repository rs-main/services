<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientMessagingParameter extends Model
{
    use HasFactory;

    protected $table = "client_messaging_parameters";

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i'
    ];

    public function parameter(){
        return $this->belongsTo(MessagingParameter::class, "parameter_id");
    }

}
