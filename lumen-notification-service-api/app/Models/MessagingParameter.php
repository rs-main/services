<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessagingParameter extends Model
{
    use HasFactory;

    protected $table = "messaging_parameters";

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i'
    ];
}
