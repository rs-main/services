<?php

namespace App\Jobs;

use App\Interfaces\NotificationStrategy;
use App\Models\Campaign;
use App\Models\NotificationModel;
use App\Strategy\Notification;
use App\Strategy\Context;
use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBulkNotification extends Job implements ShouldQueue
{
    use NotificationTrait;

    /**
     * @var NotificationStrategy $strategy
     */
    private $strategy;

    /**
     * @var Notification $notification
     */
    private $notification;

    public function __construct($strategy, $notification)
    {
        $this->strategy     = $strategy;
        $this->notification = $notification;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $context = new Context($this->strategy);
        $response = $context->send($this->notification);

        $notificationModel = new NotificationModel();
        $notificationModel->message = $this->notification->getMessage();
        $notificationModel->receiver = $this->getReceiver($this->notification);
        $notificationModel->status = null;
        $notificationModel->sms = $this->notification->getMode() == "sms";
        $notificationModel->email = $this->notification->getMode() == "email";
        $notificationModel->push = $this->notification->getMode() == "push";
        $notificationModel->gateway_response = ($response);
        $notificationModel->save();

    }

    /**
     * @param $mode
     * @param Notification $notification
     * @return mixed
     * @throws \Exception
     */
    private function getReceiver(Notification $notification){
        switch ($notification->getMode()){
            case "sms":
                $receiver = $notification->getPhoneNumber();
                break;
            case "email":
                $receiver = $notification->getEmail();
                break;

            default:
                throw new \Exception('mode not valid!');
        }

        return $receiver;
    }
}
