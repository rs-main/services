<?php

namespace App\Jobs;

use App\Helpers\SmsLengthCalculator;
use App\Interfaces\NotificationStrategy;
use App\Models\Campaign;
use App\Models\NotificationModel;
use App\Models\SmsAccount;
use App\Strategy\Notification;
use App\Strategy\Context;
use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotification extends Job implements ShouldQueue
{
    use NotificationTrait;

    /**
     * @var NotificationStrategy $strategy
     */
    private $strategy;

    private $length_calculator;

    /**
     * @var Notification $notification
     */
    private $notification;

    public function __construct($strategy, $notification)
    {
        $this->strategy     = $strategy;
        $this->notification = $notification;
        $this->length_calculator = new SmsLengthCalculator();

    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $context = new Context($this->strategy);
        $response = $context->send($this->notification);
        $this->deductCreditsFromAccount($this->notification);

        $notificationModel = new NotificationModel();
        $notificationModel->campaign_id  = $this->notification->getCampaignId();
        $notificationModel->message = $this->notification->getMessage();
        $notificationModel->sender_id = $this->notification->getSenderId();
        $notificationModel->receiver = $this->getReceiver($this->notification);
        $notificationModel->status = null;
        $notificationModel->sms = $this->notification->getMode() == "sms";
        $notificationModel->email = $this->notification->getMode() == "email";
        $notificationModel->push = $this->notification->getMode() == "push";
        $notificationModel->gateway_response = ($response);
        $notificationModel->sms_part_count = $this->length_calculator->getPartCount($this->notification->getMessage());
        $notificationModel->credit = $this->length_calculator->getPartCount($this->notification->getMessage());
        $notificationModel->charge = $this->length_calculator->getPartCount($this->notification->getMessage()) * env("SMS_CHARGE_PER_MESSAGE");
        $notificationModel->save();
    }

    /**
     * @param $mode
     * @param Notification $notification
     * @return mixed
     * @throws \Exception
     */
    private function getReceiver(Notification $notification){
        switch ($notification->getMode()){
            case "sms":
                $receiver = $notification->getPhoneNumber();
                break;
            case "email":
                $receiver = $notification->getEmail();
                break;

            default:
                throw new \Exception('mode not valid!');
        }

        return $receiver;
    }

    private function deductCreditsFromAccount(Notification $notification){
    $account = SmsAccount::whereService(strtolower($notification->getServiceName()))->first();
    $total_credits = $this->length_calculator->getPartCount($notification->getMessage());
    $old_credits = $account->credits;
    $account->update([
        "credits" => $old_credits-$total_credits
    ]);

}

}
