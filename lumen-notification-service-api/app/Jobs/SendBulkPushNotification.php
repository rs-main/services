<?php

namespace App\Jobs;

use App\Interfaces\NotificationStrategy;
use App\Models\NotificationModel;
use App\Strategy\Notification;
use App\Strategy\Context;
use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBulkPushNotification extends Job implements ShouldQueue
{
    use NotificationTrait;

    /**
     * @var NotificationStrategy $strategy
     */
    private $strategy;

    /**
     * @var Notification $notification
     */
    private $notification;

    public function __construct($strategy, $notification)
    {
        $this->strategy     = $strategy;
        $this->notification = $notification;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $context = new Context($this->strategy);
        $context->send($this->notification);
    }

}
