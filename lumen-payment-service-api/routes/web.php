<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Http;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Payments
$router->get('/api/payments/transactions', 'TransactionsController@index');

$router->post('/api/payments/transactions', ['as' => 'transactions', 'uses' => 'TransactionsController@store']);
$router->post('/api/payments/transactions/otp', ['as' => 'otp', 'uses' => 'TransactionsController@submitOtp']);
$router->get('/api/payments/verify-payment/{reference}', ['as' => 'verify', 'uses' => 'TransactionsController@verifyPayment']);
$router->get("/api/payments/paystack-webhook",['as' => 'webhook','uses' => 'TransactionsController@payStackWebhook']);

// Wallets
$router->post("/api/payments/fund-wallet", 'TransactionsController@fundWallet');
$router->get("/api/wallets/{user_id}", 'WalletsController@transactions');
$router->get("/api/wallets/balance/{user_id}", 'WalletsController@show');
$router->get("/api/wallets/transactions/{user_id}", 'WalletsController@transactions');
