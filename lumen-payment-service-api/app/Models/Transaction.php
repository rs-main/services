<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table = "transactions";
    const TRANSACTION_STATUS = "status";
    const SUCCESS = "success";
    const TRANSACTION_UNPAID = "unpaid";
    const TRANSACTION_OTP = "otp";
    const TRANSACTION_OFFLINE = "offline";
    const TRANSACTION_MAIN = "main";

    protected $guarded = [];

    protected $casts = [
        "provider_response" => "array",
        'created_at' => 'datetime:Y-m-d H:i'
    ];
}
