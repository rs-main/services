<?php

namespace App\PaymentLibraries\Flutterwave\PaymentOptions;

use App\Interfaces\IMobileMoneyPayment;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use Webpatser\Uuid\Uuid;

class FlutterWaveMobileMoney implements IMobileMoneyPayment
{

    use ConsumeExternalService,ApiResponse;

    public $baseUri;

    public $secret;

    private $url;

    private $verify_url;

    public function __construct()
    {
        $this->baseUri    = env('FLUTTERWAVE_SERVICE_BASE_URL');
        $this->secret     = env('FLUTTERWAVE_SERVICE_SECRET');
        $this->url        = '/v3/charges?type=mobile_money_ghana';
        $this->verify_url = '/v3/transactions/';
    }

    public function makePayment($options = [])
    {
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';
        return $this->performRequest('POST', $this->url, $options, $headers);
    }

    public function verifyPayment($options = [])
    {
        $verify_url = $this->verify_url.$options["tx_ref"]."/verify";
        $headers['Content-Type'] = 'application/json';
        return $this->performRequest('GET', $verify_url, $options, $headers);
    }

    public function getRequiredFields(): array
    {
        return [
            'phone_number' => 'required',
            'amount'       => 'required',
            'currency'     => 'required',
            'network'      => 'required',
            'email'        => 'required',
            'fullname'     => 'required',
            'user_id'      => 'required',

        ];
    }

    /**
     * @param array $options
     * @param $request
     * @return array
     * @throws \Exception
     */
    public function getOptions(array $options, $request): array
    {
        $options["tx_ref"] = (string)Uuid::generate();
        foreach ($this->getRequiredFields() as $key=> $option){
                $options[$key] = $request->$key;
        }
        return $options;
    }

    public function submitOtp($options = [])
    {
        // TODO: Implement submitOtp() method.
    }
}
