<?php


namespace App\PaymentLibraries;


use App\PaymentLibraries\Flutterwave\PaymentOptions\FlutterWaveMobileMoney;
use App\PaymentLibraries\PayStack\PayStackLibrary;

;

class MobileMoneyPayment
{
    private $flutterwave;
    private $payStack;

    public function __construct(FlutterWaveMobileMoney $flutterWaveMobileMoney, PayStackLibrary $payStack)
    {
        $this->flutterwave = $flutterWaveMobileMoney;
        $this->payStack = $payStack;
    }

    public function initiatePayment($options){
        return $this->payStack->makePayment($options);
//        return $this->flutterwave->makePayment($options);
    }

    public function verifyPayment($options){
        return $this->payStack->verifyPayment($options);
//        return $this->flutterwave->verifyPayment($options);
    }

    public function getPaymentRequestOptions(): array
    {
        return $this->payStack->getRequiredFields();
//        return $this->flutterwave->getRequiredFields();
    }

    public static function requiredFields(): array
    {
        return [
            'amount'       => 'required',
            'email'        => 'required',
            'currency'     => 'required',
            'phone_number' => 'required',
            'network'      => 'required',
            'fullname'     => 'required',
            'user_id'      => 'required',
//            'reference'    => 'required',
        ];
    }

    public function getPaymentOptions($options, $request): array
    {
        return $this->payStack->getOptions($options, $request);
//        return $this->flutterwave->getRequiredFields();
    }

    public function getPaymentResponse($response)
    {
        return $this->payStack->processResponse($response);
    }

    public function submitOtp($options){
        return $this->payStack->submitOtp($options);
    }

    public function getOtpResponse($response){
        return $this->payStack->processOtpSubmittedResponse($response);
    }

    public function getVerifyResponse($response){
        return $this->payStack->processVerifyResponse($response);
    }
}
