<?php

namespace App\PaymentLibraries\PayStack;

use App\Interfaces\IMobileMoneyPayment;
use App\Models\Transaction;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use Webpatser\Uuid\Uuid;

class PayStackLibrary implements IMobileMoneyPayment
{
    use ConsumeExternalService, ApiResponse;

    public const PAY_STACK_SERVICE_PROVIDER = "paystack";
    public const MODE_OF_PAYMENT = "mobile_money";

    public $baseUri;
    public $secret;
    private $url;
    private $verify_url;
    private $submit_otp_url;

    public function __construct()
    {
        $this->baseUri = env('PAYSTACK_SERVICE_BASE_URL');
        $this->secret = env('PAYSTACK_SERVICE_SECRET');
        $this->url = '/charge';
        $this->verify_url = '/transaction/verify/';
        $this->submit_otp_url = '/charge/submit_otp/';
    }

    public function makePayment($options = [])
    {
        $headers['Content-Type'] = "application/json";
        return $this->performRequest($this->baseUri, $this->secret, 'POST', $this->url, $options, $headers);
    }

    public function verifyPayment($options = [])
    {
        $verify_url = $this->verify_url . $options["reference"] ;
        $headers['Content-Type'] = 'application/json';
        return $this->performRequest($this->baseUri,$this->secret,"GET", $verify_url,[], $headers);
    }

    public function getRequiredFields($fields = []): array
    {
        $fields = [
            "amount",
//           "amount_expected",
            "email",
            "currency",
            "phone_number",
            "network",
            "fullname",
            "service",
            "description"
        ];

        $requiredFields = [];
        foreach ($fields as $field) {
            $requiredFields[$field] = "required";
        }

        return $requiredFields;
    }

    /**
     * @param array $options
     * @param $request
     * @return array
     */
    public function getOptions(array $options, $request): array
    {
        $options["reference"] = (string)Uuid::generate();
        $options["service_provider"] = self::PAY_STACK_SERVICE_PROVIDER;

        foreach ($this->getRequiredFields() as $key => $option) {
            if ($key == "phone_number") {
                $options["mobile_money"]["phone"] = $request->phone_number;
            } elseif ($key == "network") {
                $options["mobile_money"]["provider"] = $request->network;
            }else if ($key == "amount"){
                $options["amount"] = (int)$request->amount *100;
            } else {
                $options[$key] = $request->$key;
            }
        }
        unset($options["fullname"]);
        unset($options["user_id"]);
        unset($options["phone_number"]);
//        unset($options["network"]);

        return $options;
    }

    public function processResponse($response = [])
    {
//        return $response;

        if ($response["status"] == false) {
            $state = "failed";
            $display_text = $response["message"];
            $transaction_reference = "";
            return compact("state", "display_text", "transaction_reference");
        }

        $transaction_reference = $response["data"]["reference"];
        if ($this->payOffline($response["data"]["status"])) {
            return $this->offlineChargeResponse($response["data"]);
        }

        if ($this->sendOtp($response["data"]["status"])) {
            return $this->otpChargeResponse($response["data"]);
        }

        $user_id = 1;
        $mode_of_payment = self::MODE_OF_PAYMENT;
        $service_provider = self::PAY_STACK_SERVICE_PROVIDER;
        $amount_paid = 0;
        $amount_expected = $response["data"]["amount"];
        $status = Transaction::TRANSACTION_UNPAID;
        $provider_response = ($response["data"]);
        $fees = $response["data"]["fees"];
        $user_object = ($response["data"]["customer"]);
        $provider_status = ($response["data"]["gateway_response"]);
        $payment_method = self::MODE_OF_PAYMENT;
        $service = "XTRACLASS SERVICE";
        $state = Transaction::TRANSACTION_MAIN;

        return compact(
            'user_id', 'mode_of_payment', 'service_provider', 'amount_paid',
            'transaction_reference', 'status', 'provider_response', 'payment_method', 'state',
            'amount_expected', 'fees', 'user_object', 'provider_status', 'provider_response', 'service');
    }

    /**
     * @param array $response
     * @return array
     */
    public function processOtpSubmittedResponse($response = []): array
    {
            $state = $response["status"] == true ? "success" : "failed";
            $display_text = $response["message"];
            $transaction_reference = $response["data"]["reference"];
            return compact("state", "display_text", "transaction_reference");
    }

    public function processVerifyResponse($response){
//        if (isset($response["data"]["status"])) {
            $state = $response["data"]["status"];
            $display_text = $response["data"]["gateway_response"];
            $transaction_reference = $response["data"]["reference"];
            return compact("state", "display_text", "transaction_reference");
//        }
    }


    public function submitOtp($options = [])
    {
        $headers['Content-Type'] = "application/json";
        return $this->performRequest($this->baseUri, $this->secret, 'POST', $this->submit_otp_url, $options, $headers);
    }

    /**
     * @param $data
     * @return array
     */
    public function offlineChargeResponse($data): array
    {
        $status = $data["status"];
        $display_text = $data["display_text"];
        $state = "offline";
        $transaction_reference = $data["reference"];
        return compact("transaction_reference", "status", "display_text", "state");
    }

    /**
     * @param $data
     * @return array
     */
    public function otpChargeResponse($data): array
    {
        $status = $data["status"];
        $display_text = $data["display_text"];
        $state = Transaction::TRANSACTION_OTP;
        $transaction_reference = $data["reference"];
        return compact("transaction_reference", "status", "display_text", "state");
    }

    /**
     * @param $status
     * @return bool
     */
    private function payOffline($status): bool
    {
        return $status == "pay_offline";
    }

    /**
     * @param $status
     * @return bool
     */
    private function sendOtp($status): bool
    {
        return $status == "send_otp";
    }

}
