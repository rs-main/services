<?php

namespace App\PaymentLibraries\PayStack\Models;

use App\PaymentLibraries\PayStack\Models\Models\MobileMoney;

class RequestModel
{
    private $amount;
    private $email;
    private $currency;
    private $reference;
    /**
     * @var MobileMoney
     */
    private $mobileMoney;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getMobileMoney()
    {
        return $this->mobileMoney;
    }

    /**
     * @param mixed $mobileMoney
     */
    public function setMobileMoney($mobileMoney): void
    {
        $this->mobileMoney = $mobileMoney;
    }


}
