<?php


namespace App\Interfaces;


interface IMobileMoneyPayment
{
    public function makePayment($options = []);

    public function verifyPayment($options = []);

    public function submitOtp($options = []);

    public function getRequiredFields ();

}
