<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class SubscribeToPaymentChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe-payment';
    /*
This is what will become the command we are going to use in terminal to subscribe our laravel service to nodejs
*/

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to payment channel';
    /*description of the command ,this show in the laravel artisan help
    */
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Redis::subscribe(['services-channel'], function ($message) {
            $messageArray = json_decode($message, true);
            echo $message;
        });
    }
}
