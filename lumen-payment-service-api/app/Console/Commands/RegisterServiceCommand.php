<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use Illuminate\Console\Command;


class RegisterServiceCommand extends Command
{

    protected $signature = 'register:service';

    protected $description = 'Register Service';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->registerService();
    }

    private function registerService(){

        $port = "8001";
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' =>  "payment-service",
            "appName" =>  "payment-service",
            "ip"      =>  $this->getIp(),
            'port' => ["8001", true],
            'homePageUrl'    => "http://localhost:"."$port",
            'statusPageUrl'  => "http://localhost:"."$port/info",
            'healthCheckUrl' => "http://localhost:"."$port/health"
        ]);

        $serviceClient = new EurekaClient($config);
        $serviceClient->register();
    }

    public function getIp()
    {
        return env("APP_ENV")== "local" ? "127.0.0.1" : \request()->ip();
    }
}
