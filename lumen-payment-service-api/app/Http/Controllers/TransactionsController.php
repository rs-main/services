<?php

namespace App\Http\Controllers;

use App\Jobs\TopUpSmsCreditsJob;
use App\Models\Transaction;
use App\Events\PaymentInitiatedEvent;

use App\Models\Wallet;
use App\Models\WalletTransaction;
use App\PaymentLibraries\MobileMoneyPayment;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class TransactionsController extends Controller
{
    use ApiResponse;

    private $payment;
    public function __construct(MobileMoneyPayment $payment)
    {
        $this->payment = $payment;
    }

    public function index(Request $request){

        $this->validate($request, [
            "service" => "required"
        ]);

        $limit = $request->has("limit") ? $request->query("limit") : 10;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $skipped = $page == 1 ? 0 : ($page * $limit)-$limit;

        $status = $request->query("status");
        $payment_for = $request->query("payment_for");
        $service     = $request->query("service");

        $transactions_builder = Transaction::whereService(strtolower($service));

        if ($request->has("payment_for")){
            $transactions_builder->where("payment_for",$payment_for);
        }

        if ($request->has("service_provider")){
            $transactions_builder->where("service_provider",$request->query("service_provider"));
        }

        if ($request->has("mode_of_payment")){
            $transactions_builder->where("mode_of_payment", $request->query("mode_of_payment"));
        }

        $transactions_builder->where("status","paid");

        $count = $transactions_builder->count();

        $transactions = $transactions_builder->skip($skipped)->limit($limit)->get();

        $last_page = ceil($count/$limit);

        return response()->json([
            "draw" => 0,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $transactions,
            "page" => $page,
            "limit" => $limit,
            "last_page" => $last_page
        ],200);

    }

    public function store(Request $request)
    {
        $this->validate($request,$this->payment->getPaymentRequestOptions());

//        \DB::beginTransaction();
        try {
            $options = [];
            $options = ($this->payment->getPaymentOptions($this->payment->getPaymentRequestOptions(), $request));
            $response = $this->payment->getPaymentResponse($this->payment->initiatePayment($options));
            $data = $this->startTransaction($response, $request, $options);

            return $this->successResponse(
                [
                    "data" =>
                        [
                            "state" => $data["state"],
                            "display_text" => $data["display_text"],
                            "transaction_reference" => $data["transaction_reference"],
                            "message" => "Charge initiated"
                        ]
                ]
            );
        }catch (\Exception $exception){
//            \DB::rollback();
            return response()->json(['error' => $exception->getMessage()], 500);
//            return response()->json(['error' => "payment process failed"], 500);
        }
    }

    public function verifyPayment($reference){

        try {
            $options["reference"] = $reference;
            $data = $this->payment->getVerifyResponse($this->payment->verifyPayment($options));

            if ($this->paymentVerified($data)) {
                $transaction = Transaction::whereTransactionReference($reference)->first();

                if ($transaction && $transaction->status != "paid") {
                    $transaction->update([
                        "amount_paid" => $transaction->amount_expected,
                        "status" => "paid"
                    ]);

                    if ($transaction->payment_for === "sms"){
                        $job = new TopUpSmsCreditsJob($transaction->service,$transaction->amount_expected);
                        $this->dispatch($job);
                    }

                    $w_transaction = WalletTransaction::whereTransactionReference($reference)->first();

                    if ($w_transaction) {
                        $w_transaction->update([
                            "status" => "paid"
                        ]);

                        $this->update($w_transaction->amount, $transaction->user_id);
                    }
                }
            }

            return $this->successResponse(
                [
                    "data" =>
                        [
                            "state" => $data["state"],
                            "display_text" => $data["display_text"],
                            "transaction_reference" => $data["transaction_reference"],
                            "message" => $data["display_text"]
                        ]
                ]
            );
        }catch (\Exception $exception){
            return response()->json(["data" => null, "message" => $exception->getMessage()]);
        }
    }

    public function submitOtp(Request $request){
        $this->validate($request, [
            "transaction_reference" => "required",
            "otp"       => "required"
        ]);

        $options["reference"] = $request->transaction_reference;
        $options["otp"] = $request->otp;

        $data = $this->payment->getOtpResponse( $this->payment->submitOtp($options));

        return $this->successResponse(
            [
                "data" =>
                    [
                        "state" => $data["state"],
                        "display_text" => $data["display_text"],
                        "transaction_reference" => $data["transaction_reference"],
                        "message" => "Otp verification"
                    ]
            ]
        );

    }

    public function payStackWebhook(Request $request){
        Transaction::create([
            "service" => "XtraClass",
            "user_id" => "user_id",
            "provider_response" => json_encode($request->all()),
            "service_provider" => "pay-stack",
        ]);
    }

    public function fundWallet(Request $request)
    {
        $this->validate($request,$this->payment->getPaymentRequestOptions());

        $options = ($this->payment->getPaymentOptions($this->payment->getPaymentRequestOptions(), $request));
        $response = $this->payment->getPaymentResponse( $this->payment->initiatePayment($options));
        $res = ($this->startTransaction($response,$request,$options));

        try {

            \DB::transaction(function () use ($request, $res) {
                $wallet = Wallet::firstOrCreate(
                    ["user_id" => $request->user_id]
                );

                WalletTransaction::create([
                    "wallet_id" => $wallet->id,
                    "amount" => $request->amount,
                    "type" => "fund",
                    "transaction_reference" => $res["transaction_reference"],
                    "status" => "unpaid"
                ]);
            });

            return $this->successResponse(
                [
                    "data" =>
                        [
                            "state" => $res["state"],
                            "display_text" => $res["display_text"],
                            "transaction_reference" => $res["transaction_reference"],
                            "message" => "Charge initiated"
                        ]
                ]
            );

        }catch (\Exception $exception){
            \DB::rollBack();
        }

    }

    /**
     * @param array $data
     * @return bool
     */
    public function paymentVerified(array $data): bool
    {
        return $data["state"] == "success" && $data["display_text"] == "Approved";
    }

    private function requestIsSuccessful($response): bool
    {
        return $response["provider_response"][Transaction::TRANSACTION_STATUS] === Transaction::SUCCESS;
    }


    /**
     * @param array $data
     * @param Request $request
     */
    private function insertTransaction(array $data): void
    {
        $user_object = json_encode(
            ["user_id" => $data["user_id"], "fullname" => $data["fullname"],
             "email" => $data["email"], "phone_number" => $data["phone_number"]]
        );

        $mode_of_payment = isset($data["mobile_money"]) ? $data["mobile_money"]["provider"] : "";

        Transaction::create([
            "service" => $data["service"],
            "service_code" => $data["service"],
            "user_id" => $data["user_id"],
            "user_object" => $user_object,
            "service_provider" => $data["service_provider"],
            "amount_paid" => 0,
            "amount_expected" => isset($data["amount"]) && $data["amount"] != 0 ? $data["amount"]/100 : 0,
            "fees" => 0,
            "description" => $data["description"],
            "transaction_reference" => $data["transaction_reference"],
            "mode_of_payment" => $mode_of_payment,
            "payment_method" => $mode_of_payment,
            "provider_status" => json_encode($data["status"]),
            "status" => "unpaid",
            "provider_response" => json_encode($data["display_text"]),
            "payment_for"       => isset($data["payment_for"]) ? $data["payment_for"] : null
        ]);
    }


    private function startTransaction($response,$request,$options){
        $data = false;

        switch($response["state"]){
            case Transaction::TRANSACTION_OFFLINE:
                $data = $response;
//                     \event(new PaymentInitiatedEvent(array_merge($response,$options)));
                $this->insertTransaction(array_merge($request->toArray(),$response,$options));
                break;

            case Transaction::TRANSACTION_OTP:
                $data = $response;
                break;
            case Transaction::TRANSACTION_MAIN:
                $data = $response;
//                    \event(new PaymentInitiatedEvent(array_merge($response,$options)));
                $this->insertTransaction($data,$request);
                break;
        }

        return $data;
    }


    private function update($amount, $user_id){

         $wallet = Wallet::whereUserId($user_id)->first();

         if (!$wallet){
             Wallet::create([
                 "user_id" => $user_id,
                 "previous_balance" => 0,
                 "balance"  => $amount
             ]);
         }

        if ($wallet){
            $old_balance = $wallet->balance;
            $wallet->update([
                "previous_balance" => $old_balance,
                "balance" => $old_balance+$amount,
            ]);

            return $this->successResponse(["data" => $wallet],200);
        }

        return $this->successResponse(["data"=> null, "message" => "Wallet update failed!"]);
    }

}
