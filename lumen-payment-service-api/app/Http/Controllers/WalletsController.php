<?php

namespace App\Http\Controllers;


use App\Models\Wallet;
use App\Models\WalletTransaction;
use App\Services\PaymentService;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class WalletsController extends Controller
{
    use ApiResponse;

    private $payment;
    private $payment_service;
    public function __construct()
    {
//        $this->payment_service = new PaymentService();
    }

    public function index(Request $request){

        $transactions = Wallet::paginate();
        return $this->successResponse(["data" => $transactions],200);
    }

    public function transactions($user_id){
        $transactions = [];
        if (Wallet::whereUserId($user_id)->first()){
            $transactions = Wallet::with("transactions")->whereUserId($user_id)->get();
        }
        return $this->successResponse(["data" => $transactions],200);
    }

    public function show($user_id){
        $wallet = Wallet::whereUserId($user_id)->first();
        return $this->successResponse(["data" => $wallet],200);
    }

    public function fundWallet(Request $request){
         $options = ($request->all());
        $headers = [];
        $res = $this->payment_service->makePayment($options,$headers);
        $response=  $res["response"];
        $wallet = Wallet::firstOrCreate(
            ["user_id" => $request->user_id]
        );

        WalletTransaction::create([
            "wallet_id" => $wallet->id,
            "amount"    => $request->amount,
            "type"      => "fund",
            "transaction_reference" => $response["data"]["transaction_reference"],
            "status"                => "unpaid"
        ]);

//        sleep(10);
//
//        $this->approveFundPayment($response["data"]["transaction_reference"]);

    }

    public function approveFundPayment($reference){
        $res = $this->payment_service->verifyPayment($reference);
        $response=  $res["response"];
        if ($response["state"] == "success" && $response["message"] == "Approved"){
            $transaction = WalletTransaction::whereTransactionReference($reference)->update([
                "status"  => "paid"
            ]);
            $this->update($transaction->amount, $transaction->user_id);
        }
    }

    private function update($amount, $user_id){

        $wallet = Wallet::whereUserId($user_id)->first();
        if ($wallet){
            $old_balance = $wallet->balance;
            $wallet->update([
                "previous_balance" => $old_balance,
                "balance" => $old_balance+$amount,
            ]);

            return $this->successResponse(["data" => $wallet],200);
        }

        return $this->successResponse(["data"=> null, "message" => "Wallet update failed!"]);
    }

}
