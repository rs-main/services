<?php

namespace App\Jobs;

use App\Services\NotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TopUpSmsCreditsJob implements ShouldQueue
{
    use  InteractsWithQueue, Queueable, SerializesModels;


    private $service;
    private $amount;
    private $notification_service;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($service,$amount)
    {
        $this->service = $service;
        $this->amount  = $amount;
        $this->notification_service = new NotificationService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $headers = [];
        $fields  = ["service" => $this->service,"amount" => $this->amount];
        $this->notification_service->topUp($fields,$headers);
    }
}
