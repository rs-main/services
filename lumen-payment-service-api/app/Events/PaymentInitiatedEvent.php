<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Symfony\Contracts\EventDispatcher\Event;

class PaymentInitiatedEvent extends Event implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;


    public $options = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ["payments-channel"];

    }

    public function broadcastWith()
    {
        return $this->options;
    }

    public function broadcastAs()
    {
        return 'payment-initiated';
    }
}
