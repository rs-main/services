<?php

namespace App\Listeners;

use App\Events\PaymentInitiatedEvent;
use App\Jobs\UploadJob;
use App\Models\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class PaymentsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PaymentInitiatedEvent $event
     * @return void
     */
    public function handle(PaymentInitiatedEvent $event)
    {
        Artisan::call("redis:publish-payment",[$event]);
//        $this->saveTransaction($event);
    }

    /**
     * @param PaymentInitiatedEvent $event
     */
    public function saveTransaction(PaymentInitiatedEvent $event): void
    {
        $data = ($event);
        DB::beginTransaction();
             $this->insertTransaction($data);
        DB::commit();
        Redis::publish('payment', $data);
    }


    public function insertTransaction(PaymentInitiatedEvent  $event): void
    {
//        $data  = json_decode($event->payload);
//        $decoded = json_decode($event->options);


        Transaction::create([
            "service" => $event->options["service"],
            "service_code" => $event->options["service"],
            "user_id" => $event->options["user_id"],
            "user_object" => ($event->options["user_object"]),
            "service_provider" => $event->options["service_provider"],
            "amount_paid" => $event->options["amount"],
            "amount_expected" => $event->options["amount"],
            "fees" => $event->options["fees"],
            "description" => $event->options["description"],
            "transaction_reference" => $event->options["transaction_reference"],
            "mode_of_payment" => $event->options["mode_of_payment"],
            "payment_method" => $event->options["payment_method"],
            "provider_status" => $event->options["provider_status"],
            "status" => "unpaid",
            "provider_response" => ($event->options["provider_response"])
        ]);

        Throw new \Exception(($event));
    }

}
