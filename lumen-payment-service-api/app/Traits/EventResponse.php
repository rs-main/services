<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait EventResponse
{
    /**
     * Success Response
     * @param $data
     * @param int $code
     *
     */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return response($data, $code)->header('Content-Type', 'application/json');
    }

    public function publishResponse($id, $type, $trace_id, $user_id, $owner_id, $source_url, $resource_id){

        $metadata["traceId"]   = $trace_id;
        $metadata["userId"]    = $user_id;
        $data["ownerId"]       = $owner_id;
        $data["sourceUrl"]     = $source_url;
        $data["resourceId"]    = $resource_id;

        return json_encode(["id" => $id, "type" => $type, $metadata, $data]);
    }

//    public function errorResponse($message, $code)
//    {
//        return response()->json(['error' => $message, 'code' => $code], $code);
//    }
//
//    public function errorMessage($message, $code)
//    {
//        return response($message, $code)->header('Content-Type', 'application/json');
//    }
}
