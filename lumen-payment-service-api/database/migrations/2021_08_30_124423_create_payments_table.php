<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->text("service");
            $table->text("service_code")->nullable();
            $table->string("user_id");
            $table->json("user_object")->nullable();
            $table->string("service_provider");
            $table->decimal("amount_paid")->default(0);
            $table->decimal("amount_expected")->default(0);
            $table->decimal("fees")->default(0);
            $table->string('description')->nullable();
            $table->string('mode_of_payment')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->string("payment_method")->nullable();
            $table->string("payment_method_code")->nullable();
            $table->json("provider_status")->nullable();
            $table->json("provider_response")->nullable();
            $table->string("status")->nullable();
            $table->string("status_code")->nullable();
            $table->string("currency")->default("GHS")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
