package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.entity.QueryTerm;
import com.realstudios.services.uploadservice.data.entity.SearchQuery;

import java.util.List;
import java.util.Map;

public interface QueryService {


    public Map<String, Object> getSearchQueries();

    public Map<String, Object> getMonthlyQueriesStat();

    public void insert(QueryTerm query, List<File> fileList);
}
