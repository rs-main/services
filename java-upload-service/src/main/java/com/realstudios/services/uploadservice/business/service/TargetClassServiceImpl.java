package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.data.entity.TargetClass;
import com.realstudios.services.uploadservice.data.repository.TargetClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TargetClassServiceImpl implements TargetClassService {

    @Autowired
    TargetClassRepository targetClassRepository;

    @Override
    public void saveClasses(String[] classes, long file_id) {
        for (String clazz : classes) {
            TargetClass targetClazz = new TargetClass(file_id, Long.parseLong(clazz));
            targetClassRepository.save(targetClazz);
        }
    }
}
