package com.realstudios.services.uploadservice.business.publisher;

import com.realstudios.services.uploadservice.business.event.FileInsertedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class FileInsertedPublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(String message) {
        System.out.println("Publishing custom event");
        FileInsertedEvent fileInsertedEvent = new FileInsertedEvent(this, message);
        applicationEventPublisher.publishEvent(fileInsertedEvent);
    }
}
