package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.MediaHubResponse;
import com.realstudios.services.uploadservice.web.Constants;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Service
public class FilesStorageServiceImpl implements FileStorageService{

    private final RestTemplate restTemplate;
    private final HttpHeaders httpHeaders;

    public FilesStorageServiceImpl() {
        this.restTemplate = new RestTemplate();
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders.setBasicAuth(Constants.ENCODED_TOKEN);
    }

    @Override
    public void init() {

    }

    @Override
    public MediaHubResponse save(MultipartFile multipartFile) throws IOException {
        this.httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        LinkedMultiValueMap<String, String> headerMap = new LinkedMultiValueMap<>();
        headerMap.add("Content-disposition", "form-data; name=media_file; filename=" + multipartFile.getOriginalFilename());
        HttpEntity<byte[]> doc = new HttpEntity<byte[]>(multipartFile.getBytes(), headerMap);
        return getMediaHubResponse(doc);
    }


    @Override
    public MediaHubResponse save(File file, String title) throws IOException {
        this.httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        byte[] fileContent = Files.readAllBytes(file.toPath());
        LinkedMultiValueMap<String, String> headerMap = new LinkedMultiValueMap<>();
        headerMap.add("Content-disposition", "form-data; name=media_file; filename=" + "preview-"+title);
        HttpEntity<byte[]> doc = new HttpEntity<byte[]>(fileContent, headerMap);
        return getMediaHubResponse(doc);
    }

    @Override
    public Resource load(String filename) {
        return null;
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    private MediaHubResponse getMediaHubResponse(HttpEntity<byte[]> doc) {
        LinkedMultiValueMap<String, Object> multipartReqMap = new LinkedMultiValueMap<>();
        multipartReqMap.add("media_file", doc);

        HttpEntity<LinkedMultiValueMap<String, Object>> reqEntity = new HttpEntity<>(multipartReqMap, this.httpHeaders);
        ResponseEntity<MediaHubResponse> restTemp = this.restTemplate.exchange(Constants.MEDIA_HUB_FULL_URL, HttpMethod.POST, reqEntity, MediaHubResponse.class);
        return restTemp.getBody();
    }
}
