package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.data.entity.File;

public interface InstitutionTypeService {

    public void updateTargetInstitutions(Long id, String[] strings);
}
