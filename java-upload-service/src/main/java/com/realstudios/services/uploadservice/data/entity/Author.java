package com.realstudios.services.uploadservice.data.entity;

import javax.persistence.*;

@Entity
@Table( name = "author")
public class Author  {
    @Id
    @GeneratedValue
    @Column(name="id")
    private Long id;

    private String name;

//    @OneToOne(
//            mappedBy = "author",
//            cascade = {CascadeType.ALL}
//    )
//    private File file;

    public Author() {
    }

    public Author(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public File getFile() {
//        return file;
//    }
//
//    public void setFile(File file) {
//        this.file = file;
//    }
}
