package com.realstudios.services.uploadservice.web;

import org.apache.pdfbox.multipdf.PageExtractor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;

public class PDFUtility {

    private  MultipartFile multipartFile;
    private final PDDocument document;
    private  File file;

    public PDFUtility(MultipartFile multipartFile) throws IOException {
        File file = Utils.multipartToFile(multipartFile, multipartFile.getName());
        this.document = PDDocument.load(file);
        this.multipartFile = multipartFile;
    }

//    public PDFUtility(String url) throws IOException {
//         this.file = Utils.URLToFile(url);
//        this.file = Paths.get(url).toFile();
//         this.file = new File(url);

//        this.document = PDDocument.load(file);
//    }

    public int getNumberOfPages() {
        return this.document.getNumberOfPages();
    }

    public void extractPagesFromFile(String[] pages) throws IOException {
        int start = pages.length > 0 ? Integer.parseInt(pages[0]) : 0;
        int end = pages.length > 1 ? Integer.parseInt(pages[1]) : 0;
        encryptDocument();
        PageExtractor pageExtractor = new PageExtractor(this.document,start,end);
        pageExtractor.extract().save(file.getName());
    }

    public void extractPages(String[] pages) throws IOException {
        int start = pages.length > 0 ? Integer.parseInt(pages[0]) : 0;
        int end = pages.length > 1 ? Integer.parseInt(pages[1]) : 0;
        encryptDocument();
        PageExtractor pageExtractor = new PageExtractor(this.document,start,end);
        pageExtractor.extract().save(this.multipartFile.getOriginalFilename());
    }

    private void encryptDocument() throws IOException {
        int keyLength = 128;
        AccessPermission ap = new AccessPermission();
        ap.setCanPrint(false);
        ap.setReadOnly();
        StandardProtectionPolicy spp = new StandardProtectionPolicy("12345", "", ap);
        spp.setEncryptionKeyLength(keyLength);
        spp.setPermissions(ap);
        this.document.protect(spp);
    }

    public void encryptAndSave() throws IOException {
        encryptDocument();
        this.document.save("filename-encrypted.pdf");
    }

    public void close() throws IOException {
        this.document.close();
    }
}
