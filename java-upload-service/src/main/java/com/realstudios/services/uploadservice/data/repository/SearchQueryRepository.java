package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.SearchQuery;
import com.realstudios.services.uploadservice.web.ConverterInterface;
import com.realstudios.services.uploadservice.web.MonthlyReportInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchQueryRepository extends JpaRepository<SearchQuery, Long> {

    @Query(" SELECT c.query.name As name , COUNT(c.query) AS count\n" +
            ", SUM (case when c.type = 'impression' then 1 else 0 end) AS impression " +
            ", SUM (case when c.type = 'click' then 1 else 0 end) AS clicks " +
            "FROM SearchQuery c\n" +
            "GROUP BY c.query.id")
    List<ConverterInterface> findDistinctQueries();


    @Query(value = " SELECT  COUNT(*) AS count\n" +
            ", MONTHNAME (c.created) AS month " +
            ", sum(case when c.type = 'click' then 1 else 0 end) AS clicks " +
            ", SUM(case when c.type = 'impression' then 1 else 0 end) AS impression " +
            "FROM search_query AS c \n" +
            "WHERE YEAR(c.created) = YEAR(NOW()) \n" +
            "GROUP BY month LIMIT 1",
            nativeQuery = true)
    MonthlyReportInterface findMonthlyReports();

}
