package com.realstudios.services.uploadservice.web;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Constants {

    public static String BASE_URL = "https://mediahub.desafrica.com/api/";
    public static String MEDIA_HUB_BASE_URL = "https://mediahub.desafrica.com/";

    public static String MEDIA_HUB_FULL_URL = "https://mediahub.desafrica.com/api/v1/media";

    public static String MULTI_PART_FORM_DATA = "multipart/form-data";

    public static String ENCODED_TOKEN = "YWRtaW46bWVkaWFjbXM=";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit() {

        if (retrofit != null )retrofit = null;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit;
    }

}
