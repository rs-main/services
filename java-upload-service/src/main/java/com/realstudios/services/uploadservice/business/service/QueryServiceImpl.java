package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.entity.QueryTerm;
import com.realstudios.services.uploadservice.data.entity.SearchQuery;
import com.realstudios.services.uploadservice.data.repository.QueryRepository;
import com.realstudios.services.uploadservice.data.repository.SearchQueryRepository;
import com.realstudios.services.uploadservice.web.ConverterInterface;
import com.realstudios.services.uploadservice.web.MonthlyReportInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QueryServiceImpl implements QueryService{


    @Autowired
    QueryRepository queryRepository;

    @Autowired
    SearchQueryRepository searchQueryRepository;

    public Map<String, Object> getSearchQueries(){
        Map<String, Object> objectMap = new HashMap<>();
        List<ConverterInterface> distinctQueries = searchQueryRepository.findDistinctQueries();

        List<SearchQuery> searchQueryList = new ArrayList<>();
        for (ConverterInterface distinctQuery : distinctQueries) {
            String name = distinctQuery.getName();
            int count = distinctQuery.getCount();
            int clicks = distinctQuery.getClicks();
            int impression = distinctQuery.getImpression();
            float ctr = (float) (clicks / impression * 100);

            SearchQuery searchQuery = new SearchQuery();
            searchQuery.setName(name);
            searchQuery.setCount(count);
            searchQuery.setClicks(clicks);
            searchQuery.setImpression(impression);
            searchQuery.setCtr(ctr);
            searchQueryList.add(searchQuery);
        }

        objectMap.put("data", searchQueryList);

        return objectMap;
    }

    public Map<String, Object> getMonthlyQueriesStat(){
        Map<String, Object> objectMap = new HashMap<>();
        MonthlyReportInterface monthlyReports = searchQueryRepository.findMonthlyReports();
        objectMap.put("data", monthlyReports);
        return objectMap;
    }

//    @Async
    @Override
    public void insert(QueryTerm query, List<File> fileList) {
        QueryTerm queryObject = queryRepository.findQueryByName(query.getName());
        if (queryObject == null){
            queryObject = queryRepository.save(query);
        }

        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setType("search");
        searchQuery.setQuery(queryObject);
        searchQueryRepository.save(searchQuery);

        for (File file :fileList){
            SearchQuery searchQueryObject = new SearchQuery();
            searchQueryObject.setType("impression");
            searchQueryObject.setQuery(queryObject);
            searchQueryObject.setFileId(file.getId());
            searchQueryRepository.save(searchQueryObject);
        }

    }
}
