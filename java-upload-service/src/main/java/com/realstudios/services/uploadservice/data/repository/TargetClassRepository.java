package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.TargetClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetClassRepository extends JpaRepository<TargetClass,Long > {

}
