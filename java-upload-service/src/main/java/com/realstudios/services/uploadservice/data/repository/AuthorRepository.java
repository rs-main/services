package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.Author;
import com.realstudios.services.uploadservice.data.entity.File;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}
