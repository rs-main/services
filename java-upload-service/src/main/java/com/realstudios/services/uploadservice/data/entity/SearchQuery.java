package com.realstudios.services.uploadservice.data.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "search_query")
public class SearchQuery {
    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    @Column(name = "file_id")
    private long fileId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "query_id",
            referencedColumnName = "id",
            nullable = true,
            foreignKey = @ForeignKey(
                    name = "query_id_fk"
            )
    )
    private QueryTerm query;

    private String type;

    @Transient
    private String name;

    @Transient
    private int count;

    @Transient
    private int clicks;

    @Transient
    private int impression;

    @Transient
    private float ctr;

    private Date created;
    private Date updated;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public QueryTerm getQuery() {
        return query;
    }

    public void setQuery(QueryTerm query) {
        this.query = query;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getClicks() {
        return clicks;
    }

    public void setClicks(int clicks) {
        this.clicks = clicks;
    }

    public int getImpression() {
        return impression;
    }

    public void setImpression(int impression) {
        this.impression = impression;
    }

    public float getCtr() {
        return ctr;
    }

    public void setCtr(float ctr) {
        this.ctr = ctr;
    }
}
