package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.data.entity.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorService {

    public void insert(Author author);

    public Optional<Author> getAuthor(Long id);

    public List<Author> getAuthors();

    public void updateAuthor(Author author);

    public void deleteAuthor(Long id);
}
