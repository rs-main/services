package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.MediaDetailResponse;
import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.repository.FileRepository;
import com.realstudios.services.uploadservice.web.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import java.util.*;

@Service
public class FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public Map<String, Object> getFiles(String sort_by, String sort_value, int page, int size){
        List<File> fileList = new ArrayList<>();
        List<File> files = new ArrayList<>();
        Pageable pagingSort = PageRequest.of(page, size);

        Page<File> pageFiles;

        switch (sort_by){
            case "class":
                pageFiles = fileRepository.findAllByClassId(Long.parseLong(sort_value),pagingSort);
                break;

            case "paid":
                pageFiles = fileRepository.findAllByPaid(Boolean.parseBoolean((sort_value)),pagingSort);
                break;

            case "subject":
                pageFiles = fileRepository.findAllBySubjectId(Long.parseLong((sort_value)),pagingSort);
                break;

            case "type":
                pageFiles = fileRepository.findAllByType((sort_value),pagingSort);
                break;

            case "downloads":
                pageFiles = fileRepository.orderByDownloads(pagingSort);
                break;

            default:
                fileList = fileRepository.findAll();
        }

//        files = pageFiles.getContent();
        Map<String, Object> records = new HashMap<>();
        records.put("recordsTotal", fileList.size());
        records.put("recordsFiltered", fileList.size());
        records.put("draw", 1);
        records.put("data", fileList);

        return records;
//        return getPagination(files, pageFiles);
    }

    private Map<String, Object> getPagination(List<File> files, Page<File> pageFiles) {
        Map<String, Object> records = new HashMap<>();
        records.put("data", files);
        records.put("draw", pageFiles.getNumber());
        records.put("currentPage", pageFiles.getNumber());
        records.put("recordsTotal", pageFiles.getTotalElements());
        records.put("recordsFiltered", pageFiles.getTotalElements());
        records.put("totalPages", pageFiles.getTotalPages());
        return records;
    }

    public File insert(File file){
        return this.fileRepository.save(file);
    }

    public void insertAll(List<File> fileList){
         this.fileRepository.saveAll(fileList);
    }

    public File updateFile(File file){
        Optional<File> fileObject = fileRepository.findById(file.getId());
        if (fileObject.isPresent()){
            fileRepository.save(file);
        }
        return file;
    }

    public Optional<File> showFile(Long id){
         MediaDetailResponse mediaDetailResponse;
         Optional<File> file = fileRepository.findById(id);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(Constants.ENCODED_TOKEN);
        HttpEntity<LinkedMultiValueMap<String, Object>> reqEntity = new HttpEntity<>(headers);

         if (file.isPresent()){
             if (file.get().getFileUrl().contains("api")) {

                 if (file.get().getFileUrl() != null) {
                     ResponseEntity<MediaDetailResponse> mainMediaRestTemp = restTemplate.exchange(file.get().getFileUrl(), HttpMethod.GET, reqEntity, MediaDetailResponse.class);
                     mediaDetailResponse = mainMediaRestTemp.getBody();

                     if (mediaDetailResponse != null) {
                         if (mediaDetailResponse.getOriginal_media_url().contains(".pdf")){
                             file.get().setExtension(".pdf");
                         }
                         file.get().setFileUrl(String.format("%s%s", Constants.MEDIA_HUB_BASE_URL, mediaDetailResponse.getOriginal_media_url()));
                     }
                 }
             }

             if (file.get().getPreviewUrl() != null && file.get().getPreviewUrl().contains("api")) {
                 if (file.get().getPreviewUrl() != null) {
                     ResponseEntity<MediaDetailResponse> previewMediaRestTemp =
                             restTemplate.exchange(file.get().getPreviewUrl(), HttpMethod.GET, reqEntity, MediaDetailResponse.class);

                     if (previewMediaRestTemp.getBody() != null) {
                         file.get().setPreviewUrl(String.format("%s%s", Constants.MEDIA_HUB_BASE_URL, previewMediaRestTemp.getBody().getOriginal_media_url()));
                     }
                 }
             }

             if (file.get().getCoverArtUrl().contains("api")) {
                 if (file.get().getCoverArtUrl() != null) {
                     ResponseEntity<MediaDetailResponse> coverArtMediaRestTemp =
                             restTemplate.exchange(file.get().getCoverArtUrl(), HttpMethod.GET, reqEntity, MediaDetailResponse.class);

                     if (coverArtMediaRestTemp.getBody() != null) {
                         file.get().setCoverArtUrl(String.format("%s%s", Constants.MEDIA_HUB_BASE_URL, coverArtMediaRestTemp.getBody().getOriginal_media_url()));
                         String size = coverArtMediaRestTemp.getBody().getSize() != null ? coverArtMediaRestTemp.getBody().getSize() : "0MB";
                         file.get().setCoverArtSize( Float.parseFloat(size.replace("MB","")));
                     }
                 }
             }

                 fileRepository.save(file.get());
         }

         return file;
    }

    public void deleteFile(File file){
        Optional<File> fileObject = fileRepository.findById(file.getId());
        if (fileObject.isPresent()){
            this.fileRepository.delete(file);
        }
    }

    public Map<String, Object> counts(){
        int fileCounts = fileRepository.CountFiles();
        int bookCounts = fileRepository.countFilesByType("books");
        int videoCounts = fileRepository.countFilesByType("video");
        int audioCounts = fileRepository.countFilesByType("audio-book");

        Map<String, Object> map = new HashMap<>();
        map.put("counts", fileCounts);
        map.put("bookCounts", bookCounts);
        map.put("videoCounts", videoCounts);
        map.put("audioCounts", audioCounts);
        return map;
    }

    public List<File> search(String name){
        return fileRepository.findAllByNameContains(name);
    }

}
