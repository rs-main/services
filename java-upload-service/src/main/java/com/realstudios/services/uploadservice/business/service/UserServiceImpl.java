package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.User;
import com.realstudios.services.uploadservice.business.dto.UserDataResponse;
import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private FileRepository fileRepository;

    @Override
    public void init() {

    }

    @Async
    @Override
    public void updateUser(Long id, Long file_id) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        String url = (String.format("http://localhost/api/users/%s",id));
        System.out.println("Update User async");

        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LinkedMultiValueMap<String, Object>> reqEntity = new HttpEntity<>(headers);
        ResponseEntity<UserDataResponse> userRestTemp = restTemplate.exchange(url, HttpMethod.GET, reqEntity, UserDataResponse.class);

        if (userRestTemp.getBody() != null ){
        User userResponse = userRestTemp.getBody().getData();

        Optional<File> file = fileRepository.findById(file_id);
        if (file.isPresent()) {
            if (userResponse != null) {
                file.get().setUserName(String.format("%s %s",userResponse.getFirst_name(),userResponse.getLast_name()));
                System.out.printf("User name: %s%n", userResponse.getFirst_name());
                fileRepository.save(file.get());
            }
          }
        }
    }
}
