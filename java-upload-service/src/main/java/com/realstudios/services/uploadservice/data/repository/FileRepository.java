package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.File;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

//    @Query("select f from File f ORDER BY f.id DESC")
//    Page<File> findAll(Pageable pageable);

//    @Query("SELECT f from File f ORDER BY f.id DESC")
//    List<File> findAllFiles();

//    @Override
    @Query("SELECT f from File f ORDER BY f.id DESC")
    @NotNull
    List<File> findAll();

    Page<File> findAllByClassId(long classId, Pageable pageable);

    Page<File> findAllByPaid(boolean paid, Pageable pageable);

    Page<File> findAllBySubjectId(long subjectId, Pageable pageable);

    Page<File> findAllByType(String type, Pageable pageable);

    @Query("select u from File u ORDER BY u.downloads DESC")
    Page<File> orderByDownloads(Pageable pageable);

    @Query("select COUNT (u.id) from File u")
    int CountFiles();

    @Query("select COUNT(u.id) from File u where u.type = ?1")
    int countFilesByType(String type);

    List<File> findAllByNameContains(String name);

    //    @Query("select u from File u where u.classId = ?1")
//    Page<File> findAllByClassId(long class_id);

//    List<File> findAllById(Pageable pageable);
}