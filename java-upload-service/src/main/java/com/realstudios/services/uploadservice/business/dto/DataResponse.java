package com.realstudios.services.uploadservice.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataResponse {
    @JsonProperty("data")
    private Subject[] data;

    public Subject[] getData() {
        return data;
    }

    public void setData(Subject[] data) {
        this.data = data;
    }
}
