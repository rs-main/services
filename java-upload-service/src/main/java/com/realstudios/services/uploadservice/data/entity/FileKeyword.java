package com.realstudios.services.uploadservice.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "file_keyword")
public class FileKeyword {
    @Id
    private long id;

    @Column(name = "file_id")
    private long fileId;

    @Column(name = "keyword_id")
    private long keywordId;


    public FileKeyword() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public long getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(long keywordId) {
        this.keywordId = keywordId;
    }
}
