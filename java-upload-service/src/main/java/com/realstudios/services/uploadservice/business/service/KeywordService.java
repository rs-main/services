package com.realstudios.services.uploadservice.business.service;

public interface KeywordService {

    public void init();

    public void updateUser(Long id, Long file_id);
}
