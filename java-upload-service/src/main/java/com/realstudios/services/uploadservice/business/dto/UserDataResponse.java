package com.realstudios.services.uploadservice.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDataResponse {
    @JsonProperty("user")
    private User data;

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}
