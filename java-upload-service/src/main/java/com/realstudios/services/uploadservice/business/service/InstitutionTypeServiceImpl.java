package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.InstitutionTypeResponse;
import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.entity.InstitutionType;
import com.realstudios.services.uploadservice.data.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;


@Service
public class InstitutionTypeServiceImpl implements InstitutionTypeService{

    @Autowired
    private Environment env;

    @Autowired
    private FileRepository fileRepository;


    @Async
    @Override
    public void updateTargetInstitutions(Long id, String[] strings) {

        this.fetchTypes(Arrays.asList(strings)).subscribe(response -> {
            InstitutionTypeResponse institution = (InstitutionTypeResponse) response;
            InstitutionType institutionType = institution.getData();
            Map<String, String> map = new HashMap<>();
            map.put(institutionType.getName().toLowerCase()
                    .replace(" ", "-"), institutionType.getName());
            System.out.println(institution.getData().getName());
            Optional<File> file = fileRepository.findById(id);
            if (file.isPresent()) {
                map.putAll(file.get().getTargetInstitutionTypes());
                file.get().setTargetInstitutionTypes(map);
                fileRepository.save(file.get());
            }
        });
    }
//    {"basic":"Basic","senior-high":"Senior High","professional":"Professional"}
//    {"basic":"Basic","senior-high":"Senior High","professional":"Professional"}

    public Mono<InstitutionTypeResponse> getResponse(String id) {
        WebClient webClient = WebClient.create("http://localhost");

        System.out.println("Institution Type Id");

        return webClient.get()
                .uri("/api/institution-types/{id}", id)
                .retrieve()
                .bodyToMono(InstitutionTypeResponse.class);
    }

    public Flux fetchTypes(List<String> userIds) {
        return Flux.fromIterable(userIds)
                .flatMap(this::getResponse);
    }
}
