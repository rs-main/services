package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.MediaHubResponse;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageService {

    public void init();

    public MediaHubResponse save(MultipartFile multipartFile) throws IOException;

    public MediaHubResponse save(File file, String title) throws IOException;

    public Resource load(String filename);

    public void deleteAll();

    public Stream<Path> loadAll();

}