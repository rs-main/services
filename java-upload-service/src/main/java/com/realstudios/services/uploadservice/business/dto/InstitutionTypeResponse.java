package com.realstudios.services.uploadservice.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.realstudios.services.uploadservice.data.entity.InstitutionType;

public class InstitutionTypeResponse {

    @JsonProperty("data")
    private InstitutionType data;

    public InstitutionType getData() {
        return data;
    }

    public void setData(InstitutionType data) {
        this.data = data;
    }
}
