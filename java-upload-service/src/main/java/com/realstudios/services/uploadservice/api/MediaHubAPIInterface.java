package com.realstudios.services.uploadservice.api;

import com.realstudios.services.uploadservice.business.dto.MediaHubResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface MediaHubAPIInterface {
    @Multipart
    @POST("/api/v1/media")
    Call<MediaHubResponse> addMedia(@Header("Authorization") String token,
                                    @Part("title") RequestBody title,
                                    @Part("description") RequestBody description,
                                    @Part MultipartBody.Part media_file);
}
