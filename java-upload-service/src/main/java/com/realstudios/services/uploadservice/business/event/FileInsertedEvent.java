package com.realstudios.services.uploadservice.business.event;

import org.springframework.context.ApplicationEvent;

public class FileInsertedEvent extends ApplicationEvent {

    private String message;

    public FileInsertedEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
