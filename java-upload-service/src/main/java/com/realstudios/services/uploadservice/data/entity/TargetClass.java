package com.realstudios.services.uploadservice.data.entity;

import javax.persistence.*;

@Entity
public class TargetClass {

    @GeneratedValue
    @Id
    private long id;

    @Column(name = "file_id")
    private long fileId;

    @Column(name = "class_id")
    private long classId;

    public TargetClass( long fileId, long classId) {
        this.fileId = fileId;
        this.classId = classId;
    }

    public TargetClass() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }
}
