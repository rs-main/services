package com.realstudios.services.uploadservice.data.entity;

public class User {
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private String phoneNumber;
    private String dob;
    private long courseId;
    private long classId;
    private String photo;

}
