package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeywordRepository extends JpaRepository<Keyword, Long> {

}
