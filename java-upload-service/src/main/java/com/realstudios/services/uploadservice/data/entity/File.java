package com.realstudios.services.uploadservice.data.entity;

import com.realstudios.services.uploadservice.converter.StringMapConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "files")
public class File {
    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    @Column(name="service_id")
    private long serviceId;

    @Column(name="service")
    private String service;

    private String name;
    private String description;
    private String type;
    private String isbn;
    private String storage;

    @Column(name="file_url")
    private String fileUrl;

    @Column(name="preview_url")
    private String previewUrl;

    @Column(name="file_size")
    private float fileSize;
    private String extension;

    @Column(name="cover_art_url")
    private String coverArtUrl;

    @Column(name="cover_art_size")
    private float  coverArtSize;

    @Column(name="cover_art_extension")
    private String coverArtExtension;
    private int downloads;
    private int views;

    @Column(name="page_count")
    private int pageCount;
    private int duration;

    @Column(name="class_id")
    private long classId;

    @Column(name="subject_id")
    private long subjectId;

    @Column(name="subject_name")
    private String subjectName;

    @Column(name="user_id")
    private long userId;

    private String language;
    private boolean paid;
    private boolean featured;

    @Column(name="user_name")
    private String userName;

    @Column(name="created_at")
    private String createdAt;

    @Column(name="updated_at")
    private String updatedAt;

    @Column(name="published")
    private boolean published;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "author_id",
            referencedColumnName = "id",
            nullable = true,
            foreignKey = @ForeignKey(
                    name = "author_id_fk"
            )
    )
    private Author author;


    /* This converter does the trick */
    @Convert(converter = StringMapConverter.class)
    @Column(name="target_institutions")
    private Map<String, String> targetInstitutionTypes;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "keyword_id",
            referencedColumnName = "id",
            nullable = true,
            foreignKey = @ForeignKey(
                    name = "keyword_id_fk"
            )
    )
    private List<Keyword> keywords;

    public File() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public float getFileSize() {
        return fileSize;
    }

    public void setFileSize(float fileSize) {
        this.fileSize = fileSize;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getCoverArtUrl() {
        return coverArtUrl;
    }

    public void setCoverArtUrl(String coverArtUrl) {
        this.coverArtUrl = coverArtUrl;
    }

    public float getCoverArtSize() {
        return coverArtSize;
    }

    public void setCoverArtSize(float coverArtSize) {
        this.coverArtSize = coverArtSize;
    }

    public String getCoverArtExtension() {
        return coverArtExtension;
    }

    public void setCoverArtExtension(String coverArtExtension) {
        this.coverArtExtension = coverArtExtension;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Map<String, String> getTargetInstitutionTypes() {
        return targetInstitutionTypes;
    }

    public void setTargetInstitutionTypes(Map<String, String> targetInstitutionTypes) {
        this.targetInstitutionTypes = targetInstitutionTypes;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }
}
