package com.realstudios.services.uploadservice.business.service;

public interface SubjectService {

    public void init();

    public void updateSubject(Long id, Long file_id);
}
