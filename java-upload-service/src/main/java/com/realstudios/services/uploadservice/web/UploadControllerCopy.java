//package com.realstudios.services.uploadservice.web;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.realstudios.services.uploadservice.business.service.*;
//import com.realstudios.services.uploadservice.business.serviceResponse.MediaHubResponse;
//import com.realstudios.services.uploadservice.data.entity.File;
//import com.realstudios.services.uploadservice.data.entity.QueryTerm;
//import com.realstudios.services.uploadservice.data.repository.SearchQueryRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//
//import static org.springframework.web.bind.annotation.RequestMethod.POST;
//
//@RestController
//@RequestMapping("/api/uploads")
//public class UploadControllerCopy {
//
//
//    @Autowired
//    FileStorageService filesStorageService;
//
//    @Autowired
//    FileService fileService;
//
//    @Autowired
//    AuthorService authorService;
//
//    @Autowired
//    SubjectService subjectService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    TargetClassService targetClassService;
//
//    @Autowired
//    QueryService queryService;
//
//    @Autowired
//    SearchQueryRepository searchQueryRepository;
//
//    @GetMapping
//    public ResponseEntity<Map<String, Object>> getUploads(@RequestParam(required = false) String title,
//                                                          @RequestParam(defaultValue = "0") int draw,
//                                                          @RequestParam(defaultValue = "100") int size,
//                                                          @RequestParam(defaultValue = "id") String sort_by,
//                                                          @RequestParam(defaultValue = "0") String sort_value) {
//
//        Map<String, Object> response = fileService.getFiles(sort_by,sort_value,draw,size);
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }
//
//    @GetMapping("{id}")
//    public ResponseEntity<Optional<File>> showUpload(@PathVariable(name = "id") long id) {
//
//        return new ResponseEntity<>(fileService.showFile(id),HttpStatus.OK);
//    }
//
////    @RequestMapping( method = POST)
////    public ResponseEntity saveUploads(@RequestParam(name = "name" ) String name,
////                                      @RequestParam(name = "description") String description,
////                                      @RequestParam(name = "type" ) String type,
////                                      @RequestParam(name = "service" ) String service,
////                                      @RequestParam(name = "isbn", required = false) String isbn,
////                                      @RequestParam(name = "class_id", required = false) Long classId,
////                                      @RequestParam(name = "subject_id", required = false) Long subjectId,
////                                      @RequestParam(name = "user_id") long userId,
////                                      @RequestParam(name = "author_id") long author_id,
////                                      @RequestParam(name = "language", required = false) String language,
////                                      @RequestParam(name = "paid", required = false) boolean paid,
////                                      @RequestParam(name = "featured", required = false) boolean featured,
////                                      @RequestParam(name = "preview_pages", required = false) String[] previewPages,
////                                      @RequestParam(name = "target_classes") String[] targetClasses,
////                                      @RequestParam(name = "keywords")       String[] keywords,
////                                      @RequestParam(name ="cover_art_url") String cover_art_url,
////                                      @RequestParam(name ="media_file_url") String media_file_url) throws IOException {
////
////
////        MediaDetailResponse mediaDetailResponse = Utils.getMediaDetailResponse(media_file_url);
////
////        File fileModel = new File();
////
////        if (mediaDetailResponse != null) {
////            String mediaDetailUrl =
////                    Constants.MEDIA_HUB_BASE_URL+mediaDetailResponse.getOriginal_media_url();
////
////            fileModel.setName(name);
////            fileModel.setDownloads(0);
////            fileModel.setDescription(description);
////            fileModel.setType(type);
//////                        fileModel.setClassId(classId);
////            fileModel.setSubjectId(subjectId);
////            fileModel.setUserId(userId);
////            fileModel.setLanguage(language);
////            fileModel.setPaid(paid);
////            fileModel.setFeatured(featured);
////            fileModel.setPublished(true);
////            fileModel.setFileUrl(mediaDetailUrl);
////            if (mediaDetailResponse.getSize() != null) {
////                fileModel.setFileSize(Float.parseFloat(mediaDetailResponse.getSize().replace("MB", "")));
////            }
////            fileModel.setCoverArtUrl(cover_art_url);
////            Optional<Author> author = authorService.getAuthor(author_id);
////            author.ifPresent(fileModel::setAuthor);
////
//////          fileModel.setCoverArtSize(coverArtFile.getSize());
////            fileModel.setIsbn(isbn);
////            fileModel.setCreatedAt(mediaDetailResponse.getAdd_date());
////            fileModel.setUpdatedAt(mediaDetailResponse.getAdd_date());
////            fileModel.setDuration(mediaDetailResponse.getDuration());
////            fileModel.setService(service);
////
////            try{
////            if (mediaDetailUrl.contains(".pdf")) {
////                PDFUtility pdfUtility = new PDFUtility(mediaDetailUrl);
////                pdfUtility.extractPagesFromFile(previewPages);
////                countPages(pdfUtility, fileModel);
////                savePreviewPDF(mediaDetailUrl, fileModel);
////                pdfUtility.close();
////            }
////            }catch (Exception $ex){
////
////            }
////
////            fileService.insert(fileModel);
////            subjectService.updateSubject(subjectId, fileModel.getId());
////            userService.updateUser(userId, fileModel.getId());
//////          targetClassService.saveClasses(targetClasses, fileModel.getId());
////        }
////
////        Map<String, Object> map = new HashMap<>();
////        map.put("data", fileModel);
////        return ResponseEntity.status(HttpStatus.OK).body( new ObjectMapper().writeValueAsString(map));
////
////    }
//
//
//    @RequestMapping( method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
////    @CrossOrigin(origins = "http://localhost:8082")
//    public ResponseEntity saveUploads(@RequestParam(name = "name" ) String name,
//                                      @RequestParam(name = "description") String description,
//                                      @RequestParam(name = "type" ) String type,
//                                      @RequestParam(name = "service" ) String service,
//                                      @RequestParam(name = "isbn", required = false) String isbn,
//                                      @RequestParam(name = "class_id", required = false) Long classId,
//                                      @RequestParam(name = "subject_id", required = false) Long subjectId,
//                                      @RequestParam(name = "user_id") long userId,
//                                      @RequestParam(name = "language", required = false) String language,
//                                      @RequestParam(name = "paid", required = false) boolean paid,
//                                      @RequestParam(name = "featured", required = false) boolean featured,
//                                      @RequestParam(name = "preview_pages", required = false) String[] previewPages,
//                                      @RequestParam(name = "target_classes") String[] targetClasses,
//                                      @RequestParam(name = "keywords")       String[] keywords,
//                                      @RequestPart(name ="cover_art_file") MultipartFile coverArtFile,
//                                      @RequestPart(name ="media_file") MultipartFile multipartFile) throws IOException {
//
//
//        try {
//            MediaHubResponse mediaHubResponse = filesStorageService.save(multipartFile, name, description);
//            MediaHubResponse coverArtWorkResponse = filesStorageService.save(coverArtFile,name,description);
//            PDFUtility pdfUtility = new PDFUtility(multipartFile);
//            pdfUtility.extractPages(previewPages);
//
//            File fileModel = new File();
//            fileModel.setName(name);
//            fileModel.setDownloads(0);
//            fileModel.setDescription(description);
//            fileModel.setType(type);
//            fileModel.setClassId(classId);
//            fileModel.setSubjectId(subjectId);
//            fileModel.setUserId(userId);
//            fileModel.setLanguage(language);
//            fileModel.setPaid(paid);
//            fileModel.setFeatured(featured);
//            fileModel.setPublished(true);
//            fileModel.setFileUrl(mediaHubResponse.getApi_url());
//            fileModel.setFileSize(multipartFile.getSize());
//            fileModel.setCoverArtUrl(coverArtWorkResponse.getApi_url());
//            fileModel.setCoverArtSize(coverArtFile.getSize());
//            fileModel.setIsbn(isbn);
//            fileModel.setCreatedAt(mediaHubResponse.getAdd_date());
//            fileModel.setUpdatedAt(mediaHubResponse.getAdd_date());
//            countPages(multipartFile, mediaHubResponse, pdfUtility, fileModel);
//            fileModel.setDuration(mediaHubResponse.getDuration());
//            fileModel.setService(service);
//            savePreviewPDF(multipartFile, fileModel);
//            fileService.insert(fileModel);
//            pdfUtility.close();
//
//            subjectService.updateSubject(subjectId, fileModel.getId());
//            userService.updateUser(userId, fileModel.getId());
//            targetClassService.saveClasses(targetClasses, fileModel.getId());
//
//            return ResponseEntity.status(HttpStatus.OK).body(fileModel);
//        }catch (Exception exception){
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
//        }
//    }
//
//
//    @RequestMapping(value = "/image", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
//    public ResponseEntity saveImage(
//            @RequestParam( name = "name", required = false) String name,
//            @RequestParam( name = "description", required = false) String description,
//            @RequestPart(name ="file") MultipartFile multipartFile) throws IOException {
//
//        MediaHubResponse mediaHubResponse = filesStorageService.save(multipartFile);
//
//        Map<String, String> map = new HashMap<>();
//        map.put("url", mediaHubResponse.getApi_url());
//
//        return ResponseEntity.status(HttpStatus.OK).body( new ObjectMapper().writeValueAsString(map));
//    }
//
//    @GetMapping("/stats")
//    public ResponseEntity stats(){
//
//        Map<String,Object> objectMap = new HashMap<>();
//        objectMap.put("data", fileService.counts());
//        return ResponseEntity.status(HttpStatus.OK).body(objectMap);
//    }
//
//    @GetMapping("/search")
//    public ResponseEntity<Map<String,Object>> search(@RequestParam( name = "name", required = false) String name){
//        Map<String,Object> objectMap = new HashMap<>();
//        List<File> files = fileService.search(name);
//        QueryTerm query = new QueryTerm();
//        query.setName(name);
//        queryService.insert(query, files);
//        objectMap.put("data", files);
//        return ResponseEntity.status(HttpStatus.OK).body(objectMap);
//    }
//
//    @GetMapping("/queries")
//    public ResponseEntity<Map<String, Object>> queries(){
//        return ResponseEntity.status(HttpStatus.OK).body(queryService.getSearchQueries());
//    }
//
//    @GetMapping("/query-reports")
//    public ResponseEntity<Map<String, Object>> queryReports(){
//        return ResponseEntity.status(HttpStatus.OK).body(queryService.getMonthlyQueriesStat());
//    }
//
//    private void countPages( PDFUtility pdfUtility, File fileModel) {
//        fileModel.setPageCount(pdfUtility.getNumberOfPages());
//    }
//
//    private void savePreviewPDF(String url, File fileModel) throws IOException {
//        java.io.File file = Utils.URLToFile(url);
//
//        if (file.exists()) {
//            MediaHubResponse previewResponse =
//                    filesStorageService.save(file, fileModel.getName()+".pdf");
//
//            fileModel.setPreviewUrl(previewResponse.getApi_url());
//        }
//
//    }
//
//}
