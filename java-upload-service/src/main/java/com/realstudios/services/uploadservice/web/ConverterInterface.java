package com.realstudios.services.uploadservice.web;

public interface ConverterInterface {
        String getName();
        Integer getImpression();
        Integer getClicks();
        Integer getCount();
}
