package com.realstudios.services.uploadservice.business.service;

public interface TargetClassService {

    void saveClasses(String[] classes, long file_id);
}
