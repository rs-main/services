package com.realstudios.services.uploadservice.business.service;

import com.realstudios.services.uploadservice.business.dto.DataResponse;
import com.realstudios.services.uploadservice.business.dto.Subject;
import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import java.util.Optional;


@Service
public class SubjectServiceImpl implements SubjectService{

    @Autowired
    private FileRepository fileRepository;

    @Override
    public void init() {

    }

    @Async
    @Override
    public void updateSubject(Long id, Long file_id) {

        this.getSubject((id)).subscribe(response-> {
            Subject subject = response.getData()[0];
            Optional<File> file = fileRepository.findById(file_id);

            if (file.isPresent()) {
                file.get().setSubjectName(subject.getName());
                System.out.printf("Subject name: %s%n", subject.getName());
                fileRepository.save(file.get());
            }
        });
    }

    public Mono<DataResponse> getSubject(long id) {
        WebClient webClient = WebClient.create("http://localhost");

        return webClient.get()
                .uri("/api/subjects/{id}", id)
                .retrieve()
                .bodyToMono(DataResponse.class);
    }

}
