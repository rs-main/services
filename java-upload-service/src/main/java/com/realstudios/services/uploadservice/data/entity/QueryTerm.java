package com.realstudios.services.uploadservice.data.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class QueryTerm {
    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    private String name;

    private Date created;
    private Date updated;

    public QueryTerm(String name) {
        this.name = name;
    }

    public QueryTerm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }


}
