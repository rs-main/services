package com.realstudios.services.uploadservice.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.realstudios.services.uploadservice.business.service.*;
import com.realstudios.services.uploadservice.business.dto.MediaHubResponse;
import com.realstudios.services.uploadservice.data.entity.Author;
import com.realstudios.services.uploadservice.data.entity.File;
import com.realstudios.services.uploadservice.data.entity.Keyword;
import com.realstudios.services.uploadservice.data.entity.QueryTerm;
import com.realstudios.services.uploadservice.data.repository.KeywordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/uploads")
public class UploadController {

    @Autowired
    FileStorageService filesStorageService;

    @Autowired
    FileService fileService;

    @Autowired
    AuthorService authorService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    UserService userService;

    @Autowired
    InstitutionTypeService institutionTypeService;

    @Autowired
    QueryService queryService;

    @Autowired
    KeywordRepository keywordRepository;

    @GetMapping
    public ResponseEntity<Map<String, Object>> getUploads(@RequestParam(required = false) String title,
                                                          @RequestParam(defaultValue = "0") int draw,
                                                          @RequestParam(defaultValue = "100") int size,
                                                          @RequestParam(defaultValue = "id") String sort_by,
                                                          @RequestParam(defaultValue = "0") String sort_value) {

        Map<String, Object> response = fileService.getFiles(sort_by,sort_value,draw,size);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Optional<File>> showUpload(@PathVariable(name = "id") long id) {

        return new ResponseEntity<>(fileService.showFile(id),HttpStatus.OK);
    }


    @RequestMapping( method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity saveUploads(@RequestParam(name = "name" ) String name,
                                      @RequestParam(name = "description") String description,
                                      @RequestParam(name = "type" ) String type,
                                      @RequestParam(name = "service" ) String service,
                                      @RequestParam(name = "isbn", required = false) String isbn,
                                      @RequestParam(name = "class_id", required = false) Long classId,
                                      @RequestParam(name = "subject_id", required = false) Long subjectId,
                                      @RequestParam(name = "user_id") long userId,
                                      @RequestParam(name = "author_id") long authorId,
                                      @RequestParam(name = "language", required = false) String language,
                                      @RequestParam(name = "paid", required = false) boolean paid,
                                      @RequestParam(name = "featured", required = false) boolean featured,
                                      @RequestParam(name = "preview_pages", required = false) String[] previewPages,
                                      @RequestParam(name = "target_classes") String[] targetClasses,
                                      @RequestParam(name = "keywords")       String[] keywords,
                                      @RequestPart(name ="cover_art_file") MultipartFile coverArtFile,
                                      @RequestPart(name ="media_file") MultipartFile multipartFile) throws IOException {


            MediaHubResponse mediaHubResponse = filesStorageService.save(multipartFile);
            MediaHubResponse coverArtWorkResponse = filesStorageService.save(coverArtFile);
            PDFUtility pdfUtility = new PDFUtility(multipartFile);
            pdfUtility.extractPages(previewPages);
            File fileModel = new File();
            fileModel.setName(name);
            fileModel.setDownloads(0);
            fileModel.setDescription(description);
            fileModel.setType(type);
//            fileModel.setClassId(classId);
            fileModel.setSubjectId(subjectId);
            fileModel.setUserId(userId);
            fileModel.setLanguage(language);
            fileModel.setPaid(paid);
            fileModel.setFeatured(featured);
            fileModel.setPublished(true);
            fileModel.setFileUrl(mediaHubResponse.getApi_url());
            fileModel.setFileSize(multipartFile.getSize());
            fileModel.setCoverArtUrl(coverArtWorkResponse.getApi_url());
            fileModel.setCoverArtSize(coverArtFile.getSize());
            fileModel.setIsbn(isbn);
            fileModel.setCreatedAt(mediaHubResponse.getAdd_date());
            fileModel.setUpdatedAt(mediaHubResponse.getAdd_date());
            countPages(multipartFile, mediaHubResponse, pdfUtility, fileModel);
            fileModel.setDuration(mediaHubResponse.getDuration());
            fileModel.setService(service);
            Optional<Author> author = authorService.getAuthor(authorId);
            author.ifPresent(fileModel::setAuthor);
            setKeywords(keywords, fileModel);
//          savePreviewPDF(multipartFile, fileModel);
            fileService.insert(fileModel);
            pdfUtility.close();

            subjectService.updateSubject(subjectId, fileModel.getId());
            institutionTypeService.updateTargetInstitutions(fileModel.getId(), targetClasses);
            return ResponseEntity.status(HttpStatus.OK).body(fileModel);
    }



    @RequestMapping(value = "/image", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity saveImage(
            @RequestParam( name = "name", required = false) String name,
            @RequestParam( name = "description", required = false) String description,
            @RequestPart(name ="file") MultipartFile multipartFile) throws IOException {

        MediaHubResponse mediaHubResponse = filesStorageService.save(multipartFile);

        Map<String, String> map = new HashMap<>();
        map.put("url", mediaHubResponse.getApi_url());

        return ResponseEntity.status(HttpStatus.OK).body( new ObjectMapper().writeValueAsString(map));
    }

    @GetMapping("/stats")
    public ResponseEntity stats(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put("data", fileService.counts());
        return ResponseEntity.status(HttpStatus.OK).body(objectMap);
    }

    @GetMapping("/search")
    public ResponseEntity<Map<String,Object>> search(@RequestParam( name = "name", required = false) String name){
        Map<String,Object> objectMap = new HashMap<>();
        List<File> files = fileService.search(name);
        QueryTerm query = new QueryTerm();
        query.setName(name);
        queryService.insert(query, files);
        objectMap.put("data", files);
        return ResponseEntity.status(HttpStatus.OK).body(objectMap);
    }

    @GetMapping("/queries")
    public ResponseEntity<Map<String, Object>> queries(){
        return ResponseEntity.status(HttpStatus.OK).body(queryService.getSearchQueries());
    }

    @GetMapping("/query-reports")
    public ResponseEntity<Map<String, Object>> queryReports(){
        return ResponseEntity.status(HttpStatus.OK).body(queryService.getMonthlyQueriesStat());
    }

    private void countPages(MultipartFile multipartFile,MediaHubResponse mediaHubResponse, PDFUtility pdfUtility, File fileModel) {
        fileModel.setPageCount(pdfUtility.getNumberOfPages());
    }

    private void savePreviewPDF(MultipartFile multipartFile, File fileModel) throws IOException {
        java.io.File file = Utils.multipartToFile(multipartFile, fileModel.getName());
        if (file.exists()) {
            MediaHubResponse previewResponse =
                    filesStorageService.save(file, fileModel.getName()+".pdf");

            fileModel.setPreviewUrl(previewResponse.getApi_url());
        }

    }

    private void setKeywords(String[] keywords, File fileModel) {
        List<Keyword> keywordList = new ArrayList<>();
        for (String s : keywords) {
            Keyword keyword = new Keyword();
            keyword.setName(s);
            keywordList.add(keyword);
        }
        List<Keyword> keys = keywordRepository.saveAll(keywordList);
        fileModel.setKeywords(keys);
    }

}
