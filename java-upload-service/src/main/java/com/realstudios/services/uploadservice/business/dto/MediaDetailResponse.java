package com.realstudios.services.uploadservice.business.dto;

public class MediaDetailResponse {

    private String original_media_url;

    private String size;

    private String add_date;

    private int duration;

    public String getOriginal_media_url() {
        return original_media_url;
    }

    public void setOriginal_media_url(String original_media_url) {
        this.original_media_url = original_media_url;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
