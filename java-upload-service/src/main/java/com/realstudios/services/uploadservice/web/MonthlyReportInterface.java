package com.realstudios.services.uploadservice.web;

public interface MonthlyReportInterface {
        String getMonth();
        Integer getCount();
        Integer getImpression();
        Integer getClicks();
}
