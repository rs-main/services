package com.realstudios.services.uploadservice.web;


import com.realstudios.services.uploadservice.business.dto.MediaDetailResponse;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.multipdf.PageExtractor;
import org.apache.pdfbox.pdmodel.PDDocument;

import org.springframework.http.*;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class Utils {

    public void write(MultipartFile file, Path dir) {
        Path filepath = Paths.get(dir.toString(), file.getOriginalFilename());

        try (OutputStream os = Files.newOutputStream(filepath)) {
            os.write(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
        multipart.transferTo(convFile);
        return convFile;
    }

    public static int getNumberOfPages(MultipartFile multipartFile) throws IOException {
        java.io.File file = Utils.multipartToFile(multipartFile,multipartFile.getName());
        PDDocument doc = PDDocument.load(file);
        return doc.getNumberOfPages();
    }

    public static void extractPages(MultipartFile multipartFile, int start, int end) throws IOException {
        java.io.File file = Utils.multipartToFile(multipartFile,multipartFile.getName());
        PDDocument doc = PDDocument.load(file);
        PageExtractor pageExtractor = new PageExtractor(doc,start,end);
        pageExtractor.extract().save(multipartFile.getOriginalFilename());
        doc.close();
    }


    public static Optional<String> getFileExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    public static MediaDetailResponse getMediaDetailResponse(String media_file_url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(Constants.ENCODED_TOKEN);
        HttpEntity<LinkedMultiValueMap<String, Object>> reqEntity = new HttpEntity<>(headers);
        ResponseEntity<MediaDetailResponse> mainMediaRestTemp =
                restTemplate.exchange(media_file_url, HttpMethod.GET, reqEntity, MediaDetailResponse.class);
        return mainMediaRestTemp.getBody();
    }


    public static File URLToFile(String url) throws IOException {
        File file = new File("downloaded.pdf");
        FileUtils.copyURLToFile(new URL(url), file);
        return file;
    }


}
