package com.realstudios.services.uploadservice.data.repository;

import com.realstudios.services.uploadservice.data.entity.QueryTerm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QueryRepository extends JpaRepository<QueryTerm, Long> {

    @Query("select q from QueryTerm q where q.name = ?1")
    QueryTerm findQueryByName(String name);

}
