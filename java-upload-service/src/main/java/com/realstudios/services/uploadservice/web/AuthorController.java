package com.realstudios.services.uploadservice.web;

import com.realstudios.services.uploadservice.business.service.*;
import com.realstudios.services.uploadservice.data.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/authors")
public class AuthorController {

    @Autowired
    AuthorService authorService;


    @GetMapping
    public ResponseEntity<Map<String, Object>> getAuthors() {
        List<Author> authorList = authorService.getAuthors();
        Map<String, Object> authors = new HashMap<>();
        authors.put("data",authorList);
        authors.put("message","success");
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

    @GetMapping("{id}")
    public ResponseEntity<Optional<Author>> showUpload(@PathVariable(name = "id") long id) {

        return null;
    }

    @RequestMapping( method = POST)
    public ResponseEntity save(@RequestParam(name = "name" ) String name) {
        Author author = new Author(name);
        authorService.insert(author);
        Map<String, Object> authors = new HashMap<>();
        authors.put("data", author);
        authors.put("message", "success");
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

}
