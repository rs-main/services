<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class ClassService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $class_url;


    public function __construct()
    {
        $this->base_url          = config("services.classes.base_url");
        $this->secret            = config("services.classes.secret");
        $this->class_url        = config("services.classes.endpoints.classes_url");
    }

    public function getClasses($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->class_url,$options, []);
    }

    public function saveClass($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->class_url,$options,$headers);
    }

    public function getClass($id){
        $fetch_url = $this->class_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteClass($id){
        $delete_url = $this->class_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
