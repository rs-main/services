<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class FileUploadService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $files_url;
    private  $upload_url;
    private  $upload_file_url;
    private  $fetch_url;
    private  $bookmark_url;
    private  $bookmark_file_url;
    private  $delete_url;
    private  $batch_upload_url;
    private  $client_storage_url;
    private  $stats_url;

    public function __construct()
    {
        $this->base_url          = config("services.uploads.base_url");
        $this->secret            = config("services.uploads.secret");
        $this->files_url         = config("services.uploads.endpoints.files_url");
        $this->upload_url        = config("services.uploads.endpoints.upload_url");
        $this->upload_file_url   = config("services.uploads.endpoints.upload_file_url");
        $this->batch_upload_url  = config("services.uploads.endpoints.batch_upload_url");
        $this->fetch_url         = config("services.uploads.endpoints.fetch_url");
        $this->bookmark_url      = config("services.uploads.endpoints.bookmark_url");
        $this->bookmark_file_url = config("services.uploads.endpoints.bookmark_file_url");
        $this->delete_url        = config("services.uploads.endpoints.delete_url");
        $this->client_storage_url = config("services.uploads.endpoints.client_storage_url");
        $this->stats_url = config("services.uploads.endpoints.stats_url");
    }

    public function allFiles($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->files_url,$options, []);
    }

    public function upload($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->upload_url,$options,$headers);
    }

    public function uploadFile($options, $file,$file_name){
        return $this->performRequestWithFile($this->base_url,$this->secret,$this->upload_file_url,$options,$file,$file_name);
    }

    public function fetchFile($id){
        $fetch_url = $this->fetch_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function getBookMark($user_id,$file_id){
        $like_url = $this->bookmark_url.$user_id."/".$file_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$like_url,[],[]);
    }

    public function bookMark($options, $headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->bookmark_file_url,$options,$headers);
    }

    public function getBookMarks($options, $headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->bookmark_file_url,$options,$headers);
    }

    public function batchUploads($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->batch_upload_url,$options,$headers);
    }

    public function deleteFile($id){
        $delete_url = $this->delete_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

    public function addClientStorage($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->client_storage_url,$options,$headers);
    }

    public function getStorage($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->client_storage_url,$options,$headers);
    }

    public function getStats($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->stats_url,$options,$headers);
    }

}
