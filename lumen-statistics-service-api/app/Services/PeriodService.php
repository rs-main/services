<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class PeriodService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $periods_url;


    public function __construct()
    {
        $this->base_url          = config("services.periods.base_url");
        $this->secret            = config("services.periods.secret");
        $this->periods_url        = config("services.periods.endpoints.periods_url");
    }

    public function getPeriods($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->periods_url,$options, []);
    }

    public function savePeriod($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->periods_url,$options,$headers);
    }

    public function getPeriod($id){
        $fetch_url = $this->periods_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deletePeriod($id){
        $delete_url = $this->periods_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
