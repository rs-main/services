<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class DownloadsService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $bag_url;
    private  $favourite_url;
    private  $download_url;

    public function __construct()
    {
        $this->base_url              = config("services.downloads.base_url");
        $this->secret                = config("services.downloads.secret");
        $this->bag_url               = config("services.downloads.endpoints.bag_url");
        $this->favourite_url         = config("services.downloads.endpoints.favourite_url");
        $this->download_url          = config("services.downloads.endpoints.download_url");
    }

    public function getBag($options,$user_id){
        $bag_url = $this->bag_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$bag_url,$options, []);
    }

    public function sendToBag($options,$headers,$id){
        $bag_url = $this->bag_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$bag_url,$options,$headers);
    }

    public function getFavourites($options,$user_id){
        $bag_url = $this->favourite_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$bag_url,$options, []);
    }

    public function addToFavourites($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->favourite_url,$options,$headers);
    }

    public function getDownload($options,$id){
        $download_url = $this->download_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$download_url,$options, []);
    }

    public function deleteDownload($id){
        $delete_url = $this->download_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
