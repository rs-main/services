<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class CurriculumService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $curriculums_url;


    public function __construct()
    {
        $this->base_url          = config("services.curriculums.base_url");
        $this->secret            = config("services.curriculums.secret");
        $this->curriculums_url  = config("services.curriculums.endpoints.curriculums_url");
    }

    public function getCurriculums($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->curriculums_url,$options, []);
    }

    public function saveCurriculum($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->curriculums_url,$options,$headers);
    }

    public function getCurriculum($id){
        $fetch_url = $this->curriculums_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteCurriculum($id){
        $delete_url = $this->curriculums_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
