<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class IntegrationService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $integration_url;
    private  $settings_url;


    public function __construct()
    {
        $this->base_url               = config("services.integrations.base_url");
        $this->secret                 = config("services.integrations.secret");
        $this->integration_url        = config("services.integrations.endpoints.integrations_url");
        $this->settings_url           = config("services.integrations.endpoints.settings_url");
    }

    public function getIntegrations($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->integration_url,$options, []);
    }

    public function saveIntegration($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->integration_url,$options,$headers);
    }

    public function getIntegration($id){
        $fetch_url = $this->integration_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteIntegration($id){
        $delete_url = $this->integration_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

    public function getSettings($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->settings_url,$options, []);
    }

    public function storeSettings($options, $headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->settings_url,$options,$headers);
    }

}
