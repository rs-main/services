<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class SearchService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $search_url;

    public function __construct()
    {
        $this->base_url   = config("services.search.base_url");
        $this->secret     = config("services.search.secret");
        $this->search_url = config("services.search.endpoints.search_url");
    }

    public function search($query){
        $url = $this->search_url."?q=$query";
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$url,[],[]);
    }

}
