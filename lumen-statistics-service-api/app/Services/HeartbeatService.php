<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class HeartbeatService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $register_url;
    private  $deregister_url;
    private  $send_heartbeat_url;
    private  $fetch_service_url;
    private  $change_status_url;

    public function __construct()
    {
        $this->base_url                  = config("services.discovery.base_url");
        $this->secret                    = config("services.discovery.secret");
        $this->register_url              = config("services.discovery.endpoints.register_url");
        $this->deregister_url            = config("services.discovery.endpoints.deregister_url");
        $this->send_heartbeat_url        = config("services.discovery.endpoints.send_heartbeat_url");
        $this->fetch_service_url         = config("services.discovery.endpoints.fetch_service_url");
        $this->change_status_url         = config("services.discovery.endpoints.change_status_url");
    }


    public function register($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->register_url,$options,$headers);
    }

    public function deRegister($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->deregister_url,$options,$headers);
    }

    public function sendHeartbeat($id){
        $fetch_url = $this->send_heartbeat_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function fetchService($id){
        $fetch_url = $this->fetch_service_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function changeStatus($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->change_status_url,$options,$headers);
    }

}
