<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class InstitutionService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $institutions_url;


    public function __construct()
    {
        $this->base_url          = config("services.institutions.base_url");
        $this->secret            = config("services.institutions.secret");
        $this->institutions_url  = config("services.institutions.endpoints.institutions_url");
    }

    public function getInstitutions($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->institutions_url,$options, []);
    }

    public function saveInstitution($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->institutions_url,$options,$headers);
    }

    public function getInstitution($id){
        $fetch_url = $this->institutions_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }


    public function deleteInstitution($id){
        $delete_url = $this->institutions_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
