<?php

namespace App\Services;

interface ServiceInterface
{

    public function getAll();

    public function getById($id);

    public function update($id);

    public function store();

    public function delete($id);
}
