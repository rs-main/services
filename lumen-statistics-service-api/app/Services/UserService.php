<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class UserService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $users_url;
    private  $user_url;
    private  $user_counts_url;


    public function __construct()
    {
        $this->base_url          = config("services.users.base_url");
        $this->secret            = config("services.users.secret");
        $this->users_url         = config("services.users.endpoints.users_url");
        $this->user_counts_url   = config("services.users.endpoints.user_counts_url");
    }

    public function getUsers($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->users_url,$options, []);
    }

    public function saveUser($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->users_url,$options,$headers);
    }

    public function getUser($id){
        $fetch_url = $this->users_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function getUserCounts($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->user_counts_url,$options, []);
    }
    public function getStats($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->user_counts_url,$options, []);
    }

}
