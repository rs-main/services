<?php

namespace App\Http\Controllers;

use App\Services\FileUploadService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Queue;
use Illuminate\Validation\Validator;

class StatisticsController  extends Controller
{

    private $upload_service;
    private $user_service;

    public function __construct()
    {
        $this->user_service = new UserService();
        $this->upload_service = new FileUploadService();
    }

    public function index(Request $request)
    {
        $this->validate($request, [
            "client" => "required"
        ]);

        $options = $request->all();
         $user_counts = [];
         $user_counts = $this->user_service->getUserCounts($options)["response"];
         $file_stats = $this->upload_service->getStats($options,[])["response"];

         return response()->json(["user" => $user_counts, "files" => $file_stats]);
    }

}
