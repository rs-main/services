<?php

namespace App\Jobs;

use App\Models\Comment;
use App\Services\NotificationService;
use App\Services\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotificationServiceJob extends Job
{
    use  InteractsWithQueue, Queueable;

    private $options;
    private $notification_service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($options)
    {
        $this->options = $options;
        $this->notification_service       = new NotificationService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->saveNotification($this->options);
    }

    private function saveNotification($options){
            $header = [];
            ($this->notification_service->saveNotification($options,$header));
    }

}
