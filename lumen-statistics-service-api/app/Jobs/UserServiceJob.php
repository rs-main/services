<?php

namespace App\Jobs;

use App\Models\Comment;
use App\Services\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserServiceJob extends Job
{
    use  InteractsWithQueue, Queueable;

    private $comment_id;
    private $user_id;
    private $parent_id;
    private $user_service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($comment_id,$user_id,$parent_id)
    {
        $this->comment_id         = $comment_id;
        $this->user_id            = $user_id;
        $this->parent_id            = $parent_id;
        $this->user_service       = new UserService();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $comment_builder =   Comment::find($this->comment_id);

        if ($this->user_id != null){
            $comment_builder->update([
                "user" => $this->getUserObject($this->user_id),
                "parent" => $this->getUserObject($this->parent_id)
            ]);
        }

        if ($this->parent_id != null){
            $comment_builder->update([
                "parent" => $this->getUserObject($this->parent_id)
            ]);
        }
    }

    private function getUserObject($id){
        if ($id != null) {
            $response = ($this->user_service->getUser($id))["response"];
            return ($response["user"]) ?? null;
        }
    }

}
