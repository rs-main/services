<?php

namespace App\Console;

use App\Console\Commands\HeartBeatCommand;
use App\Console\Commands\RegisterServiceCommand;
use App\Console\Commands\StatsJob;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ServeCommand::class,
//        HeartBeatCommand::class,
//        RegisterServiceCommand::class,
//        StatsJob::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command(RegisterServiceCommand::class);
//        $schedule->command(StatsJob::class)->everyFifteenMinutes();
//        $schedule->command(HeartBeatCommand::class)->everyMinute();
    }
}
