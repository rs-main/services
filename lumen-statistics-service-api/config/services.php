<?php

use Illuminate\Support\Str;

return [

    'discovery'=> [
        "base_url"       => "http://gateway:80",
        "secret"         => env("GATEWAY_SERVICE_SECRET"),
        "endpoints"      =>  [
            "register"   => "/api/register",
            "login"      => "/api/login",
            "profile"         => "/api/profile"
        ]
    ],

    'users'=> [
        "base_url"       => "http://nginx:80",
        "secret"         => env("GATEWAY_SERVICE_SECRET"),
        "endpoints"      =>  [
            "register"   => "/api/register",
            "login"      => "/api/login",
            "profile"         => "/api/profile",
            "user_counts_url" => "/api/users/stats/all"
        ]
    ],

    'payments'=> [
        "base_url"           => "http://payments:8001",
        "secret"             => env("PAYMENT_SERVICE_SECRET"),
        "endpoints" => [
            "make_payment_url"   => "/api/payments/transactions",
            "verify_payment_url" => "/api/payments/verify-payment/",
            "submit_otp"         => "/api/payments/transactions/otp",
            "fund_wallet"         => "/api/payments/fund-wallet",
            "user_wallet"         => "/api/wallets/",
            "user_balance"         => "/api/wallets/balance/",
            "transactions"         => "/api/wallets/transactions/",
            ]
    ],

    'notifications' => [
        'base_url' => "http://notifications:8005",
        "secret"   => env("NOTIFICATION_SERVICE_SECRET"),
        "endpoints" => [
            "campaign_url" => "/api/notifications",
            "statistics_url" => "/api/notifications/statistics",
            "notify_url" => "/api/notify",
            "notify_otp_url" => "/api/notifications/otp/",
            "verify_otp_url" => "/api/notifications/verify-otp/",
            "create_notify_url" => "/api/notifications/user/",
            "sms_balance_url" => "/api/notifications/user/",
            "bulk_sms_url"    => "/api/notifications/bulk-sms"
        ]
    ],

    'uploads' => [
        'base_url' => "http://uploads:8003",
        'endpoints' => [
            'files_url'       => '/api/uploads',
            'upload_url'      => '/api/uploads',
            'upload_file_url' => '/api/uploads/file/new',
            'fetch_url'       => '/api/uploads/',
            'bookmark_url'    => '/api/uploads/file/bookmarks/',
            'bookmark_file_url' => '/api/uploads/file/bookmarks/',
            'delete_url'      => '/api/uploads/',
            'client_storage_url'      => '/api/storage/',
            'stats_url'      => '/api/files/stats/'
        ],
        "secret"   => env("UPLOADS_SERVICE_SECRET")
    ],

    'downloads' => [
        'base_url' => "http://uploads:8003",
        'endpoints' => [
            'bag_url'   => '/api/downloads/bag/',
            'favourite_url'  => '/api/downloads/favourites/',
            'download_url'  => '/api/downloads/',
        ],
        "secret"   => env("DOWNLOADS_SERVICE_SECRET")
    ],

    'search' => [
        'base_url' => "http://uploads:8003",
        'endpoints' => [
            'search_url' => '/api/search'
        ],
        "secret"   => env("SEARCH_SERVICE_SECRET")
    ],

    'wiki' => [
        'base_url' => "https://en.wikipedia.org",
        'endpoints' => [
            'search_url'   => '/w/api.php'
        ],
        "secret"   => env("SEARCH_SERVICE_SECRET")
    ],

    'institutions' => [
        'base_url' => "http://institutions:8004",
        'endpoints' => [
            'institutions_url'   => '/api/institutions/'
        ],
        "secret"   => env("INSTITUTIONS_SERVICE_SECRET")
    ],

    'institution_types' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'institution_type_url'        => '/api/institution-types/',
            'institution_hierarchy_url'   => '/api/institution-hierarchies/',
        ],
        "secret"   => env("INSTITUTIONS_HIERARCHY_SERVICE_SECRET")
    ],


    'curriculums' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'curriculums_url'   => '/api/curriculums/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'subjects' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'subjects_url'   => '/api/subjects/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'classes' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'classes_url'   => '/api/classes/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'courses' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'courses_url'   => '/api/courses/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'levels' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'levels_url'   => '/api/levels/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'topics' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'topics_url'   => '/api/topics/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'periods' => [
        'base_url' => "http://curriculums:8006",
        'endpoints' => [
            'periods_url'   => '/api/periods/'
        ],
        "secret"   => env("CURRICULUMS_SERVICE_SECRET")
    ],

    'integrations' => [
        'base_url' => "http://settings:8008",
        'endpoints' => [
            'integrations_url'   => '/api/integrations/',
            'settings_url'   => '/api/settings/'
        ],
        "secret"   => env("INTEGRATIONS_SERVICE_SECRET")
    ],


    'comments' => [
        'base_url' => "http://comments:8009",
        'endpoints' => [
            'comments_url'           => '/api/comments/',
            'user_comments_url'      => '/api/comments/user/',
            'comment_users_url'      => '/api/comments/users/',
            'subject_comments_url'   => '/api/comments/subject/',
            'like_comments_url'      => '/api/comments/likes/',
            'dislike_comments_url'   => '/api/comments/dislikes/',
            'report_comment_url'   => '/api/comments/reports/report/',
            'hide_comment_url'   => '/api/comments/reports/hide/',
            'participants_url'   => '/api/comments/participants/list/',
        ],
        "secret"   => env("COMMENTS_SERVICE_SECRET")
    ],


];
