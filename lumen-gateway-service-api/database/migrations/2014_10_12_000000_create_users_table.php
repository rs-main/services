<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('username')->nullable();
            $table->string('password');
            $table->string('email')->unique();
            $table->string('dob')->nullable();
            $table->unsignedBigInteger('course_id')->index()->nullable();
            $table->unsignedBigInteger('class_id')->nullable();
            $table->unsignedBigInteger('school_id')->nullable();
            $table->string('password_reset_key')->nullable()->unique();
            $table->string('twitter')->nullable()->unique();
            $table->string('facebook')->nullable()->unique();
            $table->string('linkedin')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('photo')->nullable();
            $table->enum("type",["student","teacher","author","publisher"]); // add admin as a type
            $table->string("business_registration_number")->nullable();
            $table->string("business_tin_number")->nullable();
            $table->enum("incorporation_type", ["limited-liability-company",
                                                        "non-profit","government-agency",
                                                        "sole-proprietorship","joint-venture"]
            )->nullable();
            $table->boolean("admin")->default(false);// remove this line
            $table->json("course_object")->nullable();
            $table->json("class_object")->nullable();
            $table->json("school_object")->nullable();
            $table->enum("id_type",["voters","passport","ghana-card","nhis","ssnit","student"])->nullable();
            $table->string("id_number")->nullable();
            $table->string("ghana_post_code")->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
