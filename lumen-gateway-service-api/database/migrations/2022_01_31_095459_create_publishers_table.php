<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublishersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publishers', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->unsignedBigInteger("country_id")->index()->nullable();
            $table->unsignedBigInteger("region_id")->index()->nullable();
            $table->unsignedBigInteger("location_id")->index()->nullable();
            $table->string("business_registration_number")->nullable();
            $table->string("business_tin_number")->nullable();
            $table->enum("incorporation_type", ["limited-liability-company",
                    "non-profit","government-agency",
                    "sole-proprietorship","joint-venture"]
            )->nullable();
            $table->string("ghana_post_code")->nullable();
            $table->string("contact_person_name")->nullable();
            $table->string("contact_person_number")->nullable();
            $table->string("email")->nullable();
            $table->string("website")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publishers');
    }
}
