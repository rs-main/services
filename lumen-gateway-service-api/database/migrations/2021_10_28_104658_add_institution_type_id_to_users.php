<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInstitutionTypeIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger("institution_type_id")->index()->nullable();
            $table->unsignedBigInteger("location_id")->index()->nullable();
            $table->unsignedBigInteger("region_id")->index()->nullable();
            $table->string("ownership",15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(["institution_type_id","location_id","region_id","ownership"]);
        });
    }
}
