<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Models\User;

$router->get('/', function () use ($router) {
    return ["name" => "Gateway Service", "port" => "80","version" => $router->app->version()];
});


$router->get('/wiki', function () use ($router) {
    return ["name" => "Gateway Service", "port" => "80","version" => $router->app->version()];
});

$router->get('/test', function () use ($router) {

    $circuitBreakerFactory = new \PrestaShop\CircuitBreaker\SimpleCircuitBreakerFactory();
    $circuitBreaker = $circuitBreakerFactory->create(new \PrestaShop\CircuitBreaker\FactorySettings(3, 6, 1));
//
    $fallbackResponse = function () {
        return '{}';
    };
//
    return $circuitBreaker->call('http://localhost/files/search', [], $fallbackResponse);
    return $circuitBreaker->isOpened();

    $circuitBreakerFactory = new \PrestaShop\CircuitBreaker\SimpleCircuitBreakerFactory();
    $settings = new \PrestaShop\CircuitBreaker\FactorySettings(100, 10, 10);
    $settings->setClientOptions(['method' => 'GET']);
    $circuitBreaker = $circuitBreakerFactory->create($settings);
    return $circuitBreaker->call('http://uploads:8003/files/search',[],$fallbackResponse);

    return $circuitBreaker->getState();

});

// API route group
$router->group(['prefix' => 'api'], function () use ($router) {

    //Statistic Routes
    $router->get("/statistics", "StatisticsServiceController@index");

    // Payments Service routes
    $router->get('payments',            'PaymentsServiceController@index');
    $router->post('payments',            'PaymentsServiceController@makePayment');
    $router->get('payments/{reference}', 'PaymentsServiceController@verifyPayment');
    $router->post('payments/transactions/otp',        'PaymentsServiceController@sendOtp');

    // Wallets Service routes
    $router->post("payments/fund-wallet", 'PaymentsServiceController@fundWallet');
    $router->get("wallets/{user_id}", 'PaymentsServiceController@userWallet');
    $router->get("wallets/balance/{user_id}", 'PaymentsServiceController@userBalance');
//    $router->get("wallets/balance/{user_id}", 'PaymentsServiceController@userBalance');
    $router->get("wallets/transactions/{user_id}", 'PaymentsServiceController@transactions');


    // Notifications Service routes
    $router->get('notifications', 'NotificationsServiceController@index');
    $router->get('notifications/statistics', 'NotificationsServiceController@getStatistics');
    $router->get('notifications/sms-balance', 'NotificationsServiceController@getSmsAccountBalance');
    $router->post('notifications/notify', 'NotificationsServiceController@notify');
    $router->post('notifications/bulk-sms','NotificationsServiceController@sendBulkSms');
    $router->post('notifications/otp', 'NotificationsServiceController@sendOtp');
    $router->post('notifications/verify-otp', 'NotificationsServiceController@verifyOtp');

    // Create User Notification
    $router->post('notifications/user','NotificationsServiceController@createNotification');
    // Get All User Notifications
    $router->get('notifications/user','NotificationsServiceController@getUserNotifications');

    $router->get('notifications/parameters','NotificationsServiceController@getMessagingParameters');



    // User service routes
    $router->post('register',  'AuthController@register');
    $router->post('login',     'AuthController@login');
    $router->get('profile',    'UserController@profile');
    $router->get('users/{id}', 'UserController@singleUser');
    $router->post('users/{id}', 'UserController@updateUser');

    $router->get('users',      'UserController@index');
    $router->get("users/stats/counts","UserController@getCounts");
    $router->get('users/stats/all',      'UserController@getStats');
    $router->get('users/stats/device-counts',      'UserController@getDeviceTypeCounts');
    $router->get('users/data/counts',      'UserController@getUserCounts');
    $router->post('users/sms/send','UserController@sendSmsToUsers');
    $router->get("users/data/select",'UserController@getUsers');

    $router->get("users/data/user-types",'UserController@getUserTypes');

    // Publisher Routes
    $router->get('publishers', 'PublisherController@getIndex');
    $router->post('publishers', 'PublisherController@postStorePublisher');

    // User Notes routes
    $router->get('notes',            'UserController@getNotes');
    $router->get('notes/{id}',            'UserController@getNote');
    $router->post('notes',            'UserController@saveNote');
    $router->post('notes/{id}',            'UserController@updateNote');

    // Uploads Service routes

    //
    $router->get('uploads',           'UploadsServiceController@index');


    $router->post('uploads',          'UploadsServiceController@create');

    $router->post('uploads/file/new', 'UploadsServiceController@upload');


    $router->get('uploads/{id}',      'UploadsServiceController@show');
    $router->post('uploads/{id}',     'UploadsServiceController@delete');

    // Author routes
    $router->get('authors',           'AuthorsServiceController@index');
    $router->post('authors',          'AuthorsServiceController@store');
    $router->get('authors/{id}',      'AuthorsServiceController@show');

    // storage routes
    $router->get('storage','UploadsServiceController@getStorage');
    $router->post('storage','UploadsServiceController@addClientStorage');

    // Bookmark routes
    $router->get('uploads/file/bookmarks/{user_id}/{file_id}', 'UploadsServiceController@getBookMark');
    $router->post('uploads/file/bookmarks', 'UploadsServiceController@bookMarkPage');
    $router->get('uploads/file/bookmarks','UploadsServiceController@getUserBookMarks');

    // Institution Service routes
    $router->get('institutions',       'InstitutionsServiceController@index');
    $router->post('institutions',      'InstitutionsServiceController@store');
    $router->get('institutions/{id}',  'InstitutionsServiceController@show');
    $router->post('institutions/{id}', 'InstitutionsServiceController@delete');

    $router->get('institutions/stats/all',       'InstitutionsServiceController@stats');


    // Search Service routes
    $router->get('files/search',"SearchServiceController@search");
    $router->get('search/index',"SearchServiceController@indexSearch");

    //Curriculum routes
    $router->get('curriculums',       'CurriculumsServiceController@index');
    $router->post('curriculums',      'CurriculumsServiceController@store');
    $router->get('curriculums/{id}',  'CurriculumsServiceController@show');
    $router->post('curriculums/{id}', 'CurriculumsServiceController@delete');
    $router->get('curriculums/subjects/{curriculum_id}', 'CurriculumsServiceController@subjects');
    $router->post('subject-curriculums', 'CurriculumsServiceController@addSubjectToCurriculum');

    // institution types
    $router->get('institution-types', 'InstitutionTypeServiceController@index');
    $router->post('institution-types', 'InstitutionTypeServiceController@store');
    $router->get('institution-types/{id}', 'InstitutionTypeServiceController@show');
    $router->post('institution-types/{id}', 'InstitutionTypeServiceController@delete');

    // Subject routes
    $router->get('subjects',       'SubjectsServiceController@index');
    $router->post('subjects',      'SubjectsServiceController@store');
    $router->get('subjects/{id}',  'SubjectsServiceController@show');
    $router->post('subjects/{id}', 'SubjectsServiceController@delete');

    // Topic routes
    $router->get('topics',       'TopicsServiceController@index');
    $router->post('topics',      'TopicsServiceController@store');
    $router->post('subtopics',   'TopicsServiceController@storeSubTopic');
    $router->get('topics/{id}',  'TopicsServiceController@show');
    $router->post('topics/{id}', 'TopicsServiceController@delete');

    // Levels routes
    $router->get('levels',       'LevelsServiceController@index');
    $router->post('levels',      'LevelsServiceController@store');
    $router->get('levels/{id}',  'LevelsServiceController@show');
    $router->post('levels/{id}', 'LevelsServiceController@delete');

    // Classes routes
    $router->get('classes',       'ClassesServiceController@index');
    $router->post('classes',      'ClassesServiceController@store');
    $router->get('classes/{id}',  'ClassesServiceController@show');
    $router->post('classes/{id}', 'ClassesServiceController@delete');

    // Courses routes
    $router->get('courses',       'CoursesServiceController@index');
    $router->post('courses',      'CoursesServiceController@store');
    $router->get('courses/{id}',  'CoursesServiceController@show');
    $router->post('courses/{id}', 'CoursesServiceController@delete');

    // Period routes
    $router->get('periods',       'PeriodsServiceController@index');
    $router->post('periods',      'PeriodsServiceController@store');
    $router->get('periods/{id}',  'PeriodsServiceController@show');
    $router->post('periods/{id}', 'PeriodsServiceController@delete');

    // Timetable routes
    $router->get('timetables',       'TimetableServiceController@index');
    $router->post('timetables',      'TimetableServiceController@store');
    $router->get('timetables/{id}',  'TimetableServiceController@show');
    $router->post('timetables/{id}', 'TimetableServiceController@delete');

    // API Integration routes
    $router->get('integrations',       'IntegrationServiceController@index');
    $router->post('integrations',      'IntegrationServiceController@store');
    $router->get('integrations/{id}',  'IntegrationServiceController@show');
    $router->post('integrations/{id}', 'IntegrationServiceController@delete');

    $router->get('integrations/stats/all', 'IntegrationServiceController@stats');

    // API Settings routes
    $router->get('settings',       'IntegrationServiceController@settings');
    $router->post('settings',      'IntegrationServiceController@storeSettings');

    // Downloads API routes
    $router->get("/downloads/bag/{user_id}","DownloadsServiceController@getBag");
    $router->post("/downloads/bag/{id}","DownloadsServiceController@sendToBag");

    //Favourites
    $router->get("/downloads/favourites/{user_id}","DownloadsServiceController@getFavourites");
    $router->post("/downloads/favourites","DownloadsServiceController@addToFavourites");

    // Downloads
    $router->get("/downloads/{id}","DownloadsServiceController@download");
    $router->post("/downloads/{id}","DownloadsServiceController@delete");

    // Comments
    $router->get("/comments", "CommentsServiceController@index");
    $router->get("/comments/{id}", "CommentsServiceController@show");
    $router->post("/comments", "CommentsServiceController@store");
    $router->post("/comments/{id}", "CommentsServiceController@delete");

    $router->get("/comments/user/{user_id}", "CommentsServiceController@userComments");
    $router->get("/comments/subject/{subject_id}", "CommentsServiceController@subjectComments");

    $router->post("/comments/likes/{user_id}/{comment_id}", "CommentsServiceController@likeComment");
    $router->post("/comments/dislikes/{user_id}/{comment_id}", "CommentsServiceController@dislikeComment");

//    $router->get("/comments/users","CommentsServiceController@getCommentUsers");

    // Report Comment
    $router->post("/comments/reports/report", "CommentsServiceController@reportComment");

    $router->post("/comments/reports/hide", "CommentsServiceController@hideComment");

    // Chat or Comment Participants
    $router->get("/comments/participants/list","CommentsServiceController@getParticipants");

    // Notify Participants
    $router->post("/comments/participants/notify","CommentsServiceController@notifyParticipants");

    // Service Discovery
    $router->post("service-discovery/register","ServiceDiscoveryController@register");
    $router->post("service-discovery/deregister","ServiceDiscoveryController@deRegister");
    $router->get("service-discovery/send-heartbeat","ServiceDiscoveryController@sendHeartBeat");

    // Leaderboards
    $router->get("/leaderboards/activity","LeaderBoardController@AllActvities");
    $router->get("/leaderboards/activity/single","LeaderBoardController@getActivity");
    $router->post("/leaderboards/activity","LeaderBoardController@AddActivity");

    // Leaderboard scores
    $router->post("/leaderboards/scores","LeaderBoardController@AddScore");
    $router->get("/leaderboards/scores","LeaderBoardController@index");

    // Achievements
    $router->get("/achievements", "LeaderBoardController@Achievements");
    $router->post("/achievements", "LeaderBoardController@AddAchievement");
    $router->post("/achievements/threshold", "LeaderBoardController@setAchievementThreshold");
    $router->get("/achievements/threshold", "LeaderBoardController@getAchievementThresholds");
    $router->get("/achievements/user", "LeaderBoardController@getUserAchievements");

    //MapBox Service
    $router->get("/map-box-service/search", "MapBoxServiceController@search");

    $router->get("/regions","UserController@getRegions");

    $router->get("/region-counts","UserController@getRegionCounts");

    $router->get("/region-coordinates","UserController@getRegionCoordinates");

    $router->get("/signup-counts","UserController@getRegistrationsByMonth");

});

$router->get("/generate-users",function(){

    $types = ["student","teacher","author","publisher","student"];
    $devices = ["desktop","phones","robot","tablet","unknown"];
    $random = rand(0,4);
    $regionRand = rand(1,16);

    for ($i =3220 ; $i < 3230; $i++){

        for ($x = 1; $x <= 16; $x++){
            $agent = new \Jenssegers\Agent\Agent();
            $user = new User();
            $user->first_name = "Test10".$i.$x;
            $user->last_name = "Test10".$i.$x;
            $user->email = app('hash')->make("salt".$i)."@gmail.com";
            $plainPassword = "topman";
            $user->password = app('hash')->make($plainPassword);
            $user->dob = "1988-04-04";
            $user->course_id = 1;
            $user->class_id  = 1;
            $user->school_id = $random;
            $user->photo = "";
            $user->type = $types[$random];
            $user->business_registration_number = "";
            $user->business_tin_number = "";
            $user->incorporation_type = "non-profit";
            $user->id_type = "passport";
            $user->id_number = "00000013".$i.$x;
            $user->institution_type_id = 4;
            $user->location_id = $random+1;
            $user->region_id = $x;
            $user->ownership = "";
            $user->phone_number = "09536".$i.$x;
            $user->device       = $devices[$random];
            $user->publisher_id = $random+1;
            if($user->save()){
                \App\Models\UserService::create([
                    "user_id" => $user->id,
                    "service_id" => 1
                ]);
            }
        }
        $agent = new \Jenssegers\Agent\Agent();
        $user = new User();
        $user->first_name = "Test10".$i;
        $user->last_name = "Test10".$i;
        $user->email = "test001".$i."@gmail.com";
        $plainPassword = "topman";
        $user->password = app('hash')->make($plainPassword);
        $user->dob = "1988-04-04";
        $user->course_id = 1;
        $user->class_id  = 1;
        $user->school_id = 1;
        $user->photo = "";
        $user->type = "author";
        $user->business_registration_number = "";
        $user->business_tin_number = "";
        $user->incorporation_type = "non-profit";
        $user->id_type = "passport";
        $user->id_number = "00000013".$i;
        $user->institution_type_id = 4;
        $user->location_id = $random+1;
        $user->region_id = $regionRand;
        $user->ownership = "";
        $user->phone_number = "024295366".$i;
        $user->device       = $devices[$random];
        $user->publisher_id = $random+1;
        if($user->save()){
            \App\Models\UserService::create([
                "user_id" => $user->id,
                "service_id" => 1
            ]);
        }
    }
});

$router->get('/register-all-services',function (\Illuminate\Http\Request $request){
    $services = Config("ports");

    $host_ip = env("APP_ENV") == "local" ? "localhost": request()->ip();
    $ip =  "127.0.0.1";
    $parts = [];

    foreach ($services as $key => $service){
         $port_position = strrpos($services[$key]["base_url"],":");
         $port = substr($services[$key]["base_url"],$port_position+1,4);
         $name = $key;
         $parts[$name] = $port;
     }

    foreach ($parts as $key => $part ){
        $client = new \App\Libraries\Eureka\EurekaClient([
             'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
             'hostName' => $key,
             'appName' => $key,
             'ip' => $ip,
             'port' => [$part, true],
             'homePageUrl'    => "http://$host_ip:".$part,
             'statusPageUrl'  => "http://$host_ip:"."$part/info",
             'healthCheckUrl' => "http://$host_ip:"."$part/health"
         ]);

         $client->register();
    }

    return $parts;

});

$router->get("/register-service",function (\Illuminate\Http\Request $request){

    if (!($request->has("port")) && !($request->has("service_name"))){
        return response()->json(["message" => "Missing port or service name"],400);
    }

    $host_ip = env("APP_ENV") == "local" ? "localhost": request()->ip();
    $ip = $request->has("ip_address") ? $request->query("ip_address") : "127.0.0.1";
    $port = $request->query("port");
    $name = $request->query("service_name");

    $client = new \App\Libraries\Eureka\EurekaClient([
        'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
        'hostName' => $name,
        'appName' => $name,
        'ip' => $ip,
        'port' => [$port, true],
        'homePageUrl'    => "http://$host_ip:".$port,
        'statusPageUrl'  => "http://$host_ip:"."$port/info",
        'healthCheckUrl' => "http://$host_ip:"."$port/health"
    ]);

    $client->register();

});

$router->get("/deregister-service",function (\Illuminate\Http\Request $request){

    if (!($request->has("port")) && !($request->has("service-name"))){
        return response()->json(["message" => "Missing port or service name"],400);
    }

    $host_ip = request()->ip();
    $ip = $request->has("ip_address") ? $request->query("ip_address") : "127.0.0.1";
    $port = $request->query("port");
    $name = $request->query("service_name");
    $client = new \App\Libraries\Eureka\EurekaClient([
        'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
        'hostName' => $name,
        'appName' => $name,
        'ip' => $ip,
        'port' => [$port, true],
        'homePageUrl'    => "http://$host_ip:".$port,
        'statusPageUrl'  => "http://$host_ip:"."$port/info",
        'healthCheckUrl' => "http://$host_ip:"."$port/health"
    ]);

    $client->deRegister();

});

$router->get("/change-service-status",function (\Illuminate\Http\Request $request){

    if (!($request->has("port")) && !($request->has("service-name"))){
        return response()->json(["message" => "Missing port or service name"],400);
    }

    $host_ip = request()->ip();
    $ip = $request->has("ip_address") ? $request->query("ip_address") : "127.0.0.1";
    $port = $request->query("port");
    $name = $request->query("service_name");
    $client = new \App\Libraries\Eureka\EurekaClient([
        'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
        'hostName' => $name,
        'appName' => $name,
        'ip' => $ip,
        'port' => [$port, true],
        'homePageUrl'    => "http://$host_ip:".$port,
        'statusPageUrl'  => "http://$host_ip:"."$port/info",
        'healthCheckUrl' => "http://$host_ip:"."$port/health"
    ]);

    $client->deRegister();

});

$router->get("/fetch-service",function (\Illuminate\Http\Request $request){
    $client = new \App\Libraries\Eureka\EurekaClient([
        'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
    ]);

    if (!$request->has("name")){
        return response()->json(["message" => "name query parameter is required!"],400);
    }

    $name = $request->query("name");

    try {
        $instance = $client->fetchInstance($name);

        if ($instance != null || isset($instance->leaseInfo)) {
            $last_renewal_timestamp = $instance->leaseInfo;

            $last_renewal_time = \Carbon\Carbon::createFromTimestampMs($last_renewal_timestamp->lastRenewalTimestamp)->format("Y-M-D H:i:s");
            $registration_time = \Carbon\Carbon::createFromTimestampMs($last_renewal_timestamp->registrationTimestamp)->format("Y-M-D H:i:s");
            $minutes_between_last_update = \Carbon\Carbon::createFromTimestampMs($last_renewal_timestamp->lastRenewalTimestamp)->diffInMinutes(\Carbon\Carbon::now());
            return response()->json([
                "registered" => $registration_time,
                "last_heartbeat" => $last_renewal_time,
                "minutes_between_last_renewal" => $minutes_between_last_update,
                "lease" => $instance->leaseInfo,
                "info" => $instance
            ]);
        }
    }catch(Exception $exception){
        return response()->json(["message " => "Instance doesn't exist!"],400);
    }

});

$router->get("/heartbeat",function (\Illuminate\Http\Request $request){
    if (!($request->has("service_name"))){
        return response()->json(["message" => "Missing service name"],400);
    }

    $name = $request->query("service_name");
    $config = new \App\Libraries\Eureka\EurekaConfig([
        'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
        'hostName' => $name,
        "appName" => $name,
        'port' => ["8003", true],

    ]);

    $client = new \App\Libraries\Eureka\EurekaClient($config);
    return $client->heartbeat();
});

$router->get("/upload",function(){

    return DB::table("regions")->get();

    $json_url = public_path("cities.json");
    $json = (json_decode(file_get_contents($json_url),true));

    foreach ($json as $region){
        \Illuminate\Support\Facades\DB::table("regions")->insert([
            "name"  => $region
        ]);
    }

});

$router->get("/diagonal", function(){
//    function diagonalDifference($arr) {
//        $size_of_array = count($arr);
//        $first_diagonal = 0;
//        $second_diagonal = 0;
//        $j = 0;
//
//        for($i = 0; $i < $size_of_array; $i++){
//            $first_diagonal+= $arr[$j++][$i];
//            $second_diagonal+=$arr[$size_of_array - $j][$i];
//        }
//
//       $absolute_figure =  $first_diagonal-$second_diagonal;
//        return abs($absolute_figure);
//
//    }
//
//    $arr = array(
//
//        array(11,2,4),
//        array(4,5,6),
//        array(10,8,-12),
//    );
//
//    return diagonalDifference($arr);

//    $array = new SplFixedArray(10);

//    $array = [1,2,3,4,5];
//    $summed_arrays = [];
//    foreach ($array as $key => $ar){
//        $new_array = $array;
//        unset($new_array[(array_search($ar,$new_array))]);
//        array_push($summed_arrays,array_sum($new_array));
//    }
//
//    return [min($summed_arrays),max($summed_arrays)];

        $candles = [3 ,2 ,1 ,3];
        rsort($candles);
        $values_counts = (($candles));
        return ($values_counts);
});










