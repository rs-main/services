<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;


class HealthCheck extends Command
{
    protected $host_ip;
    protected $ip;
    protected $port;
    protected $name;
    protected $signature = 'check:health';
    protected $config;

    protected $description = 'Check Services Health';


    public function __construct()
    {
        parent::__construct();

        $this->host_ip = "localhost";
        $this->ip = "127.0.0.1";
        $this->port = "8003";
        $this->name = "uploads";

        $this->config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' => $this->name,
            'appName' => $this->name,
            'ip' => $this->ip,
            'port' => [$this->port, true],
            'homePageUrl'    => "http://$this->host_ip:".$this->port,
            'statusPageUrl'  => "http://$this->host_ip:"."$this->port/info",
            'healthCheckUrl' => "http://$this->host_ip:"."$this->port/health"
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

    }

    public function getHeartBeat(){

        $client = new EurekaClient($this->config);
        return $client->heartbeat();
    }
}
