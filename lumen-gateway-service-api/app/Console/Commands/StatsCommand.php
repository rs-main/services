<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;


class StatsCommand extends Command
{

    protected $signature = 'send:stats';

    protected $description = 'Send Stats to DB';


    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

    }
}
