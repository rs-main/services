<?php

namespace App\Console\Commands;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use App\Models\LeaderBoardScore;
use Illuminate\Console\Command;

class AchievementsCommand extends Command
{

    protected $signature = 'send:heart-beat';


    protected $description = 'Send Heartbeat to Eureka';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->sendHeartBeat();
    }

    private function scanForAchievements(){
//        $leaderboardscores = LeaderBoardScore::

    }
}
