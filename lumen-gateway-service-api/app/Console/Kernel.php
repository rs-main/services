<?php

namespace App\Console;

use App\Console\Commands\HeartBeatCommand;
use App\Console\Commands\RegisterServiceCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Sentry\Laravel\PublishConfigCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ServeCommand::class,
        Commands\SubscribeToPaymentChannel::class,
        Commands\PublishPaymentChannel::class,
        HeartBeatCommand::class,
        RegisterServiceCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(RegisterServiceCommand::class);
        $schedule->command(HeartBeatCommand::class)->everyMinute();
    }
}
