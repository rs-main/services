<?php

namespace App\Http\Controllers;

use App\Services\PaymentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PaymentsServiceController  extends Controller
{
    private $payment_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->payment_service = new PaymentService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->payment_service->getTransactions($options);
        return $this->response($res["response"],$res["status"]);

    }

    public function makePayment(Request $request)
    {
        $options = ($request->all());
        $headers = [];
        $res = $this->payment_service->makePayment($options,$headers);
        return response()->json($res["response"],$res["status"]);
    }

    public function verifyPayment($reference): JsonResponse
    {
        $res = $this->payment_service->verifyPayment($reference);
        return response()->json($res["response"],$res["status"]);
    }

    public function sendOtp(Request $request): JsonResponse
    {
        $options = $request->all();
        $headers = [];
        $res = $this->payment_service->submitOtp($options,$headers);
        return response()->json($res["response"],$res["status"]);
    }

    public function webhook(Request $request){
        return $request->all();
    }

    public function fundWallet(Request $request)
    {
        $options = ($request->all());
        $headers = [];
        $res = $this->payment_service->fundWallet($options,$headers);
        return response()->json($res["response"],$res["status"]);
    }

    public function userWallet($user_id){
        $res = $this->payment_service->userWallet($user_id);
        return response()->json($res["response"],$res["status"]);
    }

    public function userBalance($user_id){
        $res = $this->payment_service->userBalance($user_id);
        return response()->json($res["response"],$res["status"]);
    }

    public function transactions($user_id){
        $res = $this->payment_service->userTransactions($user_id);
        return response()->json($res["response"],$res["status"]);
    }
}
