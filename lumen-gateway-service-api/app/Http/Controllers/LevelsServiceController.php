<?php

namespace App\Http\Controllers;

use App\Services\InstitutionService;
use App\Services\LevelService;
use App\Services\SubjectsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LevelsServiceController  extends Controller
{
    private $level_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->level_service = new LevelService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->level_service->getLevels($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->level_service->saveLevel($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->level_service->getLevel($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->level_service->deleteLevel($id);
        return $this->response($res["response"],$res["status"]);
    }

}
