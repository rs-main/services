<?php

namespace App\Http\Controllers;

use App\Services\NotificationService;
use Illuminate\Http\Request;

class NotificationsServiceController  extends Controller
{
    private $notification_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->notification_service = new NotificationService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->notification_service->campaigns($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function getMessagingParameters(Request $request){
        $options = $request->all();
        $res = $this->notification_service->messagingParameters($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function getStatistics(Request $request){
        $options = $request->all();
        $res = $this->notification_service->monthlyStatistics($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function getSmsAccountBalance(Request $request)
    {
        $options = $request->all();
        $res = $this->notification_service->smsBalance($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function notify(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->notify($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function sendBulkSms(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->bulkSms($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }


    public function sendOtp(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->sendOtp($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function verifyOtp(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->verifyOtp($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function createNotification(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->createNotification($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function getUserNotifications(Request $request){

        $options = $request->all();
        $res = $this->notification_service->userNotifications($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function addClientStorage(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->notification_service->createNotification($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }
}
