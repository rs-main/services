<?php

namespace App\Http\Controllers;

use App\Libraries\Eureka\EurekaClient;
use App\Libraries\Eureka\EurekaConfig;
use App\Libraries\Eureka\Exceptions\RegisterFailureException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ServiceDiscoveryController extends Controller
{
    private $serviceClient;
    private $config;
    private $ip;


    /**
     * @throws RegisterFailureException
     * @throws ValidationException
     */
    public function register(Request $request){

        $this->validate($request,[
            "port" => "required",
            "service_name" => "required"
        ]);

        $this->config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' =>  $request->get("service_name"),
            "appName" =>  $request->get("service_name"),
            "ip"      =>  $this->ip,
            'port' => [ $request->get("port"), true],
            'homePageUrl'    => "http://localhost:".$request->port,
            'statusPageUrl'  => "http://localhost:"."$request->port/info",
            'healthCheckUrl' => "http://localhost:"."$request->port/health"
        ]);

        $this->serviceClient = new EurekaClient($this->config);
        $this->serviceClient->register();
    }


    /**
     * @throws ValidationException
     * @throws \App\Libraries\Eureka\Exceptions\DeRegisterFailureException
     */
    public function deRegister(Request $request){

        $this->validate($request,[
            "port" => "required",
            "service_name" => "required"
        ]);

        $this->config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' =>  $request->get("service_name"),
            "appName" =>  $request->get("service_name"),
            "ip"      =>  $this->ip,
            'port' => [ $request->get("port"), true],
            'homePageUrl'    => "http://localhost:".$request->port,
            'statusPageUrl'  => "http://localhost:"."$request->port/info",
            'healthCheckUrl' => "http://localhost:"."$request->port/health"
        ]);

        $this->serviceClient = new EurekaClient($this->config);
        $this->serviceClient->deRegister();
    }

    public function start(){

    }

    public function sendHeartBeat(Request $request){

        $this->validate($request,[
            "port" => "required",
            "service_name" => "required"
        ]);

        $name = $request->query("service_name");
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' => $name,
            "appName" => $name,
            'port' => [$request->query('port'), true],

        ]);

        $client = new EurekaClient($config);
        return $client->heartbeat();

    }


    public function fetchService(Request $request){

        $this->validate($request,[
            "service_name" => "required"
        ]);

        $name = $request->query("service_name");
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka'
        ]);

        $client = new EurekaClient($config);
        return $client->fetchInstance($name);

    }


    public function changeStatus(Request $request){

        $this->validate($request,[
            "service_name" => "required",
            "status"       => "required"
        ]);

        $name = $request->query("service_name");
        $config = new EurekaConfig([
            'eurekaDefaultUrl' => 'http://eureka-server:8761/eureka',
            'hostName' => $name,
            "appName" => $name
        ]);

        $client = new EurekaClient($config);
        if ($request->status == "out-of-service"){
            $client->outOfService();
        }else{
            $client->backToService();
        }
    }

    /**
     * @param Request $request
     */
    public function getIp(Request $request)
    {
        return $this->ip = $request->has("ip_address") ? $request->query("ip_address") : "127.0.0.1";
    }

}
