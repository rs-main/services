<?php

namespace App\Http\Controllers;

use App\Services\InstitutionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class InstitutionsServiceController  extends Controller
{
    private $institution_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->institution_service = new InstitutionService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->institution_service->getInstitutions($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->institution_service->saveInstitution($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->institution_service->getInstitution($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->institution_service->deleteInstitution($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function stats(Request $request){

        $options = $request->all();
        $res = $this->institution_service->getStats($options);
        return $this->response($res["response"],$res["status"]);
    }

}
