<?php

namespace App\Http\Controllers;

use App\Services\PeriodService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PeriodsServiceController  extends Controller
{
    private $period_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->period_service = new PeriodService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->period_service->getPeriods($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->period_service->savePeriod($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->period_service->getPeriod($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->period_service->deletePeriod($id);
        return $this->response($res["response"],$res["status"]);
    }

}
