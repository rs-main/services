<?php

namespace App\Http\Controllers;

use App\Classes\Statistics;
use App\Jobs\SendSmsJob;
use App\Models\Note;
use App\Models\Region;
use App\Models\Service;
use App\Models\User;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the authenticated User.
     *
     *
     */
    public function profile()
    {
        return response()->json(['user' => Auth::user()], 200);
    }

    /**
     * Get all User.
     *
     */
    public function index(Request $request)
    {

        $user_builder = User::query();

        if ($request->has("service")){
            $service = Service::whereName(strtolower(trim($request->query('service'))))->first();

            if (!$service){
                return response()->json(["message" => "Service defined does not exist"],400);
            }

            $user_builder->whereHas("services",function ($query)use($service){
                $query->where("service_id",$service->id);
            });

        }

        $users = $user_builder->with(["services" => function($services){
            return $services->with("service:id,name");
        }])->orderByDesc("created_at")->get();

        return DataTables::of($users)->make(true);
    }

    public function getUsers(Request $request){
//        if ($request->has("service"))
            $service = Service::whereName(strtolower(trim($request->query('service'))))->first();

            if (!$service){
                return response()->json(["message" => "Service defined does not exist"],400);
            }

            $user_builder = User::query();

            $user_builder->whereHas("services",function ($query)use($service){
                $query->where("service_id",$service->id);
            });

            return $user_builder->where("type",$request->query("type"))
                ->select("id","first_name","last_name","phone_number","id_number")->get()->toArray();
    }

    public function getCounts(){

        $results = DB::select( DB::raw("SELECT COUNT(*) AS total,
                             sum( case when type = 'student' then 1 else 0 end) AS students,
                             sum( case when type = 'teacher' then 1 else 0 end) AS tutors,
                             sum( case when type = 'author' then 1 else 0 end) AS authors,
                             sum( case when type = 'publisher' then 1 else 0 end) AS publishers FROM users "));

        return response()->json(
             $results
        );
    }

    public function getUserTypes(){
        $results = User::selectRaw("COUNT(*) as count,type")
            ->groupBy("type")
            ->get();
        return DataTables::of($results)->make(true);
    }


    public function getDeviceTypeCounts(Request $request){
        $this->validate($request, ["client" => "required"]);
        $service = Service::whereName(strtolower(trim($request->query('client'))))->first();
        $userStats = new Statistics($service);

        return response()->json([
            "data" => $userStats->getDeviceCounts()->pluck("count"),
            "device" => $userStats->getDeviceCounts()->pluck("device"),
        ]);
    }


    public function getRegionCounts(Request $request){
        $this->validate($request, ["client" => "required"]);
        $service = Service::whereName(strtolower(trim($request->query('client'))))->first();
        $userStats = new Statistics($service);
        $coordinates = $userStats->getRegionCounts()->get()->map(function ($coordinate) {
                $coordinatesArray = explode(",",$coordinate->coordinates);
                $data = [
                    "lat" => floatval(count($coordinatesArray) > 0 ? $coordinatesArray[0] : null),
                    "lng" => floatval(count($coordinatesArray) > 1 ? $coordinatesArray[1] : null),
                    "icon" => "",
                    "name" => $coordinate->name
                ];
                return (["position" => $data,"status" => "delivered"]);
        });

        return response()->json([
            "data" => $userStats->getRegionCounts()->pluck("count"),
            "name" => $userStats->getRegionCounts()->pluck("name"),
            "coordinates" => $coordinates
        ]);
    }

    public function getRegionCoordinates(Request $request){
        $this->validate($request, ["client" => "required"]);
        $service = Service::whereName(strtolower(trim($request->query('client'))))->first();
        $userStats = new Statistics($service);

        $coordinates = $userStats->getRegionCounts()->get()->map(function ($coordinate) {
            $coordinatesArray = explode(",",$coordinate->coordinates);
            $data = [
                "lat" => floatval(count($coordinatesArray) > 0 ? $coordinatesArray[0] : null),
                "lng" => floatval(count($coordinatesArray) > 1 ? $coordinatesArray[1] : null),
                "icon" => "",
                "name" => $coordinate->name,
                "count" => $coordinate->count
            ];
            return (["position" => $data,"status" => "delivered"]);
        });

        return response()->json([
            "coordinates" => $coordinates
        ]);
    }

    public function  getRegistrationsByMonth(Request $request){
        $this->validate($request, ["client" => "required"]);
        $service = Service::whereName(strtolower(trim($request->query('client'))))->first();
        $userStats = new Statistics($service);

        $MONTHS = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        $registration_months = $userStats->getUserRegistrationMonthsBuilder()->pluck("month")->map(function ($item, $key) use($MONTHS) {
            return $MONTHS[$item-1];
        });

        return response()->json([
            "count" => $userStats->getUserRegistrationMonthsBuilder()->pluck("count"),
            "month" => $userStats->getUserRegistrationMonthsBuilder()->pluck("month")->toArray(),
            "object" => $userStats->getUserRegistrationMonthsBuilder()->get(),
            "mapped" => $registration_months
        ]);

    }

    public function updateUser(Request $request, $id){
        $user = User::find($id);

            $user->update([
            "institution_type_id" => $request->institution_type_id,
            "course_id" => $request->has("course_id")? $request->course_id : $user->course_id,
            "class_id" =>  $request->has("class_id") ? $request->class_id : $user->class_id,
            "school_id" => $request->has("school_id") ? $request->school_id : $user->school_id,
            "id_number" => $request->has("id_number") ? $request->id_number : $user->id_number,
            "business_tin_number" => $request->has("business_tin_number") ?
                $request->business_tin_number : $user->business_tin_number,
            "publisher_id" => $request->has("publisher_id") ?
                $request->publisher_id : $user->publisher_id
        ]);

        return response()->json(["data" => $user, "message" => "success"]);
    }

    /**
     * Get one user.
     *
     *
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);
            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'user not found!'], 404);
        }
    }

    public function getNotes( Request $request){
        $limit = $request->has("limit") ? $request->query("limit") : 5;
        $page  = $request->has("page") ? $request->query("page") : 1;
        $user_id = $request->query("user_id");

        $notes_builder = Note::whereUserId($user_id)->whereService($request->get("service"));
        $skipped = $page == 1 ? 0 : ($page * $limit)-$limit;
        $notes = $notes_builder->skip($skipped)->limit($limit)->orderByDesc("created_at")->get();
        $count = Note::whereUserId($user_id)->whereService($request->get("service"))->count();
        $last_page = ceil($count/$limit);
        return response()->json(["data" => $notes, "page" => (int)$page,
            "total" => (int)$count, "last_page" => (int)$last_page, "limit"=> (int)$limit]);
    }

    public function getNote($id){
        $note = Note::find($id);
        return response()->json(["data" => $note],200);
    }

    public function saveNote(Request $request){

        $this->validate($request,[
            "user_id" => "required",
            "text"    => "required",
            "service" => "required"
        ]);

        $note = Note::create([
            "user_id" => $request->user_id,
            "title"   => $request->get("title"),
            "text"    => $request->get("text"),
            "service" => $request->get("service")
        ]);

        return response()->json(["data" => $note],200);
    }

    public function updateNote($id,Request $request){

        $this->validate($request,[
            "user_id" => "required",
            "text"    => "required",
            "service" => "required"
        ]);

        $note = Note::find($id);

        $note->update([
            "user_id" => $request->user_id,
            "title"   => $request->get("title"),
            "text"    => $request->get("text"),
        ]);

        return response()->json(["data" => $note],200);
    }

    public function getUserCounts(Request $request){

        $user_type = $request->query("user_type");
        $institution_type = $request->query("institution_type");
        $region           = $request->query("region");
        $location         = $request->query("location");
        $ownership        = $request->query("ownership");
        $school           = $request->query("school");
        $class            = $request->query("class");

        $user_builder = User::query();

        if ($request->has("service")){
            $service = Service::whereName(strtolower(trim($request->query('service'))))->first();

            if (!$service){
                return response()->json(["message" => "Service defined does not exist"],400);
            }

            $user_builder->whereHas("services",function ($query)use($service){
                $query->where("service_id",$service->id);
            });
        }

        if ($request->has("user_type") && !empty($user_type)) {
            $user_builder->where("type", $user_type);
        }

        if ($request->has("user_types") && !empty($request->query("user_types"))){
            $user_builder
                ->whereIn("type", explode(",", $request->query("user_types")));
        }

        if ($request->has("location")&& !empty($location)) {
            $user_builder->where("location_id", $location);
        }

        if ($request->has("school") && !empty($school)
            && $request->query("school") != null){
            $user_builder->where("school_id",$school);
        }

        if ($request->has("institution_type") && !empty($institution_type)){
            $user_builder->where("institution_type_id", $institution_type);
        }

        if ($request->has("institution_types") && !empty($request->query("institution_types"))){
            $user_builder
                ->whereIn("institution_type_id", explode(",", $request->query("institution_types")));
        }

        if ($request->has("region") && !empty($region)){
            $user_builder->where("region_id", $region);
        }

        if ($request->has("class") && !$request->query("class")){
            $user_builder->where("class_id", $class);
        }

        if ($request->has("regions") && !empty($request->query("regions"))){
            $user_builder->whereIn("region_id", explode(",", $request->query("regions")));
        }

        if ($request->has("data")){
            return $user_builder->selectRaw("id,first_name,last_name,phone_number,email,device_token")->get();
        }

        if ($request->has("phone_number")){
            return $user_builder->whereNotNull("phone_number")->pluck("phone_number");
        }

        return $user_builder->count();

    }

    public function sendSmsToUsers(Request $request){

        $this->validate($request,[
//            "user_id" => "required",
            "service" => "required",
            "sender_id" => "required",
            "message"       => "required",
            "campaign_name" => "required",
            "type"          => "required"
        ]);

        $user_type        = $request->get("user_type");
        $institution_type = $request->get("institution_type");
        $region           = $request->get("region");
        $location         = $request->get("location");
        $ownership        = $request->get("ownership");
        $school           = $request->get("school");
        $class            = $request->get("class");

        $user_builder = User::query();

        if ($request->has("service")){
            $service = Service::whereName(strtolower(trim($request->get('service'))))->first();

            if (!$service){
                return response()->json(["message" => "Service defined does not exist"],400);
            }

            $user_builder->whereHas("services",function ($query)use($service){
                $query->where("service_id",$service->id);
            });
        }

        if ($this->hasPostField("user_type", $request)) { $user_builder->where("type", $user_type);}

        if ($this->hasPostField("user_types", $request)){
            $user_builder
                ->whereIn("type", explode(",", $request->get("user_types")));
        }

        if ($this->hasPostField("location", $request)){$user_builder->where("location_id", $location);}

        if ($this->hasPostField("school", $request)){$user_builder->where("school_id",$school);}

        if ($this->hasPostField("institution_type", $request)){
            $user_builder->where("institution_type_id", $institution_type);
        }

        if ($this->hasPostField("institution_types", $request)){
            $user_builder->whereIn("institution_type_id", explode(",", $request->get("institution_types")));
        }

        if ($this->hasPostField("region", $request)){$user_builder->where("region_id", $region);}

        if ($this->hasPostField("regions", $request)){
            $user_builder->whereIn("region_id", explode(",", $request->get("regions")));
        }

        if ($this->hasPostField("class", $request)){$user_builder->where("class_id", $class);}

        $phone_numbers = $this->getPhoneNumbers($user_builder);
        $emails        = $this->getFieldValues($user_builder,"email");
        $options       = $request->all();

        if (count($phone_numbers) != 0 ) {
            $options["phone_numbers"]       = implode(",", $phone_numbers);
            $options["emails"]              = implode(",", $emails);
            $options["user_ids"]            = implode(",", $this->getUserIds($user_builder));
            $options["sender_id"]           = $request->get("sender_id");
            $options["type"]                = $request->get("type");
            $options["message"]             = $request->get("message");
            $options["campaign_name"]       = $request->get("campaign_name");
            $job = new SendSmsJob($options);
            $this->dispatch($job);
        }

        $format = 'Message will be sent to %d users';
        return response()->json(["message" => sprintf($format, count($phone_numbers))]);
    }

    public function getStats(Request $request){

        $this->validate($request, ["client" => "required"]);
        $service = Service::whereName(strtolower(trim($request->query('client'))))->first();
        $userStats = new Statistics($service);

        $total_counts = $userStats->getUserCounts();
        $primary_students = $userStats->getUserCountBySchoolType("basic");
        $jhs_students = $userStats->getUserCountBySchoolType("jhs");
        $shs_students = $userStats->getUserCountBySchoolType("senior-high");
        $tertiary_students = $userStats->getUserCountBySchoolType("tertiary");
        $professional_students = $userStats->getUserCountBySchoolType("professional");
        $user_registration_stats  = $userStats->getUserRegistrationMonthsBuilder()->get();
        $device_types = $userStats->getDeviceCounts();
        $regions = $userStats->getRegionCounts();
//        $user_age_groups =

        return response()->json([
            "total_counts" => $total_counts, "primary_students" => $primary_students,
            "jhs_students" => $jhs_students, "shs_students" => $shs_students,
            "tertiary_students" => $tertiary_students, "professional_students" => $professional_students,
            "yearly_stats" => $user_registration_stats,"device_types" => $device_types,
            "region_counts" => $regions
            ]);
    }

    /**
     * @param Builder $user_builder
     * @return array
     */
    public function getPhoneNumbers(Builder $user_builder): array
    {
        $phone_numbers = $user_builder
            ->whereNotNull("phone_number")
            ->pluck("phone_number")->toArray();
        return $phone_numbers;
    }

    public function getFieldValues(Builder $user_builder, $field): array
    {
        $phone_numbers = $user_builder
            ->whereNotNull($field)
            ->pluck($field)->toArray();
        return $phone_numbers;
    }

    /**
     * @param Builder $user_builder
     * @return array
     */
    public function getUserIds(Builder $user_builder): array
    {
        return $user_builder->pluck("id")->toArray();
    }

    public function getRegions(){
        $regions = Region::all();
        return response()->json(["data" => $regions]);
    }

    private  function hasPostField($field, Request $request) :bool{
        return ($request->has($field) && !empty($request->get($field)));
    }

}

