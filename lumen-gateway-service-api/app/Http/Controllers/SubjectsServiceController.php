<?php

namespace App\Http\Controllers;

use App\Services\SubjectsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SubjectsServiceController  extends Controller
{
    private $subject_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->subject_service = new SubjectsService();
    }

    public function index(Request $request): JsonResponse
    {
        $options = $request->all();
        $res = $this->subject_service->getSubjects($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request): JsonResponse
    {
        $options = $request->all();
        $headers = [];
        $res = $this->subject_service->saveSubject($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->subject_service->getSubject($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id): JsonResponse
    {
        $res = $this->subject_service->deleteSubject($id);
        return $this->response($res["response"],$res["status"]);
    }

}
