<?php

namespace App\Http\Controllers;

use App\Services\AuthorsService;
use App\Services\SubjectsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthorsServiceController  extends Controller
{
    private $author_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->author_service = new AuthorsService();
    }

    public function index(Request $request): JsonResponse
    {
        $options = $request->all();
        $res = $this->author_service->getAuthors($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request): JsonResponse
    {
        $options = $request->all();
        $headers = [];
        $res = $this->author_service->saveAuthor($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->author_service->getAuthor($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id): JsonResponse
    {
        $res = $this->author_service->deleteAuthor($id);
        return $this->response($res["response"],$res["status"]);
    }

}
