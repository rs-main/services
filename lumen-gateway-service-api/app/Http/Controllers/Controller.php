<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function respondWithToken($token,$user): JsonResponse
    {
        return response()->json([
            'message' => 'success',
            'token' => $token,
            'user'  => $user,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 240
        ], 200);
    }

    protected function response($json, $status): JsonResponse
    {
        return response()->json(["data" => $json],$status);
    }
}
