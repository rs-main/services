<?php

namespace App\Http\Controllers;

use App\Services\TopicService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TopicsServiceController  extends Controller
{
    private $topic_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->topic_service = new TopicService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->topic_service->getTopics($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->topic_service->saveTopic($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

     public function storeSubTopic(Request $request)
        {
            $options = $request->all();
            $headers = [];
            $res = $this->topic_service->saveSubTopic($options,$headers);
            return $this->response($res["response"],$res["status"]);
        }

    public function show($id): JsonResponse
    {
        $res = $this->topic_service->getTopic($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->topic_service->deleteToic($id);
        return $this->response($res["response"],$res["status"]);
    }

}
