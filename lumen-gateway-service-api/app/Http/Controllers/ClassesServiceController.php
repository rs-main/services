<?php

namespace App\Http\Controllers;

use App\Services\ClassService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClassesServiceController  extends Controller
{
    private $class_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->class_service = new ClassService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->class_service->getClasses($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->class_service->saveClass($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->class_service->getClass($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->class_service->deleteClass($id);
        return $this->response($res["response"],$res["status"]);
    }

}
