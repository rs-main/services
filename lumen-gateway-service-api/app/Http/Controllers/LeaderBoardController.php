<?php

namespace App\Http\Controllers;

use App\Jobs\AchievementsJob;
use App\Models\Achievement;
use App\Models\AchievementThreshold;
use App\Models\LeaderBoard;
use App\Models\LeaderBoardScore;
use App\Models\Note;
use App\Models\User;
use App\Models\UserAchievement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class LeaderBoardController extends Controller
{

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Get all User.
     *
     */
    public function index(Request $request)
    {
        $limit = $this->getLimit($request);
        $page = $this->getPage($request);
        $leaderboard_id = $request->query("leaderboard_id");
        $leader_builder = LeaderBoardScore::with(["user:id,first_name,last_name,email,username,school_id,class_id,photo","leaderboard"]);

        if ($request->has("leaderboard_id")){
            $leader_builder->where("leaderboard_id",$leaderboard_id);
        }

        $skipped = $this->getSkipped($page, $limit);
        $leader_boards = $leader_builder->skip($skipped)->limit($limit)->orderByDesc("created_at")->get();

        $count = LeaderBoardScore::count();

        $last_page = ceil($count/$limit);
        return response()->json(["data" => $leader_boards, "page" => (int)$page,
            "total" => (int)$count, "last_page" => (int)$last_page, "limit"=> (int)$limit]);
    }

    public function AddScore(Request $request){
        $this->validate($request,[
            "user_id" => "required",
            "leaderboard_id" => "required",
            "scores"   => 'required|numeric'
        ]);

        $leaderboardScore = LeaderBoardScore::whereUserId($request->user_id)
            ->where("leaderboard_id", $request->get("leaderboard_id"))->first();
        if (!$leaderboardScore){
            $leaderboardScore = LeaderBoardScore::create($request->only(["user_id","leaderboard_id","scores"]));
        }else{
            $previous_scores = $leaderboardScore->scores;
            $leaderboardScore->update([
                "user_id" => $request->get("user_id"),
                "leaderboard_id" => $request->get("leaderboard_id"),
                "scores" => $previous_scores+$request->get("scores")
            ]);
        }

        $job = new AchievementsJob($leaderboardScore->id);
        \Queue::push($job);

        return response()->json(["data" => $leaderboardScore],200);
    }


    public function AddActivity(Request $request){

        $this->validate($request,[
            "service" => "required",
            "activity" => "required",
        ]);

        $leaderboard = LeaderBoard::whereService($request->service)->whereActivity($request->get("activity"))->first();

        if (!$leaderboard){
            $leaderboard = LeaderBoard::create($request->only(["service","activity"]));
        }
        return response()->json(["data" => $leaderboard],200);
    }

    public function AllActvities(Request $request){

        $leaderboards_builder = LeaderBoard::query();
        if ($request->has("service")){
            $leaderboards_builder->whereService($request->query("service"));
        }
        $leader_boards = $leaderboards_builder->get();
        return response()->json(["data" => $leader_boards ],200);
    }

    public function getActivity(Request $request){
        $leaderboard = LeaderBoard::whereService($request->query("service"))
            ->where("activity",$request->query("activity"))->first();

        return response()->json(["data" => $leaderboard ],200);
    }

    public function Achievements(){
        $achievements = Achievement::all();
        return response()->json(["data" => $achievements],200);
    }

    public function AddAchievement(Request $request){
        $this->validate($request,[
            "name" => "required"
        ]);

        $achievement = Achievement::create($request->only(["name"]));
        return response()->json(["data" => $achievement],200);
    }

    public function getAchievementThresholds(Request $request){
        return AchievementThreshold::with(["leaderboard:id,activity,service","achievement:id,name"])->get();
    }

    public function setAchievementThreshold(Request $request){

        $validated = $this->validate($request,[
            "score" => "required",
            "leaderboard_id" => "required",
            "achievement_id" => "required"
        ]);

        $achievement_threshold = AchievementThreshold::where("leaderboard_id",$validated["leaderboard_id"])
            ->where("achievement_id", $validated["achievement_id"])->first();

        if (!$achievement_threshold){
            $achievement_threshold = AchievementThreshold::create($request->only(["score","leaderboard_id","achievement_id"]));
        }else{
            $achievement_threshold->update([
                "score" => $validated["score"]
            ]);
        }

        return response()->json(["data" => $achievement_threshold],200);
    }

    public function getUserAchievements(){
        return UserAchievement::all();
    }

    /**
     * @param $page
     * @param $limit
     * @return float|int
     */
    public function getSkipped($page, $limit)
    {
        $skipped = $page == 1 ? 0 : ($page * $limit) - $limit;
        return $skipped;
    }

    /**
     * @param Request $request
     * @return array|int|string|null
     */
    public function getLimit(Request $request)
    {
        $limit = $request->has("limit") ? $request->query("limit") : 10;
        return $limit;
    }

    /**
     * @param Request $request
     * @return array|int|string|null
     */
    public function getPage(Request $request)
    {
        $page = $request->has("page") ? $request->query("page") : 1;
        return $page;
    }


}

