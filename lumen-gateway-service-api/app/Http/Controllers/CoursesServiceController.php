<?php

namespace App\Http\Controllers;

use App\Services\CourseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CoursesServiceController  extends Controller
{
    private $course_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->course_service = new CourseService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->course_service->getCourses($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->course_service->saveCourse($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->course_service->getCourse($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->course_service->deleteCourse($id);
        return $this->response($res["response"],$res["status"]);
    }

}
