<?php

namespace App\Http\Controllers;

use App\Services\CommentService;
use App\Services\TopicService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentsServiceController  extends Controller
{
    private $comment_service;

    public function __construct()
    {
        $this->middleware('auth');
        $this->comment_service = new CommentService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->comment_service->getComments($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->comment_service->saveComment($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }


    public function show($id): JsonResponse
    {
        $res = $this->comment_service->getComment($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->comment_service->deleteComment($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function userComments($user_id){
        $res = $this->comment_service->getComment($user_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function getCommentUsers(Request $request){
        $options = $request->all();
        $res = $this->comment_service->getComments($options);
        return $this->response($res["response"],$res["status"]);
    }



    public function subjectComments($subject_id){
        $res = $this->comment_service->getComment($subject_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function likeComment($user_id, $comment_id){
        $res = $this->comment_service->likeComment($user_id, $comment_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function dislikeComment($user_id, $comment_id){
        $res = $this->comment_service->dislikeComment($user_id, $comment_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function reportComment(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->comment_service->reportComment($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function hideComment(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->comment_service->hideComment($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function getParticipants(Request $request){
        $options = $request->all();
        $res = $this->comment_service->getParticipants($options);
        return $this->response($res["response"],$res["status"]);
    }

}
