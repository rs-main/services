<?php

namespace App\Http\Controllers;

use App\Services\NotificationService;
use App\Services\SearchService;
use App\Services\WikipediaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchServiceController  extends Controller
{
    private $search_service;
    private $wiki_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->search_service = new SearchService();
        $this->wiki_service   = new WikipediaService();
    }

    public function search(Request $request)
    {
        $options = urlencode($request->query("q"));
        if ($request->has("wiki")){
            return $this->searchWiki($options);
        }else{
            return $this->searchGkb($options);
        }
    }

    private function searchGkb($query): JsonResponse
    {
        $res = $this->search_service->search($query);
        return $this->response($res["response"],$res["status"]);
    }

    private function searchWiki($query): JsonResponse
    {
        $params = [
            "action" => "query",
            "list" => "search",
            "srsearch" => $query,
            "format" => "json"
        ];

//         $options = http_build_query( $params );
         $res = $this->wiki_service->search($params);
         return $this->response($res["response"],$res["status"]);
    }
}
