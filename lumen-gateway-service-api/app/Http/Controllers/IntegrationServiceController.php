<?php

namespace App\Http\Controllers;

use App\Services\InstitutionService;
use App\Services\IntegrationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IntegrationServiceController  extends Controller
{
    private $integration_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->integration_service = new IntegrationService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->integration_service->getIntegrations($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->integration_service->saveIntegration($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->integration_service->getIntegration($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->integration_service->deleteIntegration($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function settings(Request $request){
        $options = $request->all();
        $res = $this->integration_service->getSettings($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function storeSettings(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->integration_service->storeSettings($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function stats(Request $request){

        $options = $request->all();
        $res = $this->integration_service->getStats($options);
        return $this->response($res["response"],$res["status"]);
    }

}
