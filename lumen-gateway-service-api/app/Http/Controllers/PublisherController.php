<?php

namespace App\Http\Controllers;

use App\Jobs\OTPServiceJob;
use App\Jobs\UserServiceJob;
use App\Models\Contact;
use App\Models\Service;
use App\Models\User;
use App\Models\UserService;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;


class PublisherController extends Controller
{
    private $notification_service;
    public function __construct()
    {
        $this->notification_service = new NotificationService();
    }


    public function getIndex(){
        $publishers = DB::table("publishers")->get();
        return DataTables::of($publishers)->make(true);
    }

    public function postStorePublisher(Request $request){

        $this->validate($request,
            [
                "name" => "required",
            ]);

        DB::table("publishers")->insert([
            "name" => $request->name,
            "email" => $request->email,
            "business_registration_number" => $request->business_registration_number,
            "business_tin_number" => $request->business_tin_number,
            "incorporation_type" => $request->incorporation_type,
            "ghana_post_code" => $request->ghana_post_code,
            "contact_person_name" => $request->contact_person_name,
            "contact_person_number" => $request->contact_person_number,
            "website" => $request->website,
            "country_id" => $request->country_id,
            "region_id" => $request->region_id,
            "location_id" => $request->location_id,
        ]);

        return response()->json(["message" => "success"]);
    }

}
