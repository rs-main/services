<?php

namespace App\Http\Controllers;

use App\Services\CourseService;
use App\Services\InstitutionTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class InstitutionTypeServiceController  extends Controller
{
    private $institution_type_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->institution_type_service = new InstitutionTypeService();
    }

    public function index(Request $request){
        $options = $request->all();
        $res = $this->institution_type_service->getInstitutionTypes($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->institution_type_service->saveInstitutionType($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->institution_type_service->getInstitutionType($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->institution_type_service->deleteInstitutionType($id);
        return $this->response($res["response"],$res["status"]);
    }

}
