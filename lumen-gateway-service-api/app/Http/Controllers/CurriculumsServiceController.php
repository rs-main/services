<?php

namespace App\Http\Controllers;

use App\Services\CurriculumService;
use App\Services\InstitutionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CurriculumsServiceController  extends Controller
{
    private $curriculum_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->curriculum_service = new CurriculumService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->curriculum_service->getCurriculums($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->curriculum_service->saveCurriculum($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->curriculum_service->getCurriculum($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function subjects($curriculum_id): JsonResponse
    {
        $res = $this->curriculum_service->getCurriculumSubjects($curriculum_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function addSubjectToCurriculum(Request $request): JsonResponse
    {
        $options = $request->all();
        $headers = [];
        $res = $this->curriculum_service->addSubjectToCurriculum($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->curriculum_service->deleteCurriculum($id);
        return $this->response($res["response"],$res["status"]);
    }

}
