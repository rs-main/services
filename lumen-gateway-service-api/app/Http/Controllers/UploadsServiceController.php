<?php

namespace App\Http\Controllers;

use App\Services\FileUploadService;
use App\Services\UploadService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadsServiceController  extends Controller
{
    private $fileUploadService;
    private $uploadService;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->fileUploadService = new FileUploadService();
        $this->uploadService     = new UploadService();
    }

    public function index(Request $request){
        $options = $request->toArray();
        $res = $this->uploadService->allFiles($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function create(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->fileUploadService->upload($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }


    public function upload(Request $request)
    {
        $this->validate($request,
            [
                "media_file" => "required|file"
            ]
        );

        $options = $request->all();
        $file= $request->file("media_file");
        $res = $this->uploadService->uploadFile($options,file_get_contents($file), $file->getClientOriginalName());
        return $this->response($res["response"],$res["status"]);
    }

    public function show(Request $request, $id)
    {
        $options = $request->all();
        $res = $this->uploadService->fetchFile($options,$id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->fileUploadService->deleteFile($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function getBookMark($user_id, $file_id){
        $res = $this->fileUploadService->getBookMark($user_id,$file_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function bookMarkPage(Request $request){
        $options = $request->all();
        $res = $this->fileUploadService->bookMark($options,[]);
        return $this->response($res["response"],$res["status"]);
    }

    public function getUserBookMarks(Request $request){
        $options = $request->all();
        $res = $this->fileUploadService->getBookMarks($options,[]);
        return $this->response($res["response"],$res["status"]);
    }

    public function addClientStorage(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->fileUploadService->addClientStorage($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function getStorage(Request $request)
    {
        $options = $request->all();
        $options["user"] = Auth::check() ? Auth::user() : null;
        $headers = [];
        $res = $this->fileUploadService->getStorage($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

}
