<?php

namespace App\Http\Controllers;

use App\Services\AuthorsService;
use App\Services\StatisticsService;
use App\Services\SubjectsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StatisticsServiceController  extends Controller
{
    private $statistics_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->statistics_service = new StatisticsService();
    }

    public function index(Request $request): JsonResponse
    {
        $options = $request->all();
        $res = $this->statistics_service->getStatistics($options);
        return $this->response($res["response"],$res["status"]);
    }

}
