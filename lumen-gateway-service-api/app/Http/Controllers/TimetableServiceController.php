<?php

namespace App\Http\Controllers;

use App\Services\PeriodService;
use App\Services\TimetableService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TimetableServiceController  extends Controller
{
    private $timetable_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->timetable_service = new TimetableService();
    }

    public function index(Request $request){

        $options = $request->all();
        $res = $this->timetable_service->getAll($options);
        return $this->response($res["response"],$res["status"]);
    }

    public function store(Request $request)
    {
        $options = $request->all();
        $headers = [];
        $res = $this->timetable_service->save($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function show($id): JsonResponse
    {
        $res = $this->timetable_service->get($id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id)
    {
        $res = $this->timetable_service->delete($id);
        return $this->response($res["response"],$res["status"]);
    }

}
