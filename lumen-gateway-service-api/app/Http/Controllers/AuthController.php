<?php

namespace App\Http\Controllers;

use App\Jobs\OTPServiceJob;
use App\Jobs\UserServiceJob;
use App\Models\Contact;
use App\Models\Service;
use App\Models\User;
use App\Models\UserService;
use App\Services\NotificationService;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    private $notification_service;
    public function __construct()
    {
        $this->notification_service = new NotificationService();
    }

    /**
     * Store a new user.
     *
     */
    public function register(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users',
            'phone_number' => 'required|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required',
            'type'     => 'required',
//            'region_id' => 'required',
            "dob"      => 'required|date',
            "service"  => "required"
        ]);

        $service = Service::whereName(trim($request->service))->first();

        if (!$service){
            return response()->json(["message" => "No service by that name"],400);
        }

        if ($request->has("type") && !in_array($request->get("type"),["student","teacher","author","publisher"])){
            return response()->json(["message" => "Resource type not valid! Choose from student,teacher,author,publisher"],400);
        }

        try {

            $agent = new \Jenssegers\Agent\Agent();
            $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->dob = $request->dob;
            $user->course_id = !empty($request->course_id) ? $request->course_id : null;
            $user->class_id  = !empty($request->class_id) ? $request->class_id : null;
            $user->school_id = !empty($request->school_id) ? $request->school_id : null;
            $user->photo = $request->photo;
            $user->type = $request->get('type');
            $user->business_registration_number = $request->business_registration_number;
            $user->business_tin_number = $request->business_tin_number;
            $user->incorporation_type = $request->incorporation_type;
            $user->id_type = $request->id_type;
            $user->id_number = $request->id_number;
            $user->institution_type_id = $request->institution_type_id;
            $user->location_id = $request->location_id;
            $user->region_id = $request->region_id;
            $user->ownership = $request->ownership;
            $user->phone_number = $request->phone_number;
            $user->device       = $agent->deviceType();
            $user->publisher_id = $request->publisher_id;

            if ($user->save()){
                UserService::create([
                    "user_id" => $user->id,
                    "service_id" => $service->id
                ]);

                if($request->has("contacts") && $request->has("contact_labels")) {
                    $contacts = explode(",", $request->get("contacts"));
                    $contact_labels = explode(",", $request->get("contact_labels"));
                    $contact_types = explode(",", $request->get("contact_types"));
                    $contact_values = explode(",", $request->get("contact_values"));

                    foreach ($contacts as $key => $contact) {
                        $model = new Contact();
                        $model->user_id = $user->id;
                        $model->type = $contact_types[$key];
                        $model->label = $contact_labels[$key];
                        $model->value = $contact_values[$key];
                        $model->save();
                    }
                }
            }

            $job = new UserServiceJob($user);
            $this->dispatch($job);

            $smsJob = new OTPServiceJob($user);
            $this->dispatch($smsJob);

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!', "exception" => $e->getMessage()], 409);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
            'service'  => 'required'
        ]);

        try {

            $service = Service::whereName($request->service)->first();

            if (!$service) {
                return response()->json(['message' => 'Service does not exist']);
            }

            $user = User::whereEmail($request->email)->first();

            if ($request->has("otp_code")) {
                $this->verifyUser($request, $user);
            }

            $credentials = $request->only(['email', 'password']);
            $token = auth('api')->attempt($credentials);
            $user_service = UserService::whereServiceId($service->id)->whereUserId($user->id)->first();

            if ($token && !$user_service) {
                UserService::create([
                    "user_id" => $user->id,
                    "service_id" => $service->id
                ]);
            }

            if (!$token) {
                return response()->json(['message' => 'unauthorized'], 401);

            } elseif (!$user->verified) {

                $headers = [];
                $options = [
                    'user_phone_number' => $user->phone_number,
                    'user_email' => $user->email,
                    'user_id' => $user->id,
                    'type' => 'otp',
                    'mode' => 'otp',
                    'service' => 'GKB',
                    'sender_id' => '233242953672'
                ];
                 $this->notification_service->sendOtp($options, $headers);
                return response()->json(['message' => 'Account is not verified.','user' => $user], 401);
            }

            return $this->respondWithToken($token, $user);
        }catch (\Exception $exception){
            return response()->json(["message" => "failed"]);
        }
    }

    private function verifyOtp($options)
    {
        $headers = [];
        $res = $this->notification_service->verifyOtp($options,$headers);
        return $res["status"] == 200 && $res["response"]["status"] == "success";
    }

    /**
     * @param Request $request
     * @param $user
     */
    public function verifyUser(Request $request, $user): void
    {
        $options = [
            "phone_number" => explode(",", $request->get("contacts"))[0],
            "code" => $request->otp_code
        ];

        if ($verified = $this->verifyOtp($options)) {
            $user->update([
                "verified" => true
            ]);
        }
    }

}
