<?php

namespace App\Http\Controllers;

use App\Services\AuthorsService;
use App\Services\MapBoxService;
use App\Services\SubjectsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MapBoxServiceController  extends Controller
{
    private $mapbox_service;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->mapbox_service = new MapBoxService();
    }

    public function search(Request $request)
    {
        $options = $request->all();
        $query= $request->query("q");
        return $res = $this->mapbox_service->search($query,$options)["response"];
        $features = $res["features"];
        return $this->response($features,200);
    }

}
