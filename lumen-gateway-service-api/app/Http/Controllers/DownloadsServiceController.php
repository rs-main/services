<?php

namespace App\Http\Controllers;

use App\Models\Download;
use App\Services\DownloadsService;
use Illuminate\Http\Request;

class DownloadsServiceController  extends Controller
{
    private $download_service;

    public function __construct()
    {
        $this->middleware('auth');
        $this->download_service = new DownloadsService();
    }

    public function getBag(Request $request, $user_id){
        $options = $request->toArray();
        $res = $this->download_service->getBag($options, $user_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function sendToBag($id,Request $request){
        $headers = [];
        $options = $request->all();
        $res = $this->download_service->sendToBag($options, $headers,$id);
        return $this->response($res["response"],$res["status"]);
    }

    public function getFavourites($user_id, Request $request){
        $options = $request->all();
        $res = $this->download_service->getFavourites($options, $user_id);
        return $this->response($res["response"],$res["status"]);
    }

    public function addToFavourites(Request $request){
        $headers = [];
        $options = $request->all();
        $res = $this->download_service->addToFavourites($options, $headers);
        return $this->response($res["response"],$res["status"]);
    }

    public function download(Request $request,$id){
        $options = $request->all();
        $res = $this->download_service->getDownload($options, $id);
        return $this->response($res["response"],$res["status"]);
    }

    public function delete($id){
        $res = $this->download_service->deleteDownload($id);
        return $this->response($res["response"],$res["status"]);
    }

}
