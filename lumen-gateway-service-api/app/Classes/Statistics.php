<?php

namespace App\Classes;

use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Statistics
{
    private $service;
    private $user;

    /**
     * @param $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function getUserCounts(){
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->select("id")->count();
    }

    public function getUserCountBySchoolType($type): int
    {
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->whereJsonContains("school_object->type",$type)->count();

    }

    public function getUserRegistrationCountsPerMonth(){
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
//        })->select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(created_at) as month"))
        })->select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTH(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy('month')
            ->orderByDesc("month")
            ->get();
    }

    public function getUserRegistrationMonthsBuilder(){
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTH(created_at) as month"))
//        })->select(DB::raw("(COUNT(*)) as count"), DB::raw("MONTH(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy('month')
            ->orderBy("month");
    }

    public function getDeviceCounts(){
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->select(DB::raw("(COUNT(*)) as count"), "device")
            ->groupBy("device");
    }

    public function getDeviceTypeCounts(){
        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->select(DB::raw("(COUNT(*)) as count"), "device")->groupBy("device")->pluck("count");
    }



    public function getRegionCounts(){
        $region_ids =  User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->pluck("region_id");

        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->leftJoin("regions","regions.id","=","users.region_id")
            ->whereIn("region_id", $region_ids)
            ->select(DB::raw("(COUNT(*)) as count"), "regions.name", "coordinates")
            ->groupBy("region_id")
            ->orderBy("name");
    }

    public function getRegionData(){
        $region_ids =  User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->pluck("region_id");

        return User::whereHas("services",function ($query){
            $query->where("service_id",$this->service->id);
        })->leftJoin("regions","regions.id","=","users.region_id")
            ->whereIn("region_id", $region_ids)
            ->select(DB::raw("(COUNT(*)) as count"), "regions.name")
            ->groupBy("region_id")->get();
    }
}
