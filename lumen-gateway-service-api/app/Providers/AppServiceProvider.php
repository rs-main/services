<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */

    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Configure global rate limiter
     *
     * @return void
     */
    public function boot()
    {
        app(\Illuminate\Cache\RateLimiter::class)->for('global', function () {
            return \Illuminate\Cache\RateLimiting\Limit::perMinute(60)->by(request()->ip());
        });
    }
}
