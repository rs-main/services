<?php

namespace App\Jobs;

use App\Models\AchievementThreshold;
use App\Models\LeaderBoardScore;
use App\Models\UserAchievement;
use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class AchievementsJob extends Job implements ShouldQueue
{
    private $leaderboardScoreId;
    private $userId;
    public function __construct($leaderboardscore_id)
    {
        $this->leaderboardScoreId = $leaderboardscore_id;

    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $score = LeaderBoardScore::find($this->leaderboardScoreId);

        $achievements = AchievementThreshold::where("leaderboard_id",$score->leaderboard_id)->get();
        foreach ($achievements as $achievement){
            if ($score->scores == $achievement->score || $score->scores > $achievement->score){
                $user_achievement = UserAchievement::whereUserId($score->user_id)
                    ->where("leaderboard_id", $score->leaderboard_id)
                    ->where("achievement_id",$achievement->achievement_id)->first();

                if (!$user_achievement){
                    UserAchievement::create([
                        "user_id" => $score->user_id,
                        "leaderboard_id" => $score->leaderboard_id,
                        "achievement_id" => $achievement->achievement_id
                    ]);
                }
            }
        }

    }
}
