<?php

namespace App\Jobs;

use App\Services\NotificationService;
use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSmsJob extends Job implements ShouldQueue
{

    private $options;
    private $notification_service;
    public function __construct($options)
    {
        $this->options = $options;
        $this->notification_service = new NotificationService();

    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $headers = [];
        $this->notification_service->bulkSms($this->options,$headers);
    }
}
