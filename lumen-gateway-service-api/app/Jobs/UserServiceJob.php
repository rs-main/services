<?php

namespace App\Jobs;

use App\Models\User;
use App\Services\ClassService;
use App\Services\CourseService;
use App\Services\InstitutionService;
use App\Services\NotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserServiceJob implements ShouldQueue
{
    use  InteractsWithQueue, Queueable;

    private $user;
    private $institution_service;
    private $course_service;
    private $class_service;
    private $notification_service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->institution_service  = new InstitutionService();
        $this->course_service       = new CourseService();
        $this->class_service        = new ClassService();
        $this->notification_service = new NotificationService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_object =   User::find($this->user->id);
        if ($this->user && $this->user->school_id != null){
          $user_object->update([
                "school_object" => $this->getInstitutionObject($this->user->school_id)
            ]);
        }

        if ($this->user && $this->user->class_id != null){
            $user_object->update([
                "class_object" => $this->getClassObject($this->user->class_id)
            ]);
        }

        if ($this->user && $this->user->course_id != null){
            $user_object->update([
                "course_object" => $this->getCourseObject($this->user->course_id)
            ]);
        }
//        if ($user_object->phone_number != null) {
//            $options = ["user_phone_number" => $this->user->phone_number];
//            $this->sendOtp($options);
//        }
    }

    private function getInstitutionObject($id){
        $response =  ($this->institution_service->getInstitution($id))["response"];
        return ($response["data"]) ?? null;
    }

    private function getClassObject($id){
        $response = $this->class_service->getClass($id)["response"];
        return ($response["data"]) ?? null;
    }

    private function getCourseObject($id){
        $response = $this->course_service->getCourse($id)["response"];
        return ($response["data"]) ?? null;
    }

    private function sendOtp($options){
        $headers = [];
        $this->notification_service->sendOtp($options, $headers);
    }

}
