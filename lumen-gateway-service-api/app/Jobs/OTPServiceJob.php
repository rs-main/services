<?php

namespace App\Jobs;

use App\Models\User;
use App\Services\ClassService;
use App\Services\CourseService;
use App\Services\InstitutionService;
use App\Services\NotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OTPServiceJob implements ShouldQueue
{
    use  InteractsWithQueue, Queueable;

    private $user;
    private $institution_service;
    private $course_service;
    private $class_service;
    private $notification_service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->notification_service = new NotificationService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            $options = ["user_phone_number" => $this->user->phone_number];
            $this->sendOtp($options);
    }

    private function sendOtp($options){
        $headers = [];
        $this->notification_service->sendOtp($options, $headers);
    }

}
