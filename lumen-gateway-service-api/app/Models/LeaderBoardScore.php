<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaderBoardScore extends Model
{
    use HasFactory;

    protected $table = "leaderboard_scores";

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function leaderboard(){
        return $this->belongsTo(LeaderBoard::class);
    }

}
