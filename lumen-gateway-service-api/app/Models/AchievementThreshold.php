<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AchievementThreshold extends Model
{
    use HasFactory;

    protected $table = "achievement_thresholds";

    protected $guarded = [];

    public function leaderboard(){
        return $this->belongsTo(LeaderBoard::class,"leaderboard_id");
    }

    public function achievement(){
        return $this->belongsTo(Achievement::class,"achievement_id");
    }

}
