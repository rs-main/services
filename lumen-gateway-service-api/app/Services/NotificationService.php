<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class NotificationService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $campaign_url;
    private  $messaging_parameters_url;
    private  $statistics_url;
    private  $notify_url;
    private  $bulk_sms_url;
    private  $notify_otp_url;
    private  $verify_otp_url;
    private  $create_notify_url;
    private  $sms_balance_url;

    public function __construct()
    {
        $this->base_url          = config("services.notifications.base_url");
        $this->secret            = config("services.notifications.secret");
        $this->campaign_url      = config("services.notifications.endpoints.campaign_url");
        $this->messaging_parameters_url      = config("services.notifications.endpoints.messaging_parameters_url");
        $this->statistics_url      = config("services.notifications.endpoints.statistics_url");
        $this->notify_url        = config("services.notifications.endpoints.notify_url");
        $this->bulk_sms_url      = config("services.notifications.endpoints.bulk_sms_url");
        $this->notify_otp_url    = config("services.notifications.endpoints.notify_otp_url");
        $this->verify_otp_url    = config("services.notifications.endpoints.verify_otp_url");
        $this->create_notify_url = config("services.notifications.endpoints.create_notify_url");
        $this->sms_balance_url   = config("services.notifications.endpoints.sms_balance_url");
    }

    public function campaigns($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->campaign_url,$options,[]);
    }

    public function messagingParameters($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->messaging_parameters_url,$options,[]);
    }

    public function monthlyStatistics($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->statistics_url,$options,[]);
    }

    public function bulkSms($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->bulk_sms_url,$options,$headers);
    }

    public function notify($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->notify_url,$options,$headers);
    }

    public function sendOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->notify_otp_url,$options,$headers);
    }

    public function verifyOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->verify_otp_url,$options,$headers);
    }

    public function createNotification($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->create_notify_url,$options,$headers);
    }

    public function userNotifications($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->create_notify_url,$options,[]);
    }

    public function smsBalance($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->sms_balance_url,$options,[]);
    }

}
