<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class SubjectsService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $subjects_url;


    public function __construct()
    {
        $this->base_url          = config("services.subjects.base_url");
        $this->secret            = config("services.subjects.secret");
        $this->subjects_url      = config("services.subjects.endpoints.subjects_url");
    }

    public function getSubjects($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->subjects_url,$options, []);
    }

    public function saveSubject($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->subjects_url,$options,$headers);
    }

    public function getSubject($id){
        $fetch_url = $this->subjects_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteSubject($id){
        $delete_url = $this->subjects_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
