<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class WikipediaService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $search_url;

    public function __construct()
    {
        $this->base_url   = config("services.wiki.base_url");
        $this->secret     = config("services.wiki.secret");
        $this->search_url = config("services.wiki.endpoints.search_url");
    }

    public function search($params){
//        $url = $this->search_url."?q=$query";
        $url = $this->search_url . "?" . http_build_query( $params );
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$url,[],[]);
    }

}
