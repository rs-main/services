<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class CommentService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $comments_url;
    private  $user_comments_url;
    private  $comment_users_url;
    private  $subject_comments_url;
    private  $like_comments_url;
    private  $dislike_comments_url;
    private  $report_comment_url;
    private  $hide_comment_url;
    private  $participants_url;


    public function __construct()
    {
        $this->base_url                 = config("services.comments.base_url");
        $this->secret                   = config("services.comments.secret");
        $this->comments_url             = config("services.comments.endpoints.comments_url");
        $this->user_comments_url        = config("services.comments.endpoints.user_comments_url");
        $this->comment_users_url        = config("services.comments.endpoints.comment_users_url");
        $this->subject_comments_url     = config("services.comments.endpoints.subject_comments_url");
        $this->like_comments_url        = config("services.comments.endpoints.like_comments_url");
        $this->dislike_comments_url     = config("services.comments.endpoints.dislike_comments_url");
        $this->report_comment_url       = config("services.comments.endpoints.report_comment_url");
        $this->hide_comment_url         = config("services.comments.endpoints.hide_comment_url");
        $this->participants_url         = config("services.comments.endpoints.participants_url");
    }

    public function getComments($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->comments_url,$options, []);
    }

    public function saveComment($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->comments_url,$options,$headers);
    }

    public function getComment($id){
        $fetch_url = $this->comments_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteComment($id){
        $delete_url = $this->comments_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

    public function getUserComments($user_id){
        $fetch_url = $this->comments_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function getCommentUsers($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->comments_url,$options, []);
    }

    public function getSubjectComments($subject_id){
        $fetch_url = $this->subject_comments_url.$subject_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }


    public function likeComment($user_id,$comment_id){
        $like_url = $this->comments_url.$user_id."/".$comment_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$like_url,[],[]);
    }

    public function dislikeComment($user_id,$comment_id){
        $like_url = $this->dislike_comments_url.$user_id."/".$comment_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$like_url,[],[]);
    }

    public function reportComment($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->report_comment_url,$options,$headers);
    }

    public function hideComment($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->report_comment_url,$options,$headers);
    }

    public function getParticipants($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->participants_url,$options, []);
    }

}
