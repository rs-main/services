<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class CurriculumService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $curriculums_url;
    private  $curriculum_subjects_url;
    private  $add_subject_curriculum_url;


    public function __construct()
    {
        $this->base_url          = config("services.curriculums.base_url");
        $this->secret            = config("services.curriculums.secret");
        $this->curriculums_url  = config("services.curriculums.endpoints.curriculums_url");
        $this->curriculum_subjects_url  = config("services.curriculums.endpoints.curriculum_subjects_url");
        $this->add_subject_curriculum_url  = config("services.curriculums.endpoints.add_subject_curriculum_url");
    }

    public function getCurriculums($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->curriculums_url,$options, []);
    }

    public function saveCurriculum($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->curriculums_url,$options,$headers);
    }

    public function getCurriculum($id){
        $fetch_url = $this->curriculums_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function getCurriculumSubjects($id){
        $fetch_url = $this->curriculum_subjects_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function addSubjectToCurriculum($options, $headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->add_subject_curriculum_url,$options,$headers);
    }

    public function deleteCurriculum($id){
        $delete_url = $this->curriculums_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
