<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class LevelService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $levels_url;


    public function __construct()
    {
        $this->base_url          = config("services.levels.base_url");
        $this->secret            = config("services.levels.secret");
        $this->levels_url        = config("services.levels.endpoints.levels_url");
    }

    public function getLevels($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->levels_url,$options, []);
    }

    public function saveLevel($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->levels_url,$options,$headers);
    }

    public function getLevel($id){
        $fetch_url = $this->levels_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteLevel($id){
        $delete_url = $this->levels_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
