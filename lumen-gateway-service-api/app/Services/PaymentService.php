<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class PaymentService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $make_payment_url;
    private  $verify_payment_url;
    private  $otp_url;
    private  $fund_wallet_url;
    private  $user_wallet_url;
    private  $user_balance_url;
    private  $transactions_url;

    public function __construct()
    {
        $this->base_url           = config("services.payments.base_url");
        $this->secret             = config("services.payments.secret");
        $this->make_payment_url   = config("services.payments.endpoints.make_payment_url");
        $this->verify_payment_url = config("services.payments.endpoints.verify_payment_url");
        $this->otp_url            = config("services.payments.endpoints.submit_otp");
        $this->fund_wallet_url    = config("services.payments.endpoints.fund_wallet");
        $this->user_wallet_url    = config("services.payments.endpoints.user_wallet");
        $this->user_balance_url   = config("services.payments.endpoints.user_balance");
        $this->transactions_url   = config("services.payments.endpoints.transactions");
    }

    public function getTransactions($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->make_payment_url,$options, []);
    }

    public function makePayment($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->make_payment_url,$options,$headers);
    }

    public function verifyPayment($reference){
        $verify_payment_url = $this->verify_payment_url.$reference;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$verify_payment_url,[],[]);
    }

    public function submitOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->otp_url,$options,$headers);
    }

    public function fundWallet($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->fund_wallet_url,$options,$headers);
    }

    public function userWallet($user_id){
        $user_wallet_url = $this->user_wallet_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$user_wallet_url,[],[]);
    }

    public function userBalance($user_id){
        $user_balance_url = $this->user_balance_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$user_balance_url,[],[]);
    }

    public function userTransactions($user_id){
        $user_balance_url = $this->transactions_url.$user_id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$user_balance_url,[],[]);
    }



}
