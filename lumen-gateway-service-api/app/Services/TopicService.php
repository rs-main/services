<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class TopicService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $topics_url;
    private  $sub_topics_url;


    public function __construct()
    {
        $this->base_url          = config("services.topics.base_url");
        $this->secret            = config("services.topics.secret");
        $this->topics_url        = config("services.topics.endpoints.topics_url");
        $this->sub_topics_url        = config("services.topics.endpoints.sub_topics_url");
    }

    public function getTopics($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->topics_url,$options, []);
    }

    public function saveTopic($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->topics_url,$options,$headers);
    }

    public function saveSubTopic($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->sub_topics_url,$options,$headers);
    }

    public function getTopic($id){
        $fetch_url = $this->topics_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteToic($id){
        $delete_url = $this->topics_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
