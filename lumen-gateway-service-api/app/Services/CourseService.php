<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class CourseService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $course_url;


    public function __construct()
    {
        $this->base_url          = config("services.courses.base_url");
        $this->secret            = config("services.courses.secret");
        $this->course_url        = config("services.courses.endpoints.courses_url");
    }

    public function getCourses($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->course_url,$options, []);
    }

    public function saveCourse($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->course_url,$options,$headers);
    }

    public function getCourse($id){
        $fetch_url = $this->course_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteCourse($id){
        $delete_url = $this->course_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
