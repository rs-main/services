<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class InstitutionTypeService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $institution_type_url;


    public function __construct()
    {
        $this->base_url                 = config("services.institution_types.base_url");
        $this->secret                   = config("services.institution_types.secret");
        $this->institution_type_url     = config("services.institution_types.endpoints.institution_type_url");
    }

    public function getInstitutionTypes($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->institution_type_url,$options, []);
    }

    public function saveInstitutionType($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->institution_type_url,$options,$headers);
    }

    public function getInstitutionType($id){
        $fetch_url = $this->institution_type_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteInstitutionType($id){
        $delete_url = $this->institution_type_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }


}
