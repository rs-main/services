<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class AuthorsService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $authors_url;


    public function __construct()
    {
        $this->base_url          = config("services.authors.base_url");
        $this->secret            = config("services.authors.secret");
        $this->authors_url      = config("services.authors.endpoints.authors_url");
    }

    public function getAuthors($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->authors_url,$options, []);
    }

    public function saveAuthor($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->authors_url,$options,$headers);
    }

    public function getAuthor($id){
        $fetch_url = $this->authors_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function deleteAuthor($id){
        $delete_url = $this->authors_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
