<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class MapBoxService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $locations_url;

    const BASE_URL = "https://api.mapbox.com/";
    const SEARCH_URL = "geocoding/v5/mapbox.places/";
    const SECRET = "pk.eyJ1IjoicmVhbHN0dWRpb3Mtd2ViIiwiYSI6ImNreGl2dXF1YjY5ZjgydGxhdjJxcTMzMXcifQ.gGB_m9YlHT1L3FDjcySGBw";

    public function __construct()
    {
    }

    public function search($q){
        $options = [
            "access_token" => self::SECRET
        ];

        $url = self::SEARCH_URL.$q.".json";
        return $this->performRequestWithStatus(self::BASE_URL,
            $this->secret,"GET",$url,$options, []);
    }


}
