<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class UploadService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $files_url;
    private  $upload_url;

    public function __construct()
    {
        $this->base_url          = config("services.upload_service.base_url");
        $this->secret            = config("services.upload_service.secret");
        $this->upload_url        = config("services.upload_service.endpoints.upload_url");
    }


    public function allFiles($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->upload_url,$options, []);
    }

    public function upload($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->upload_url,$options,$headers);
    }

    public function uploadFile($options, $file,$file_name){
        return $this->performRequestWithFile($this->base_url,$this->secret,$this->upload_url,$options,$file,$file_name);
    }

    public function fetchFile($options,$id){
        $fetch_url = $this->upload_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,$options,[]);
    }


}
