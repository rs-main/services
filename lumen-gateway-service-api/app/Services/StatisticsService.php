<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class StatisticsService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $statistics_url;


    public function __construct()
    {
        $this->base_url          = config("services.statistics.base_url");
        $this->secret            = config("services.statistics.secret");
        $this->statistics_url      = config("services.statistics.endpoints.statistics_url");
    }

    public function getStatistics($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->statistics_url,$options, []);
    }


}
