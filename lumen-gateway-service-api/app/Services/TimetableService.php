<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class TimetableService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $timetable_url;


    public function __construct()
    {
        $this->base_url          = config("services.timetable.base_url");
        $this->secret            = config("services.timetable.secret");
        $this->timetable_url     = config("services.timetable.endpoints.url");
    }

    public function getAll($options){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$this->timetable_url,$options, []);
    }

    public function save($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->timetable_url,$options,$headers);
    }

    public function get($id){
        $fetch_url = $this->timetable_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"GET",$fetch_url,[],[]);
    }

    public function delete($id){
        $delete_url = $this->timetable_url.$id;
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$delete_url,[],[]);
    }

}
