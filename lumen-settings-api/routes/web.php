<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    // API Integration routes
    $router->get('integrations',       'IntegrationsController@index');
    $router->post('integrations',      'IntegrationsController@store');
    $router->get('integrations/{id}',  'IntegrationsController@show');
    $router->post('integrations/{id}', 'IntegrationsController@delete');
    $router->get('integrations/stats/all', 'IntegrationsController@stats');

    // API Settings routes
    $router->get('settings',       'SettingsController@index');
    $router->post('settings',      'SettingsController@store');
});

