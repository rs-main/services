<?php

namespace App\Libraries\SmsLibraries;

use App\Interfaces\Sms\ISms;
use App\Traits\ConsumeExternalService;
use Illuminate\Support\Facades\URL;

class CoreSmsLibrary implements ISms
{
    use ConsumeExternalService;

    public const CORE_SMS_SERVICE_PROVIDER = "core-sms";
    private $text;
    private $apiKey;
    public $baseUri;
    public $secret;
    private $url;
    private $verify_url;

    public function __construct()
    {
        $this->baseUri    = env('CORE_SMS_SERVICE_BASE_URL');
        $this->url        = '/otp/send';
        $this->apiKey     = env("CORE_SMS_API_TOKEN");
    }

    public function buildMessage($text)
    {
        $this->text = $text;
    }

    public function sendSms($to){
        $formParams = json_encode(["apiKey" => $this->apiKey, "msisdn" => $to]);
        $response = $this->performRequest($this->baseUri,"","POST",$this->url, $formParams,[]);
        return  (json_decode($response));
    }



}
