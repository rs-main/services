<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchProvider extends Model
{
    use HasFactory;

    protected $table = "search_providers";

    protected $guarded = [];


}
