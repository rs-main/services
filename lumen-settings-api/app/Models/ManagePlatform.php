<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagePlatform extends Model
{
    use HasFactory;

    protected $table = "manage_platform";

    protected $guarded = [];

    protected $casts = [
        "background_images" => "array",
        "search_providers"  => "array",
        "landing_page"  => "array",
    ];

}
