<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Integration;
use App\Models\SearchProvider;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SearchProvidersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request){

    }

    public function store(Request $request){

        $this->validate($request,[
            "provider_name" => "required|unique:search_providers",
            "access_key" => "required",
            "api_link" => "required",
            "logo_url"       => "required"
        ]);

        $search = new SearchProvider();
        $search->provider_name = $request->provider_name;
        $search->access_key = $request->access_key;
        $search->api_link = $request->api_link;
        $search->logo_url = $request->logo_url;
        $search->order    = $request->order;
        $search->save();

        return $this->response($search,200);
    }

    public function update($id, Request $request){
        $this->validate($request,[
            "provider_name" => "required|unique:search_providers",
            "access_key" => "required",
            "api_link" => "required",
            "logo_url"       => "required"
        ]);

        $search = SearchProvider::find($id)->update($request->only(["provider_name", "access_key","api_link","logo_url"]));
        return $this->response($search, 200);
    }

    public function delete($id){
        $search_provider = SearchProvider::find($id);

        if ($search_provider){
            $search_provider->delete();
        }

        return $this->response([], 200);
    }

}
