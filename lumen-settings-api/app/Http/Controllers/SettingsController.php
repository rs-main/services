<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Integration;
use App\Models\ManagePlatform;
use App\Models\SearchProvider;
use App\Models\Setting;
use App\Services\NestJsService;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SettingsController extends Controller
{

    private $notification_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->notification_service = new NotificationService();
    }

    public function index(){
        $setting = Setting::first();
        return $this->response($setting,200);
    }

    public function store(Request $request){

        $verified = false;
        $setting = Setting::first();
        if ($setting){
            $setting->update($request->only(
                $this->getKeys()
            ));

            $this->saveContacts($request, $setting);

            if (
                $request->has("bank_account_name")
                || $request->has("bank_account_number")
                || $request->has("bank_branch")
                && $request->has("contacts")
                && $request->has("otp_code")){

                $options = [
                    "phone_number" => explode(",",$request->get("contacts"))[0],
                    "code" => $request->otp_code
                ];

                if($verified = $this->verifyOtp($options)){
                    $this->updateBankDetails($setting, $request);
                }
            }

        }else{
            $setting = Setting::create($request->only($this->getKeys()));
            $this->updateBankDetails($setting, $request);
        }

        return $this->response($setting,200);
//        return $this->response($verified,200);
    }

    /**
     * @return string[]
     */
    public function getKeys(): array
    {
        return [
            "revenue_distribution",
            "auto_payout_to_publishers_interval",
            "auto_receive_payment_interval",
            "facebook_page",
            "twitter_page",
            "linkedin_page",
            "instagram_handle",
            "gkb_admin_signature_message",
            "ges_signature_message",
            "moe_signature_message",
            "default_table_view",
            "videos_upload_size_limit",
            "audio_books_upload_size_limit",
            "ebooks_upload_size_limit",
        ];
    }

    /**
     * @param Request $request
     * @param $setting
     */
    public function saveContacts(Request $request, $setting): void
    {
        if ($request->has("contacts")) {
            $setting->update([
                "contact_info" => (explode(",", $request->get("contacts")))
            ]);
        }
    }

    /**
     * @param $setting
     * @param Request $request
     */
    public function updateBankDetails($setting, Request $request): void
    {
        $setting->update($request->only("bank_name", "bank_account_name", "bank_account_number", "bank_branch"));
    }

    private function sendOtp($options)
    {
        $headers = [];
        $res = $this->notification_service->sendOtp($options,$headers);
        return $this->response($res["response"],$res["status"]);
    }

    private function verifyOtp($options)
    {
        $headers = [];
        $res = $this->notification_service->verifyOtp($options,$headers);
        return $res["status"] == 200 && $res["response"]["status"] == "success";
    }

}
