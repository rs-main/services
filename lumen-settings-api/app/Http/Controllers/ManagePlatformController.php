<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\ManagePlatform;
use App\Models\SearchProvider;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ManagePlatformController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request){

//        return DataTables::of($builder)->make(true);
    }

    public function storeLandingPage(Request $request){

        $this->validate($request,[
            "header_title" => "required",
            "sub_header" => "required",
            "introduction" => "required",
        ]);

        $manage_platform = ManagePlatform::first();
        $manage_platform->update([
            "landing_page" => json_encode($request->only(["header_title","sub_header","introduction"]))
        ]);

        return $this->response($manage_platform,200);
    }

    public function update(Request $request){
        $this->validate($request,[
            "header_title" => "required",
            "sub_header" => "required",
            "introduction" => "required",
        ]);

        $manage_platform = ManagePlatform::first();
        $manage_platform->update([
            "landing_page" => json_encode($request->only(["header_title","sub_header","introduction"]))
        ]);

        return $this->response($manage_platform,200);
    }

}
