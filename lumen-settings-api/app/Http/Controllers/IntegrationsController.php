<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Integration;
use App\Models\ManagePlatform;
use App\Models\SearchProvider;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class IntegrationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $integrations = Integration::orderByDesc("created_at")->get();
        return DataTables::of($integrations)->make(true);
    }

    public function store(Request $request){

        $this->validate($request,[
            "institution_name" => "required|unique:integrations",
            "integration_type" => "required",
            "method" => "required",
            "access_link" => "required",
            "api_key" => "required",
        ]);

        $integration =
            Integration::create($request->only([
            "institution_name",
            "type",
            "integration_type",
            "method",
            "access_link",
            "api_key",
            "access_token",
            "status",
            "notes"
        ]));

        return $this->response($integration,200);
    }

    public function update($id,Request $request){
        $this->validate($request,[
            "institution_name" => "required",
            "integration_type" => "required",
            "method" => "required",
            "access_link" => "required",
            "api_key" => "required",
        ]);

        $integration =
            Integration::find($id)->update($request->only([
                "institution_name",
                "type",
                "integration_type",
                "method",
                "access_link",
                "api_key",
                "access_token",
                "status",
                "notes"
            ]));

        return $this->response($integration,200);
    }

    public function delete($id){
        $integration = Integration::find($id);

        if ($integration){
            $integration->delete();
        }

    }

    public function stats(){

        $integrations_count = \DB::table("integrations")->count();
        $search_providers   = \DB::table("integrations")->where("type","search")->count();
        $sso   = \DB::table("integrations")->where("type","sso")->count();
        $incoming_connections   = \DB::table("integrations")->where("access_link","incoming")->count();
        $outgoing_connections   = \DB::table("integrations")->where("access_link","outgoing")->count();

        return response()->json([
            "integrations_count" => $integrations_count,
            "search_providers" => $search_providers,
            "sso"              => $sso,
            "incoming"         => $incoming_connections,
            "outgoing"         => $outgoing_connections,
        ]);

    }

}
