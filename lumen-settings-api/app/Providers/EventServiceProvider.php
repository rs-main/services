<?php

namespace App\Providers;

use App\Models\SearchProvider;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use SearchProviderObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

    ];

    public function boot()
    {
        parent::boot();
//        SearchProvider::observe(SearchProviderObserver::class);
    }
}
