<?php

use App\Models\SearchProvider;

class LandingPageObserver
{

    /**
     * Handle the User "created" event.
     *
     * @param SearchProvider $searchProvider
     * @return void
     */
    public function created(SearchProvider $searchProvider)
    {
        //
    }

    /**
     * Handle the User "updated" event.
     *
     * @param SearchProvider $searchProvider
     * @return void
     */
    public function updated(SearchProvider $searchProvider)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param SearchProvider $searchProvider
     * @return void
     */
    public function deleted(SearchProvider $searchProvider)
    {
        //
    }

    /**
     * Handle the User "forceDeleted" event.
     *
     * @param SearchProvider $searchProvider
     * @return void
     */
    public function forceDeleted(SearchProvider $searchProvider)
    {
        //
    }
}
