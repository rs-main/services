<?php

namespace App\Console\Commands;

use App\Payment\Api\V1\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class PublishPaymentChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:publish-payment {message}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish to MobileMoneyPayment Channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Redis::publish('payment', json_encode(["message"=>  $this->argument("message")]));
    }
}
