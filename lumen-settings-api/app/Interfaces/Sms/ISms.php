<?php

namespace App\Interfaces\Sms;

interface ISms
{
    public function buildMessage($text);

    public function sendSms($to);

}
