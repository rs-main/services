<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class NotificationService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $notify_url;
    private  $notify_otp_url;
    private  $verify_otp_url;

    public function __construct()
    {
        $this->base_url       = config("services.notifications.base_url");
        $this->secret         = config("services.notifications.secret");
        $this->notify_url     = config("services.notifications.endpoints.notify_url");
        $this->verify_otp_url = config("services.notifications.endpoints.verify_otp_url");
    }

    public function notify($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->notify_url,$options,$headers);
    }

    public function sendOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->notify_otp_url,$options,$headers);
    }

    public function verifyOtp($options,$headers){
        return $this->performRequestWithStatus($this->base_url,$this->secret,"POST",$this->verify_otp_url,$options,$headers);
    }

}
