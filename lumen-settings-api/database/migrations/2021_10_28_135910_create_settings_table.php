<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->decimal("revenue_distribution")->default(0.0)->nullable();
            $table->string("auto_payout_to_publishers_interval",15)->nullable();
            $table->string("auto_receive_payment_interval",15)->nullable();
            $table->string("bank_name",50)->nullable();
            $table->string("bank_account_name",100)->nullable();
            $table->string("bank_account_number",50)->nullable();
            $table->string("bank_branch",100)->nullable();
            $table->json("contact_info")->nullable();
            $table->string("facebook_page",60)->nullable();
            $table->string("twitter_page",60)->nullable();
            $table->string("linkedin_page",60)->nullable();
            $table->string("instagram_handle",60)->nullable();
            $table->string("gkb_admin_signature_message")->nullable();
            $table->string("ges_signature_message")->nullable();
            $table->string("moe_signature_message")->nullable();
            $table->integer("default_table_view")->default(0);
            $table->integer("videos_upload_size_limit")->default(0);
            $table->integer("audio_books_upload_size_limit")->default(0);
            $table->integer("ebooks_upload_size_limit")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
