<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagePlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_platform', function (Blueprint $table) {
            $table->id();
            $table->enum("interchange_frequency",["hourly","daily","weekly","monthly"]);
            $table->json("background_images")->nullable();
            $table->json("search_providers")->nullable();
            $table->json("landing_page_object")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_platform');
    }
}
