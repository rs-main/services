<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsBackgroundImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_background_images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("students_id");
            $table->foreign("students_id")->references("id")->on("students")->onDelete("cascade");
            $table->unsignedBigInteger("background_image_id");
            $table->foreign("background_image_id")->references("id")->on("background_images")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_background_images');
    }
}
