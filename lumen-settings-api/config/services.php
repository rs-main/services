<?php

use Illuminate\Support\Str;

return [

    'nexmo' => [
        'sms_from' => '233242953672',
    ],

    'notifications' => [
        'base_url' => "http://notifications:8005",
        "secret"   => env("NOTIFICATION_SERVICE_SECRET"),
        "endpoints" => [
            "notify_url" => "/api/notify",
            "notify_otp_url" => "/api/notifications/otp/",
            "verify_otp_url" => "/api/notifications/verify-otp/",
        ]
    ],
];
