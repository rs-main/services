# gateway database
CREATE DATABASE IF NOT EXISTS `gateway_db`;
GRANT ALL PRIVILEGES ON  `gateway_db`.* TO 'user'@'%';
# payments database
CREATE DATABASE IF NOT EXISTS `payments_db`;
GRANT ALL PRIVILEGES ON  `payments_db`.* TO 'user'@'%';

CREATE DATABASE IF NOT EXISTS `uploads_db`;
GRANT ALL PRIVILEGES ON  `uploads_db`.* TO 'user'@'%';

CREATE DATABASE IF NOT EXISTS `institutions_db`;
GRANT ALL PRIVILEGES ON  `institutions_db`.* TO 'user'@'%';

CREATE DATABASE IF NOT EXISTS `notifications_db`;
GRANT ALL PRIVILEGES ON  `notifications_db`.* TO 'user'@'%';

CREATE DATABASE IF NOT EXISTS `curriculum_db`;
GRANT ALL PRIVILEGES ON  `curriculum_db`.* TO 'user'@'%';