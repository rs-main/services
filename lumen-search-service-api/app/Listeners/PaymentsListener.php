<?php

namespace App\Listeners;

use App\Events\PaymentInitiatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class PaymentsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PaymentInitiatedEvent $event
     * @return void
     */
    public function handle(PaymentInitiatedEvent $event)
    {
        $this->saveTransaction($event);
    }

    /**
     * @param PaymentInitiatedEvent $event
     */
    public function saveTransaction(PaymentInitiatedEvent $event): void
    {
        $data = $event;
        DB::beginTransaction();
        $this->insertTransaction($data);
        DB::commit();
        Redis::publish('payment', $data);
    }


    public function insertTransaction(PaymentInitiatedEvent  $event): void
    {
//        Throw new \Exception(($event));
    }

}
