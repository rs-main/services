<?php

namespace App\Http\Controllers;

use App\Jobs\SendNotification;
use App\Traits\NotificationTrait;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class NotificationController  extends Controller
{
    use NotificationTrait;

    public function __construct()
    {
        //
    }

    public function notify(Request $request){

        $this->validate($request,[
            "user" => "required",
            "type" => "required",
            "service" => "required",
            "mode"    => "required"
        ]);

        $user = $request->user;
        $type = $request->type;
        $service = $request->service;
        $mode = $request->mode;
        $message = $request->message;

        $job = new SendNotification($user,$type, $service,$mode,$message);
        $this->dispatch($job);
        return json_encode(["data" => ($user)]);
    }
}
