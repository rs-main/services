<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    public const XTRACLASS_SERVICE_CODE = "rs-xtra-class";
    public const XTRACLASS_SERVICE_NAME = "XTRA CLASS";

    public const PAYMENT_SERVICE_CODE = "rs-payment";

    public const GKB_SERVICE_CODE = "rs-gkb";
    public const GKB_SERVICE_NAME = "Ghana Knowledge Bank";

}
