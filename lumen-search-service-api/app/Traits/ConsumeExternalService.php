<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

trait ConsumeExternalService
{
    /**
     * Send request to any service
     * @param $base_uri
     * @param $secret
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return mixed
     */
    public function performRequest($base_uri, $secret, $method, $requestUrl, array $formParams = [], $headers = [])
    {
//        $headers['Authorization'] = $secret;
        $response = Http::withHeaders($headers)->withBody(
            json_encode($formParams),"application/json")->post($base_uri.$requestUrl);

        return json_decode($response->body(), true);
    }
}
