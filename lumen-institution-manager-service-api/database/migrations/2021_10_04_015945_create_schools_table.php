<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("short_name")->nullable();
            $table->enum("type",["basic","senior-high","tertiary"]);
            $table->bigInteger("region_id")->index()->nullable();
            $table->bigInteger("location_id")->index()->nullable();
//            $table->bigInteger("contact_id")->index()->nullable();
            $table->enum("ownership",["public","private","non-private"]);
            $table->enum("sex",["mixed","male","female"]);
            $table->enum("students",["regular","special-needs"]);
            $table->enum("classification",["first-class","second-class","third-class"])->nullable();
            $table->string("school_code")->nullable();
            $table->integer("population")->nullable();
            $table->string("bio_info")->nullable();
            $table->string("ghana_post_code")->nullable();
            $table->string("email")->nullable();
            $table->string("website")->nullable();
            $table->json("colours")->nullable();
            $table->string("logo_url")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
