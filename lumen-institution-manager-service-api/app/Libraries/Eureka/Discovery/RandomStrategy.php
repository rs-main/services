<?php

namespace App\Libraries\Eureka\Discovery;


use App\Libraries\Eureka\Interfaces\DiscoveryStrategy;

class RandomStrategy implements DiscoveryStrategy {

    public function getInstance($instances) {
        if(count($instances) == 0)
            return null;

        return $instances[rand(0, count($instances) - 1)];
    }
}
