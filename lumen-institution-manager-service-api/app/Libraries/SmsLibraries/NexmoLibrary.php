<?php

namespace App\Libraries\SmsLibraries;

use App\Interfaces\Sms\ISms;

class NexmoLibrary implements ISms
{
    private $nexmo;
    private $text;

    public function __construct()
    {
        $this->nexmo  = app('Nexmo\Client');
    }

    public function buildMessage($text)
    {
        $this->text = $text;
    }


    public function sendSms($to){
        $this->nexmo->message()->send(
            [
                'to'   => $to,
                'from' => env("SMS_FROM_NUMBER"),
                'text' => $this->text
            ]
        );
    }



}
