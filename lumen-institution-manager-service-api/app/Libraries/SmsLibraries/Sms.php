<?php

namespace App\Libraries\SmsLibraries;


class Sms
{
    private $nexmo;
    private $coreSms;
    private $text;
    private $to;

    public function __construct($to,$text)
    {
        $this->text = $text;
        $this->to = $to;

        // Nexmo Implementation
        $this->nexmo = new NexmoLibrary();
        $this->nexmo->buildMessage($text);


        // Core Sms
        $this->coreSms = new CoreSmsLibrary();
        $this->coreSms->buildMessage($text);
    }

    public function sendSms()
    {
        $this->nexmo->sendSms($this->to);
    }

    public function sendOtp(){
        $this->coreSms->sendSms($this->to);
    }
}
