<?php

namespace App\Helpers;

class Helper
{
    public static function DetermineNetWork($prefix){
        $has_zero_prefix = substr($prefix,0,1) == 0;
        $prefix_code = $has_zero_prefix ? $prefix : "0".$prefix;
        $network = false;

        switch ($prefix_code){
            case "024":
            case "054":
            case "055":
            case "059":
                return $network = "MTN";
            case "020":
            case "050":
                return $network = "Vodafone";
            case "027":
            case "057":
            case "026":
            case "056":
                return $network = "AirtelTigo";
            default:
                return $network;
        }
    }
}
