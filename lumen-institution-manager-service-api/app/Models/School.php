<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\School
 *
 * @property int $id
 * @property string $name
 * @property string|null $short_name
 * @property string $type
 * @property int|null $region_id
 * @property int|null $location_id
 * @property int|null $contact_id
 * @property string $ownership
 * @property string $sex
 * @property string $students
 * @property string|null $classification
 * @property string|null $school_code
 * @property int|null $population
 * @property string|null $bio_info
 * @property string|null $ghana_post_code
 * @property string|null $email
 * @property string|null $website
 * @property string|null $colours
 * @property string|null $logo_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Integration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration query()
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereBioInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereClassification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereColours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereContactId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereGhanaPostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereLogoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereOwnership($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration wherePopulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereSchoolCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereStudents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Integration whereWebsite($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 * @property-read int|null $contacts_count
 */
class School extends Model
{
    use HasFactory;

    protected $table = "schools";

    protected $guarded = [];


    public function contacts(){
        return $this->hasMany(Contact::class);
    }
}
