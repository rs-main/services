<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class InstitutionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request){

        $query = $request->query("field");
        $value = $request->query("value");
        $data_type = $request->query("data");

        $builder = School::with("contacts");

        switch ($query){
            case "name":
                 $builder->where("name", "LIKE","%$value%");
                 break;

            case "ownership":
                 $builder->whereOwnership($value);
                 break;

            case "sex":
                 $builder->whereSex($value);
                 break;

            case "classification":
                 ($builder->whereClassification($value));
                 break;

            case "students":
                 ($builder->whereStudents($value));
                 break;

            case "type":
                 ($builder->whereType($value));
                 break;

            default:
                  $builder;
        }

        return DataTables::of($builder)->make(true);
    }

    public function store(Request $request){

        $this->validate($request,[
            "name" => "required|unique:schools",
            "type" => "required",
            "ownership" => "required",
            "sex"       => "required",
            "students"       => "required",
            "population"       => "required",
            "email"       => "email|unique:schools",
        ]);

        $school = new School();
        $school->name = $request->name;
        $school->short_name = $request->short_name;
        $school->type = $request->type;
        $school->region_id = $request->region_id;
        $school->location_id = $request->location_id;
        $school->ownership = $request->ownership;
        $school->sex = $request->sex;
        $school->students = $request->students;
        $school->classification = $request->classification;
        $school->school_code = $request->school_code;
        $school->population = $request->population;
        $school->bio_info = $request->bio_info;
        $school->ghana_post_code = $request->ghana_post_code;
        $school->email = $request->email;
        $school->website = $request->website;
        $school->colours = json_encode($request->colours);
        $school->logo_url = $request->logo_url;

        if ($school->save() && $request->has("contacts") && $request->has("contact_names")){
            foreach (explode(",",$request->get("contacts")) as $key => $contact){
                $contactObject = new Contact();
                $contactObject->school_id = $school->id;
                $contactObject->contact_number = $contact;
                $contactObject->contact_name =
                    explode(",",$request->get("contact_names"))[$key];
                $contactObject->save();
            }
        }

        return $this->response($school,200);
    }

    public function show($id){
        $school = School::with("contacts")->find($id);
        return $this->response($school, 200);
    }

    public function stats(){

        $results = DB::select( DB::raw("SELECT COUNT(*) AS total,
                             sum( case when type = 'basic' then 1 else 0 end) AS basic,
                             sum( case when type = 'senior-high' then 1 else 0 end) AS senior_high,
                             sum( case when type = 'training-college' then 1 else 0 end) AS training_college,
                             sum( case when type = 'tertiary' then 1 else 0 end) AS tertiary  FROM schools "));

        return response()->json(
            $results
        );
    }

}
