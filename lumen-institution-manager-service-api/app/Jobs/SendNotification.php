<?php

namespace App\Jobs;

use App\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotification extends Job implements ShouldQueue
{
    use NotificationTrait;

    public function __construct($user, $type,$service,$mode,$message)
    {
        $this->buildNotification($user,$type,$service,$mode,$message);
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->sendNotification();
    }
}
