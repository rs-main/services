import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';

@Controller()
export class AppController {
  @MessagePattern({ cmd: 'greeting' })
  getHello(name: string): string {
    return `Hello ${name}!`;
  }

  @MessagePattern({ cmd: 'observable' })
  getObservable(): Observable<number> {
    return from([1, 2, 3]);
  }
}
